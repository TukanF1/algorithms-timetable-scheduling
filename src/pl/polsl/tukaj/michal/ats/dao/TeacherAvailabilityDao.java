package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ITeacherAvailabilityDao;
import pl.polsl.tukaj.michal.ats.model.Teacher;
import pl.polsl.tukaj.michal.ats.model.TeacherAvailability;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TeacherAvailabilityDao extends DBConnection implements ITeacherAvailabilityDao {
    public TeacherAvailabilityDao() {
        super();
    }

    @Override
    public List<TeacherAvailability> getAllTeachersAvailabilities() {
        List<TeacherAvailability> teacherAvailabilities = new ArrayList<>();
        String sql = "SELECT * FROM TEACHERS_AVAILABILITY;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                teacherAvailabilities.add(new TeacherAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("teacher_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teacherAvailabilities;
    }

    @Override
    public TeacherAvailability getTeacherAvailabilityById(Integer id) {
        TeacherAvailability teacherAvailability = null;
        String sql = "SELECT * FROM TEACHERS_AVAILABILITY WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                teacherAvailability = new TeacherAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("teacher_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teacherAvailability;
    }

    @Override
    public List<TeacherAvailability> getTeacherAvailabilitiesByTeacherId(Integer teacherId) {
        List<TeacherAvailability> teacherAvailabilities = new ArrayList<>();
        String sql = "SELECT * FROM TEACHERS_AVAILABILITY WHERE TEACHER_ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, teacherId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                teacherAvailabilities.add(new TeacherAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("teacher_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teacherAvailabilities;
    }

    @Override
    public List<TeacherAvailability> getTeacherAvailabilitiesByTeacher(Teacher teacher) {
        return this.getTeacherAvailabilitiesByTeacherId(teacher.getId());
    }

    @Override
    public Integer saveTeacherAvailability(TeacherAvailability teacherAvailability) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (teacherAvailability.getId() == null
                    || this.getTeacherAvailabilityById(teacherAvailability.getId()) == null) {
                sql = "INSERT INTO TEACHERS_AVAILABILITY (WEEK_DAY, FROM_HOUR, TO_HOUR, TEACHER_ID) "
                        + "VALUES (?, ?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE TEACHERS_AVAILABILITY SET WEEK_DAY = ?, FROM_HOUR = ?, TO_HOUR = ?, TEACHER_ID = ? "
                        + "WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(5, teacherAvailability.getId());
            }
            preparedStatement.setInt(1, teacherAvailability.getWeekDay().getValue());
            preparedStatement.setString(2, teacherAvailability.getFromHour().toString());
            preparedStatement.setString(3, teacherAvailability.getToHour().toString());
            preparedStatement.setInt(4, teacherAvailability.getTeacherId());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error TeacherDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteTeacherAvailability(TeacherAvailability teacherAvailability) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM TEACHERS_AVAILABILITY WHERE ID = ? AND TEACHER_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, teacherAvailability.getId());
            preparedStatement.setInt(2, teacherAvailability.getTeacherId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteTeacherAvailabilityById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM TEACHERS_AVAILABILITY WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteTeacherAvailabilitiesByTeacherId(Integer teacherId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM TEACHERS_AVAILABILITY WHERE TEACHER_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, teacherId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteTeacherAvailabilitiesByTeacher(Teacher teacher) {
        return this.deleteTeacherAvailabilitiesByTeacherId(teacher.getId());
    }
}
