package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.controller.SqliteConnector;

import java.sql.Connection;

class DBConnection {
    Connection connection;

    public DBConnection() {
        this.connection = SqliteConnector.getConnection();
        if (connection == null) {
            System.out.println("Connection to DB failed.");
            System.exit(1);
        }
    }
}
