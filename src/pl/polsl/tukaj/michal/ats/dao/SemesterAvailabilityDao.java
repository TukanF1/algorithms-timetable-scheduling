package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ISemesterAvailabilityDao;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.SemesterAvailability;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SemesterAvailabilityDao extends DBConnection implements ISemesterAvailabilityDao {
    public SemesterAvailabilityDao() {
        super();
    }

    @Override
    public List<SemesterAvailability> getAllSemesterAvailabilities() {
        List<SemesterAvailability> groupAvailabilities = new ArrayList<>();
        String sql = "SELECT * FROM SEMESTER_AVAILABILITY;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                groupAvailabilities.add(new SemesterAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("semester_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupAvailabilities;
    }

    @Override
    public SemesterAvailability getSemesterAvailabilityById(Integer id) {
        SemesterAvailability semesterAvailability = null;
        String sql = "SELECT * FROM SEMESTER_AVAILABILITY WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                semesterAvailability = new SemesterAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("semester_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return semesterAvailability;
    }

    @Override
    public List<SemesterAvailability> getSemesterAvailabilitiesBySemesterId(Integer semesterId) {
        List<SemesterAvailability> groupAvailabilities = new ArrayList<>();
        String sql = "SELECT * FROM SEMESTER_AVAILABILITY WHERE SEMESTER_ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, semesterId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                groupAvailabilities.add(new SemesterAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("semester_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupAvailabilities;
    }

    @Override
    public List<SemesterAvailability> getSemesterAvailabilitiesBySemester(Semester semester) {
        return this.getSemesterAvailabilitiesBySemesterId(semester.getId());
    }

    @Override
    public Integer saveSemestersAvailability(SemesterAvailability semesterAvailability) {
        PreparedStatement preparedStatement;
        String sql;

        try {
            if (semesterAvailability.getId() == null
                    || this.getSemesterAvailabilityById(semesterAvailability.getId()) == null) {
                sql = "INSERT INTO SEMESTER_AVAILABILITY (WEEK_DAY, FROM_HOUR, TO_HOUR, SEMESTER_ID) VALUES (?, ?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE SEMESTER_AVAILABILITY SET WEEK_DAY = ?, FROM_HOUR = ?, TO_HOUR = ?, SEMESTER_ID = ? "
                        + "WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(5, semesterAvailability.getId());
            }
            preparedStatement.setInt(1, semesterAvailability.getWeekDay().getValue());
            preparedStatement.setString(2, semesterAvailability.getFromHour().toString());
            preparedStatement.setString(3, semesterAvailability.getToHour().toString());
            preparedStatement.setInt(4, semesterAvailability.getSemesterId());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error SemesterAvailabilityDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemestersAvailability(SemesterAvailability semesterAvailability) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SEMESTER_AVAILABILITY WHERE ID = ? AND SEMESTER_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, semesterAvailability.getId());
            preparedStatement.setInt(2, semesterAvailability.getSemesterId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemestersAvailabilityById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SEMESTER_AVAILABILITY WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemestersAvailabilitiesBySemesterId(Integer groupId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SEMESTER_AVAILABILITY WHERE SEMESTER_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, groupId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemestersAvailabilitiesBySemester(Semester semester) {
        return this.deleteSemestersAvailabilitiesBySemesterId(semester.getId());
    }
}
