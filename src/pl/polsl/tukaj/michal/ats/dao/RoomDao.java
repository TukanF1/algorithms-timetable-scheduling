package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.IRoomDao;
import pl.polsl.tukaj.michal.ats.model.Room;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RoomDao extends DBConnection implements IRoomDao {
    public RoomDao() {
        super();
    }

    @Override
    public List<Room> getAllRooms() {
        List<Room> rooms = new ArrayList<>();
        String sql = "SELECT * FROM ROOMS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                rooms.add(new Room(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("capacity"),
                        resultSet.getBoolean("virtual")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rooms;
    }

    @Override
    public Room getRoomById(Integer id) {
        Room room = null;
        String sql = "SELECT * FROM ROOMS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                room = new Room(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("capacity"),
                        resultSet.getBoolean("virtual")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return room;
    }

    @Override
    public Room getRoomByName(String name) {
        Room room = null;
        String sql = "SELECT * FROM ROOMS WHERE NAME = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                room = new Room(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("capacity"),
                        resultSet.getBoolean("virtual")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return room;
    }

    @Override
    public Integer saveRoom(Room room) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (room.getId() == null || this.getRoomById(room.getId()) == null) {
                if (this.getRoomByName(room.getName()) == null) {
                    sql = "INSERT INTO ROOMS (NAME, CAPACITY, VIRTUAL) VALUES (?, ?, ?)";
                    preparedStatement = this.connection.prepareStatement(sql, Statement.NO_GENERATED_KEYS);
                } else {
                    return null;
                }
            } else {
                sql = "UPDATE ROOMS SET NAME = ?, CAPACITY = ?, VIRTUAL = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(3, room.getId());
            }
            preparedStatement.setString(1, room.getName());
            preparedStatement.setInt(2, room.getCapacity());
            preparedStatement.setBoolean(3, room.getVirtual());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error RoomDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteRoom(Room room) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM ROOMS WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, room.getId());
            preparedStatement.setString(2, room.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteRoomById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM ROOMS WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
