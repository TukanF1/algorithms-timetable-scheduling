package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.IGeneratedSectionsDao;
import pl.polsl.tukaj.michal.ats.model.GeneratedSection;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GeneratedSectionDao extends DBConnection implements IGeneratedSectionsDao {
    public GeneratedSectionDao() {
        super();
    }

    private List<GeneratedSection> getGeneratedSectionsByColumnId(String columnName, Integer id) {
        String sql = "SELECT * FROM GENERATED_SECTIONS WHERE " + columnName + " = ?;";
        List<GeneratedSection> generatedSections = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                generatedSections.add(new GeneratedSection(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("number_of_people"),
                        resultSet.getInt("section_id"),
                        resultSet.getInt("group_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return generatedSections;
    }

    @Override
    public List<GeneratedSection> getAllGeneratedSections() {
        List<GeneratedSection> generatedSections = new ArrayList<>();
        String sql = "SELECT * FROM GENERATED_SECTIONS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                generatedSections.add(new GeneratedSection(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("number_of_people"),
                        resultSet.getInt("section_id"),
                        resultSet.getInt("group_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return generatedSections;
    }

    @Override
    public GeneratedSection getGeneratedSectionById(Integer id) {
        GeneratedSection generatedSection = null;
        String sql = "SELECT * FROM GENERATED_SECTIONS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                generatedSection = new GeneratedSection(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("number_of_people"),
                        resultSet.getInt("section_id"),
                        resultSet.getInt("group_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return generatedSection;
    }

    @Override
    public List<GeneratedSection> getGeneratedSectionsBySemesterId(Integer semesterId) {
        String columnName = "SEMESTER_ID";
        return this.getGeneratedSectionsByColumnId(columnName, semesterId);
    }

    @Override
    public List<GeneratedSection> getGeneratedSectionsBySemester(Semester semester) {
        return this.getGeneratedSectionsBySemesterId(semester.getId());
    }

    @Override
    public List<GeneratedSection> getGeneratedSectionsByGroupId(Integer groupId) {
        String columnName = "GROUP_ID";
        return this.getGeneratedSectionsByColumnId(columnName, groupId);
    }

    @Override
    public List<GeneratedSection> getGeneratedSectionsByGroup(Group group) {
        return this.getGeneratedSectionsByGroupId(group.getId());
    }

    @Override
    public List<GeneratedSection> geGeneratedSectionsBySectionId(Integer sectionId) {
        String columnName = "SECTION_ID";
        return this.getGeneratedSectionsByColumnId(columnName, sectionId);
    }

    @Override
    public List<GeneratedSection> getGeneratedSectionsBySection(Section section) {
        return this.getGeneratedSectionsBySemesterId(section.getId());
    }

    @Override
    public Integer saveGeneratedSection(GeneratedSection generatedSection) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (generatedSection.getId() == null || this.getGeneratedSectionById(generatedSection.getId()) == null) {
                sql = "INSERT INTO GENERATED_SECTIONS (NAME, NUMBER_OF_PEOPLE, SECTION_ID, GROUP_ID) " +
                        "VALUES (?, ?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE GENERATED_SECTIONS SET NAME = ?, NUMBER_OF_PEOPLE = ?, SECTION_ID = ?, GROUP_ID = ? " +
                        "WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(5, generatedSection.getId());
            }
            preparedStatement.setString(1, generatedSection.getName());
            preparedStatement.setInt(2, generatedSection.getNumberOfPeople());
            if (generatedSection.getSectionId() != null) {
                preparedStatement.setInt(3, generatedSection.getSectionId());
            } else {
                preparedStatement.setNull(3, Types.NULL);
            }
            if (generatedSection.getGroupId() != null) {
                preparedStatement.setInt(4, generatedSection.getGroupId());
            } else {
                preparedStatement.setNull(4, Types.NULL);
            }
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error GeneratedSectionDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void saveGeneratedSections(List<GeneratedSection> generatedSections) {
        generatedSections.forEach(
                generatedSection -> generatedSection.setId(this.saveGeneratedSection(generatedSection))
        );
    }


    @Override
    public Integer deleteGeneratedSection(GeneratedSection generatedSection) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM GENERATED_SECTIONS WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, generatedSection.getId());
            preparedStatement.setString(2, generatedSection.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteGeneratedSectionById(Integer generatedSectionId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM GENERATED_SECTIONS WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, generatedSectionId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteGeneratedSectionBySectionId(Integer sectionId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM GENERATED_SECTIONS WHERE SECTION_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, sectionId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteGeneratedSectionByGroupId(Integer groupId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM GENERATED_SECTIONS WHERE GROUP_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, groupId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
