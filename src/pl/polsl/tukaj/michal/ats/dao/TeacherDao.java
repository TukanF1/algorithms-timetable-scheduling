package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ITeacherDao;
import pl.polsl.tukaj.michal.ats.model.Lesson;
import pl.polsl.tukaj.michal.ats.model.Subject;
import pl.polsl.tukaj.michal.ats.model.Teacher;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TeacherDao extends DBConnection implements ITeacherDao {
    public TeacherDao() {
        super();
    }

    @Override
    public List<Teacher> getAllTeachers() {
        List<Teacher> teachers = new ArrayList<>();
        String sql = "SELECT * FROM TEACHERS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                teachers.add(new Teacher(
                        resultSet.getInt("id"),
                        resultSet.getString("full_name"),
                        resultSet.getBoolean("virtual")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teachers;
    }

    @Override
    public Teacher getTeacherById(Integer id) {
        Teacher teacher = null;
        String sql = "SELECT * FROM TEACHERS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                teacher = new Teacher(
                        resultSet.getInt("id"),
                        resultSet.getString("full_name"),
                        resultSet.getBoolean("virtual")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teacher;
    }

    @Override
    public Teacher getTeacherByFullName(String fullName) {
        Teacher teacher = null;
        String sql = "SELECT * FROM TEACHERS WHERE FULL_NAME = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, fullName);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                teacher = new Teacher(
                        resultSet.getInt("id"),
                        resultSet.getString("full_name"),
                        resultSet.getBoolean("virtual")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teacher;
    }

    @Override
    public List<Teacher> getTeachersBySubject(Subject subject) {
        List<Teacher> teachers = new ArrayList<>();
        if (subject.getId() != null) {
            String sql = "SELECT TEACHERS.* FROM TEACHERS " +
                    "JOIN SUBJECTS_TO_TEACHERS ON TEACHERS.ID = SUBJECTS_TO_TEACHERS.TEACHER_ID " +
                    "WHERE SUBJECTS_TO_TEACHERS.SUBJECT_ID = ?;";
            PreparedStatement preparedStatement;
            ResultSet resultSet;
            try {
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(1, subject.getId());
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    teachers.add(new Teacher(
                            resultSet.getInt("id"),
                            resultSet.getString("full_name"),
                            resultSet.getBoolean("virtual")
                    ));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return teachers;
    }

    @Override
    public List<Teacher> getTeachersByLessonId(Integer lessonId) {
        List<Teacher> teachers = new ArrayList<>();
        String sql = "SELECT TEACHERS.* FROM TEACHERS " +
                "JOIN LESSONS_TO_TEACHERS ON TEACHERS.ID = LESSONS_TO_TEACHERS.TEACHER_ID " +
                "WHERE LESSONS_TO_TEACHERS.LESSON_ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, lessonId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                teachers.add(new Teacher(
                        resultSet.getInt("id"),
                        resultSet.getString("full_name"),
                        resultSet.getBoolean("virtual")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teachers;
    }

    @Override
    public List<Teacher> getTeachersByLesson(Lesson lesson) {
        if (lesson.getId() != null) {
            return this.getTeachersByLessonId(lesson.getId());
        }
        return new ArrayList<>();
    }

    @Override
    public Integer addTeacherIdToLessonById(Integer teacherId, Integer lessonId) {
        if (teacherId != null && lessonId != null) {
            String sql = "INSERT INTO LESSONS_TO_TEACHERS (TEACHER_ID, LESSON_ID) VALUES (?, ?)";
            PreparedStatement preparedStatement;
            List<Teacher> teachers = this.getTeachersByLessonId(lessonId);
            Teacher teacher = this.getTeacherById(teacherId);
            if (!teachers.contains(teacher)) {
                try {
                    preparedStatement = this.connection.prepareStatement(sql);
                    preparedStatement.setInt(1, teacherId);
                    preparedStatement.setInt(2, lessonId);
                    return preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                System.out.println("CONTAINS : " + teacherId);
            }
        }
        return 0;
    }

    @Override
    public Integer addTeacherToLesson(Teacher teacher, Lesson lesson) {
        return this.addTeacherIdToLessonById(teacher.getId(), lesson.getId());
    }

    @Override
    public Integer saveTeacher(Teacher teacher) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (teacher.getId() == null || this.getTeacherById(teacher.getId()) == null) {
                if (this.getTeacherByFullName(teacher.getFullName()) == null) {
                    sql = "INSERT INTO TEACHERS (FULL_NAME) VALUES (?)";
                    preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                } else {
                    return null;
                }
            } else {
                sql = "UPDATE TEACHERS SET FULL_NAME = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(2, teacher.getId());
            }
            preparedStatement.setString(1, teacher.getFullName());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error TeacherDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteTeacher(Teacher room) {
        return null;
    }

    @Override
    public Integer deleteTeacherById(Integer id) {
        return null;
    }
}
