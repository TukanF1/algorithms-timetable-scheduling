package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ITimetableDao;
import pl.polsl.tukaj.michal.ats.model.Timetable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TimetableDao extends DBConnection implements ITimetableDao {
    public TimetableDao() {
        super();
    }

    @Override
    public List<Timetable> getAllTimetables() {
        List<Timetable> timetables = new ArrayList<>();
        String sql = "SELECT * FROM TIMETABLE;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                timetables.add(new Timetable(
                        resultSet.getInt("id"),
                        resultSet.getString("name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return timetables;
    }

    @Override
    public Timetable getTimetableById(Integer id) {
        Timetable timetable = null;
        String sql = "SELECT * FROM TIMETABLE WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                timetable = new Timetable(resultSet.getInt("id"), resultSet.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return timetable;
    }

    @Override
    public Integer saveTimetable(Timetable timetable) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (timetable.getId() == null || this.getTimetableById(timetable.getId()) == null) {
                sql = "INSERT INTO TIMETABLE (NAME) VALUES (?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE TIMETABLE SET NAME = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(2, timetable.getId());
            }
            preparedStatement.setString(1, timetable.getName());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error FeatureDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteTimetableById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM TIMETABLE WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteTimetable(Timetable timetable) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM TIMETABLE WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetable.getId());
            preparedStatement.setString(2, timetable.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
