package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ISemesterDao;
import pl.polsl.tukaj.michal.ats.model.FieldOfStudy;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SemesterDao extends DBConnection implements ISemesterDao {
    public SemesterDao() {
        super();
    }

    @Override
    public List<Semester> getAllSemesters() {
        List<Semester> semesters = new ArrayList<>();
        String sql = "SELECT * FROM SEMESTERS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                semesters.add(new Semester(
                        resultSet.getInt("id"),
                        resultSet.getInt("semester"),
                        resultSet.getInt("field_of_study_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return semesters;
    }

    @Override
    public Semester getSemesterById(Integer id) {
        Semester semester = null;
        String sql = "SELECT * FROM SEMESTERS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                semester = new Semester(
                        resultSet.getInt("id"),
                        resultSet.getInt("semester"),
                        resultSet.getInt("field_of_study_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return semester;
    }

    @Override
    public List<Semester> getSemestersByFieldOfStudyId(Integer fieldOfStudyId) {
        String sql = "SELECT * FROM SEMESTERS WHERE FIELD_OF_STUDY_ID = ?;";
        List<Semester> semesters = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, fieldOfStudyId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                semesters.add(new Semester(
                        resultSet.getInt("id"),
                        resultSet.getInt("semester"),
                        resultSet.getInt("field_of_study_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return semesters;
    }

    @Override
    public List<Semester> getSemestersByFieldOfStudy(FieldOfStudy fieldOfStudy) {
        return this.getSemestersByFieldOfStudyId(fieldOfStudy.getId());
    }

    @Override
    public Semester getSemesterByGroupId(Integer groupId) {
        String sql = "SELECT SEMESTERS.* FROM SEMESTERS " +
                "JOIN GROUPS ON SEMESTERS.ID = GROUPS.SEMESTER_ID " +
                "WHERE GROUPS.ID = ? " +
                "LIMIT 1;";
        Semester semester = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, groupId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                semester = new Semester(
                        resultSet.getInt("id"),
                        resultSet.getInt("semester"),
                        resultSet.getInt("field_of_study_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return semester;
    }

    @Override
    public Semester getSemesterByGroup(Group group) {
        return this.getSemesterByGroupId(group.getId());
    }

    @Override
    public Semester getSemesterBySectionId(Integer sectionId) {
        String sql = "SELECT SEMESTERS.* FROM SEMESTERS " +
                "LEFT JOIN GROUPS ON SEMESTERS.ID = GROUPS.SEMESTER_ID " +
                "LEFT JOIN SECTIONS S on GROUPS.ID = S.GROUP_ID " +
                "LEFT JOIN SECTIONS S2 ON SEMESTERS.ID = S2.SEMESTER_ID " +
                "WHERE S.ID = ? OR S2.ID = ? " +
                "LIMIT 1;";
        Semester semester = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, sectionId);
            preparedStatement.setInt(2, sectionId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                semester = new Semester(
                        resultSet.getInt("id"),
                        resultSet.getInt("semester"),
                        resultSet.getInt("field_of_study_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return semester;
    }

    @Override
    public Semester getSemesterBySection(Section section) {
        return this.getSemesterBySectionId(section.getId());
    }

    @Override
    public Integer saveSemester(Semester semester) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (semester.getId() == null || this.getSemesterById(semester.getId()) == null) {
                sql = "INSERT INTO SEMESTERS (SEMESTER, FIELD_OF_STUDY_ID) VALUES (?, ?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE SEMESTERS SET SEMESTER = ?, FIELD_OF_STUDY_ID = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(3, semester.getId());
            }
            preparedStatement.setInt(1, semester.getSemester());
            preparedStatement.setInt(2, semester.getFieldOfStudyId());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error SemesterDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer addSemesterToFieldOfStudy(Semester semester, FieldOfStudy fieldOfStudy) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            semester.setFieldOfStudyId(fieldOfStudy.getId());
            if (!this.getSemestersByFieldOfStudy(fieldOfStudy).contains(semester)) {
                sql = "INSERT INTO SEMESTERS (SEMESTER, FIELD_OF_STUDY_ID) VALUES (?, ?)";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(1, semester.getSemester());
                preparedStatement.setInt(2, fieldOfStudy.getId());
                return preparedStatement.executeUpdate();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemester(Semester semester) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SEMESTERS WHERE ID = ? AND SEMESTER = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, semester.getId());
            preparedStatement.setInt(2, semester.getSemester());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemesterById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SEMESTERS WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemestersByFieldOfStudyId(Integer fieldOfStudyId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SEMESTERS WHERE FIELD_OF_STUDY_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, fieldOfStudyId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSemestersByFieldOfStudy(FieldOfStudy fieldOfStudy) {
        return this.deleteSemestersByFieldOfStudyId(fieldOfStudy.getId());
    }
}
