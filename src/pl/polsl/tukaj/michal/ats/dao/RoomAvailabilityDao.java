package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.IRoomAvailabilityDao;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.RoomAvailability;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RoomAvailabilityDao extends DBConnection implements IRoomAvailabilityDao {
    public RoomAvailabilityDao() {
        super();
    }

    @Override
    public List<RoomAvailability> getAllRoomsAvailabilities() {
        List<RoomAvailability> roomsAvailabilities = new ArrayList<>();
        String sql = "SELECT * FROM ROOMS_AVAILABILITY;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                roomsAvailabilities.add(new RoomAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("room_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roomsAvailabilities;
    }

    @Override
    public RoomAvailability getRoomAvailabilityById(Integer id) {
        RoomAvailability roomAvailability = null;
        String sql = "SELECT * FROM ROOMS_AVAILABILITY WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                roomAvailability = new RoomAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("room_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roomAvailability;
    }

    @Override
    public List<RoomAvailability> getRoomAvailabilitiesByRoomId(Integer roomId) {
        List<RoomAvailability> roomsAvailabilities = new ArrayList<>();
        String sql = "SELECT * FROM ROOMS_AVAILABILITY WHERE ROOM_ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, roomId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                roomsAvailabilities.add(new RoomAvailability(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getInt("room_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return roomsAvailabilities;
    }

    @Override
    public List<RoomAvailability> getRoomAvailabilitiesByRoom(Room room) {
        return this.getRoomAvailabilitiesByRoomId(room.getId());
    }

    @Override
    public Integer saveRoomAvailability(RoomAvailability roomAvailability) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (roomAvailability.getId() == null || this.getRoomAvailabilityById(roomAvailability.getId()) == null) {
                sql = "INSERT INTO ROOMS_AVAILABILITY (WEEK_DAY, FROM_HOUR, TO_HOUR, ROOM_ID) VALUES (?, ?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE ROOMS_AVAILABILITY SET WEEK_DAY = ?, FROM_HOUR = ?, TO_HOUR = ?, ROOM_ID = ? "
                        + "WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(5, roomAvailability.getId());
            }
            preparedStatement.setInt(1, roomAvailability.getWeekDay().getValue());
            preparedStatement.setString(2, roomAvailability.getFromHour().toString());
            preparedStatement.setString(3, roomAvailability.getToHour().toString());
            preparedStatement.setInt(4, roomAvailability.getRoomId());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error RoomAvailabilityDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteRoomAvailability(RoomAvailability roomAvailability) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM ROOMS_AVAILABILITY WHERE ID = ? AND ROOM_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, roomAvailability.getId());
            preparedStatement.setInt(2, roomAvailability.getRoomId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteRoomAvailabilityById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM ROOMS_AVAILABILITY WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteRoomAvailabilitiesByRoomId(Integer roomId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM ROOMS_AVAILABILITY WHERE ROOM_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, roomId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteRoomAvailabilitiesByRoom(Room room) {
        return this.deleteRoomAvailabilitiesByRoomId(room.getId());
    }
}
