package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Room;

import java.util.List;

public interface IRoomDao {
    List<Room> getAllRooms();

    Room getRoomById(Integer id);

    Room getRoomByName(String name);

    Integer saveRoom(Room room);

    Integer deleteRoom(Room room);

    Integer deleteRoomById(Integer id);
}
