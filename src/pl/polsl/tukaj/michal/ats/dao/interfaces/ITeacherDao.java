package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Lesson;
import pl.polsl.tukaj.michal.ats.model.Subject;
import pl.polsl.tukaj.michal.ats.model.Teacher;

import java.util.List;

public interface ITeacherDao {
    List<Teacher> getAllTeachers();

    Teacher getTeacherById(Integer id);

    Teacher getTeacherByFullName(String name);

    List<Teacher> getTeachersBySubject(Subject subject);

    List<Teacher> getTeachersByLessonId(Integer lessonId);

    List<Teacher> getTeachersByLesson(Lesson lesson);

    Integer addTeacherIdToLessonById(Integer teacherId, Integer lessonId);

    Integer addTeacherToLesson(Teacher teacher, Lesson lesson);

    Integer saveTeacher(Teacher room);

    Integer deleteTeacher(Teacher room);

    Integer deleteTeacherById(Integer id);
}
