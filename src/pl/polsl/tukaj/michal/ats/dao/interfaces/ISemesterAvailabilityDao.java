package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.SemesterAvailability;

import java.util.List;

public interface ISemesterAvailabilityDao {
    List<SemesterAvailability> getAllSemesterAvailabilities();

    SemesterAvailability getSemesterAvailabilityById(Integer id);

    List<SemesterAvailability> getSemesterAvailabilitiesBySemesterId(Integer semesterId);

    List<SemesterAvailability> getSemesterAvailabilitiesBySemester(Semester semester);

    Integer saveSemestersAvailability(SemesterAvailability semesterAvailability);

    Integer deleteSemestersAvailability(SemesterAvailability semesterAvailability);

    Integer deleteSemestersAvailabilityById(Integer id);

    Integer deleteSemestersAvailabilitiesBySemesterId(Integer semesterId);

    Integer deleteSemestersAvailabilitiesBySemester(Semester semester);
}
