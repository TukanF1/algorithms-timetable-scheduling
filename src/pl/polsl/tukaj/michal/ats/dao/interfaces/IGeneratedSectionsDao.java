package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.GeneratedSection;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.util.List;

public interface IGeneratedSectionsDao {
    List<GeneratedSection> getAllGeneratedSections();

    GeneratedSection getGeneratedSectionById(Integer id);

    List<GeneratedSection> getGeneratedSectionsBySemesterId(Integer semesterId);

    List<GeneratedSection> getGeneratedSectionsBySemester(Semester semester);

    List<GeneratedSection> getGeneratedSectionsByGroupId(Integer groupId);

    List<GeneratedSection> getGeneratedSectionsByGroup(Group group);

    List<GeneratedSection> geGeneratedSectionsBySectionId(Integer sectionId);

    List<GeneratedSection> getGeneratedSectionsBySection(Section section);

    Integer saveGeneratedSection(GeneratedSection generatedSection);

    void saveGeneratedSections(List<GeneratedSection> generatedSections);

    Integer deleteGeneratedSection(GeneratedSection generatedSection);

    Integer deleteGeneratedSectionById(Integer generatedSectionId);

    Integer deleteGeneratedSectionBySectionId(Integer sectionId);

    Integer deleteGeneratedSectionByGroupId(Integer groupId);
}
