package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.*;

import java.util.List;

public interface ILessonDao {
    List<Lesson> getAllLessons();

    Lesson getLessonById(Integer id);

    List<Lesson> getLessonsByTimetableId(Integer timetableId);

    List<Lesson> getLessonsByTimetable(Timetable timetable);

    List<Lesson> getLessonsByTeacherIdAndTimetableId(Integer teacherId, Integer timetableId);

    List<Lesson> getLessonsByTeacherAndTimetable(Teacher teacher, Timetable timetable);

    List<Lesson> getLessonsByGeneratedSectionIdAndTimetableId(Integer generatedSectionId, Integer timetableId);

    List<Lesson> getLessonsByGeneratedSectionAndTimetable(GeneratedSection generatedSection, Timetable timetable);

    List<Lesson> getLessonsBySectionIdAndTimetableId(Integer sectionId, Integer timetableId);

    List<Lesson> getLessonsBySectionAndTimetable(Section section, Timetable timetable);

    List<Lesson> getLessonsByGroupIdAndTimetableId(Integer groupId, Integer timetableId);

    List<Lesson> getLessonsByGroupAndTimetable(Group group, Timetable timetable);

    List<Lesson> getLessonsBySemesterIdAndTimetableId(Integer semesterId, Integer timetableId);

    List<Lesson> getLessonsBySemesterAndTimetable(Semester semester, Timetable timetable);

    List<Lesson> getLessonsByRoomIdAndTimetableId(Integer roomId, Integer timetableId);

    List<Lesson> getLessonsByRoomAndTimetable(Room room, Timetable timetable);

    Integer saveLesson(Lesson lesson);

    void saveLessons(List<Lesson> lessons);

    Integer deleteLessonById(Integer id);

    Integer deleteLesson(Lesson lesson);

    Integer deleteLessonByTimetableId(Integer timetableId);

    Integer deleteLessonByTimetable(Timetable timetable);
}
