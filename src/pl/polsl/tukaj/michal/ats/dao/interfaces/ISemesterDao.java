package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.FieldOfStudy;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.util.List;

public interface ISemesterDao {
    List<Semester> getAllSemesters();

    Semester getSemesterById(Integer id);

    List<Semester> getSemestersByFieldOfStudyId(Integer fieldOfStudyId);

    List<Semester> getSemestersByFieldOfStudy(FieldOfStudy fieldOfStudy);

    Semester getSemesterByGroupId(Integer fieldOfStudyId);

    Semester getSemesterBySectionId(Integer sectionId);

    Semester getSemesterByGroup(Group group);

    Semester getSemesterBySection(Section section);

    Integer saveSemester(Semester semester);

    Integer addSemesterToFieldOfStudy(Semester semester, FieldOfStudy fieldOfStudy);

    Integer deleteSemester(Semester semester);

    Integer deleteSemesterById(Integer id);

    Integer deleteSemestersByFieldOfStudyId(Integer fieldOfStudyId);

    Integer deleteSemestersByFieldOfStudy(FieldOfStudy fieldOfStudy);
}
