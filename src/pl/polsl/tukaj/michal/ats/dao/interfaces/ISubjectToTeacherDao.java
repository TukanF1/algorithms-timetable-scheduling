package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.SubjectToTeacher;

import java.util.List;

public interface ISubjectToTeacherDao {
    List<SubjectToTeacher> getAllSubjectsToTeachers();

    SubjectToTeacher getSubjectToTeacherById(Integer id);

    Integer saveSubjectToTeacher(SubjectToTeacher subjectToTeacher);

    Integer deleteSubjectToTeacher(SubjectToTeacher subjectToTeacher);

    Integer deleteSubjectToTeacherById(Integer id);
}
