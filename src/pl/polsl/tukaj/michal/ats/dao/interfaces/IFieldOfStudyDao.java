package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.FieldOfStudy;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.util.List;

public interface IFieldOfStudyDao {
    List<FieldOfStudy> getAllFieldsOfStudy();

    FieldOfStudy getFieldOfStudyById(Integer fieldOfStudyId);

    FieldOfStudy getFieldOfStudyByName(String name);

    FieldOfStudy getFieldOfStudyBySemesterId(Integer semesterId);

    FieldOfStudy getFieldOfStudyBySemester(Semester semester);

    FieldOfStudy getFieldOfStudyByGroupId(Integer groupId);

    FieldOfStudy getFieldOfStudyByGroup(Group group);

    Integer saveFieldOfStudy(FieldOfStudy fieldOfStudy);

    Integer deleteFieldOfStudy(FieldOfStudy fieldOfStudy);

    Integer deleteFieldOfStudyById(Integer fieldOfStudyId);
}
