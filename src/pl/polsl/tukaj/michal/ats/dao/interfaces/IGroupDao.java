package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.util.List;

public interface IGroupDao {
    List<Group> getAllGroups();

    Group getGroupById(Integer id);

    List<Group> getGroupsBySemesterId(Integer semesterId);

    List<Group> getGroupsBySemester(Semester semester);

    List<Group> getGroupsBySubject(Subject subject);

    Group getGroupBySectionId(Integer sectionId);

    Group getGroupBySection(Section section);

    Integer saveGroup(Group group);

    Integer addGroupToSemester(Group group, Semester semester);

    Integer deleteGroup(Group group);

    Integer deleteGroupById(Integer id);

    Integer deleteGroupsBySemesterId(Integer semesterId);

    Integer deleteGroupsBySemester(Semester semester);
}
