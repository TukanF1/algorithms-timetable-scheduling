package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.RoomAvailability;

import java.util.List;

public interface IRoomAvailabilityDao {
    List<RoomAvailability> getAllRoomsAvailabilities();

    RoomAvailability getRoomAvailabilityById(Integer id);

    List<RoomAvailability> getRoomAvailabilitiesByRoomId(Integer roomId);

    List<RoomAvailability> getRoomAvailabilitiesByRoom(Room room);

    Integer saveRoomAvailability(RoomAvailability roomAvailability);

    Integer deleteRoomAvailability(RoomAvailability roomAvailability);

    Integer deleteRoomAvailabilityById(Integer id);

    Integer deleteRoomAvailabilitiesByRoomId(Integer roomId);

    Integer deleteRoomAvailabilitiesByRoom(Room room);
}
