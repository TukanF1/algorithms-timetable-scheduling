package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Feature;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.util.List;

public interface IFeatureDao {
    List<Feature> getAllFeatures();

    Feature getFeatureById(Integer id);

    Feature getFeatureByName(String name);

    List<Feature> getFeaturesByRoomId(Integer id);

    List<Feature> getFeaturesByRoom(Room room);

    List<Feature> getFeaturesBySubjectId(Integer id);

    List<Feature> getFeaturesBySubject(Subject subject);

    Integer addFeatureToRoom(Feature feature, Room room);

    Integer addFeatureToSubject(Feature feature, Subject subject);

    Integer saveFeature(Feature feature);

    Integer deleteFeature(Feature feature);

    Integer deleteFeatureById(Integer id);

    Integer deleteFeaturesFromRoom(Room room);

    Integer deleteFeaturesFromSubject(Subject subject);
}
