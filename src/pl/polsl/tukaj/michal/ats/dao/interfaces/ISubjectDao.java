package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.*;

import java.util.List;

public interface ISubjectDao {
    List<Subject> getAllSubjects();

    Subject getSubjectById(Integer id);

    Subject getSubjectByName(String name);

    List<Subject> getSubjectsByTeacher(Teacher teacher);

    List<Subject> getSubjectsByGroup(Group group);

    List<Subject> getSubjectsBySection(Section section);

    List<Subject> getSubjectsNotAddedByTimetableId(Integer timetableId);

    List<Subject> getSubjectsNotAddedByTimetable(Timetable timetable);

    Integer addSubjectIdToNotAddedInTimetableById(Integer subjectId, Integer timetableId);

    Integer addSubjectToNotAddedInTimetable(Subject subject, Timetable timetable);

    Integer saveSubject(Subject subject);

    Integer deleteSubjectIdFromNotAddedInTimetableById(Integer subjectId, Integer timetableId);

    Integer deleteSubjectFromNotAddedInTimetable(Subject subject, Timetable timetable);

    Integer deleteSubject(Subject subject);

    Integer deleteSubjectById(Integer id);
}
