package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Timetable;

import java.util.List;

public interface ITimetableDao {
    List<Timetable> getAllTimetables();

    Timetable getTimetableById(Integer id);

    Integer saveTimetable(Timetable feature);

    Integer deleteTimetableById(Integer id);

    Integer deleteTimetable(Timetable feature);
}
