package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.util.List;

public interface ISectionDao {
    List<Section> getAllSections();

    Section getSectionById(Integer id);

    List<Section> getSectionsByGroupId(Integer groupId);

    List<Section> getSectionsByGroup(Group group);

    List<Section> getSectionsBySemesterId(Integer semesterId);

    List<Section> getSectionsBySemester(Semester semester);

    List<Section> getSectionsBySubject(Subject subject);

    Integer saveSection(Section section);

    Integer addSectionToGroup(Section section, Group group);

    Integer deleteSection(Section section);

    Integer deleteSectionById(Integer id);

    Integer deleteSectionsByGroupId(Integer groupId);

    Integer deleteSectionsByGroup(Group group);
}
