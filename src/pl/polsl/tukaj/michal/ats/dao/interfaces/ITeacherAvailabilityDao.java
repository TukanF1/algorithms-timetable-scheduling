package pl.polsl.tukaj.michal.ats.dao.interfaces;

import pl.polsl.tukaj.michal.ats.model.Teacher;
import pl.polsl.tukaj.michal.ats.model.TeacherAvailability;

import java.util.List;

public interface ITeacherAvailabilityDao {
    List<TeacherAvailability> getAllTeachersAvailabilities();

    TeacherAvailability getTeacherAvailabilityById(Integer id);

    List<TeacherAvailability> getTeacherAvailabilitiesByTeacherId(Integer teacherId);

    List<TeacherAvailability> getTeacherAvailabilitiesByTeacher(Teacher teacher);

    Integer saveTeacherAvailability(TeacherAvailability teacherAvailability);

    Integer deleteTeacherAvailability(TeacherAvailability teacherAvailability);

    Integer deleteTeacherAvailabilityById(Integer id);

    Integer deleteTeacherAvailabilitiesByTeacherId(Integer teacherId);

    Integer deleteTeacherAvailabilitiesByTeacher(Teacher teacher);
}
