package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.IFieldOfStudyDao;
import pl.polsl.tukaj.michal.ats.model.FieldOfStudy;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FieldOfStudyDao extends DBConnection implements IFieldOfStudyDao {
    public FieldOfStudyDao() {
        super();
    }

    @Override
    public List<FieldOfStudy> getAllFieldsOfStudy() {
        List<FieldOfStudy> fieldOfStudies = new ArrayList<>();
        String sql = "SELECT * FROM FIELDS_OF_STUDY;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                fieldOfStudies.add(new FieldOfStudy(
                        resultSet.getInt("id"),
                        resultSet.getString("name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fieldOfStudies;
    }

    public List<FieldOfStudy> getAllBySemesterId(Integer semesterId) {
        List<FieldOfStudy> fieldOfStudies = new ArrayList<>();
        String sql = "SELECT * FROM FIELDS_OF_STUDY JOIN SEMESTERS S on FIELDS_OF_STUDY.ID = S.FIELD_OF_STUDY_ID WHERE S.ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, semesterId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                fieldOfStudies.add(new FieldOfStudy(
                        resultSet.getInt("id"),
                        resultSet.getString("name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fieldOfStudies;
    }

    @Override
    public FieldOfStudy getFieldOfStudyById(Integer fieldOfStudyId) {
        FieldOfStudy fieldOfStudy = null;
        String sql = "SELECT * FROM FIELDS_OF_STUDY WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, fieldOfStudyId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                fieldOfStudy = new FieldOfStudy(
                        resultSet.getInt("id"),
                        resultSet.getString("name")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fieldOfStudy;
    }

    @Override
    public FieldOfStudy getFieldOfStudyByName(String name) {
        FieldOfStudy fieldOfStudy = null;
        String sql = "SELECT * FROM FIELDS_OF_STUDY WHERE NAME = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                fieldOfStudy = new FieldOfStudy(
                        resultSet.getInt("id"),
                        resultSet.getString("name")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fieldOfStudy;
    }

    @Override
    public FieldOfStudy getFieldOfStudyBySemesterId(Integer semesterId) {
        FieldOfStudy fieldOfStudy = null;
        String sql = "SELECT FIELDS_OF_STUDY.* FROM FIELDS_OF_STUDY " +
                "JOIN SEMESTERS ON FIELDS_OF_STUDY.ID = SEMESTERS.FIELD_OF_STUDY_ID " +
                "WHERE SEMESTERS.ID = ? " +
                "LIMIT 1;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, semesterId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                fieldOfStudy = new FieldOfStudy(
                        resultSet.getInt("id"),
                        resultSet.getString("name")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fieldOfStudy;
    }

    @Override
    public FieldOfStudy getFieldOfStudyBySemester(Semester semester) {
        return this.getFieldOfStudyBySemesterId(semester.getId());
    }

    @Override
    public FieldOfStudy getFieldOfStudyByGroupId(Integer groupId) {
        return this.getFieldOfStudyBySemester(new SemesterDao().getSemesterByGroupId(groupId));
    }

    @Override
    public FieldOfStudy getFieldOfStudyByGroup(Group group) {
        return this.getFieldOfStudyBySemester(new SemesterDao().getSemesterByGroup(group));
    }

    @Override
    public Integer saveFieldOfStudy(FieldOfStudy fieldOfStudy) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (fieldOfStudy.getId() == null || this.getFieldOfStudyById(fieldOfStudy.getId()) == null) {
                if (this.getFieldOfStudyByName(fieldOfStudy.getName()) == null) {
                    sql = "INSERT INTO FIELDS_OF_STUDY (NAME) VALUES (?)";
                    preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                } else {
                    return null;
                }
            } else {
                sql = "UPDATE FIELDS_OF_STUDY SET NAME = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(2, fieldOfStudy.getId());
            }
            preparedStatement.setString(1, fieldOfStudy.getName());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error FieldOfStudyDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteFieldOfStudy(FieldOfStudy fieldOfStudy) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM FIELDS_OF_STUDY WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, fieldOfStudy.getId());
            preparedStatement.setString(2, fieldOfStudy.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteFieldOfStudyById(Integer fieldOfStudyId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM FIELDS_OF_STUDY WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, fieldOfStudyId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
