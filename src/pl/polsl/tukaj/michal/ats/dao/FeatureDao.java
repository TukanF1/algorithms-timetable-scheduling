package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.IFeatureDao;
import pl.polsl.tukaj.michal.ats.model.Feature;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class FeatureDao extends DBConnection implements IFeatureDao {
    public FeatureDao() {
        super();
    }

    @Override
    public List<Feature> getAllFeatures() {
        List<Feature> features = new ArrayList<>();
        String sql = "SELECT * FROM FEATURES;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                features.add(new Feature(resultSet.getInt("id"), resultSet.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return features;
    }

    @Override
    public Feature getFeatureById(Integer id) {
        Feature feature = null;
        String sql = "SELECT * FROM FEATURES WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                feature = new Feature(resultSet.getInt("id"), resultSet.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feature;
    }

    @Override
    public Feature getFeatureByName(String name) {
        Feature feature = null;
        String sql = "SELECT * FROM FEATURES WHERE NAME = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                feature = new Feature(resultSet.getInt("id"), resultSet.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feature;
    }

    private List<Feature> getFeaturesByPutIdToSql(String sql, Integer id) {
        List<Feature> features = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                features.add(new Feature(resultSet.getInt("id"), resultSet.getString("name")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return features;
    }

    @Override
    public List<Feature> getFeaturesByRoomId(Integer id) {
        String sql = "SELECT * FROM FEATURES WHERE ID IN (SELECT FEATURE_ID FROM FEATURES_TO_ROOMS WHERE ROOM_ID = ?);";
        return this.getFeaturesByPutIdToSql(sql, id);
    }

    @Override
    public List<Feature> getFeaturesByRoom(Room room) {
        return this.getFeaturesByRoomId(room.getId());
    }

    @Override
    public List<Feature> getFeaturesBySubjectId(Integer id) {
        String sql = "SELECT * FROM FEATURES WHERE ID IN "
                + "(SELECT FEATURE_ID FROM FEATURES_TO_SUBJECTS WHERE SUBJECT_ID = ?);";
        return this.getFeaturesByPutIdToSql(sql, id);
    }

    @Override
    public List<Feature> getFeaturesBySubject(Subject subject) {
        return this.getFeaturesBySubjectId(subject.getId());
    }

    @Override
    public Integer addFeatureToRoom(Feature feature, Room room) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            List<Feature> features = this.getFeaturesByRoom(room);
            Feature featureDB = this.getFeatureById(feature.getId());
            if ((features == null || !features.contains(feature))
                    && (featureDB != null && featureDB.getName().equals(feature.getName()))) {
                sql = "INSERT INTO FEATURES_TO_ROOMS (FEATURE_ID, ROOM_ID) VALUES (?, ?)";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(1, feature.getId());
                preparedStatement.setInt(2, room.getId());
                return preparedStatement.executeUpdate();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer addFeatureToSubject(Feature feature, Subject subject) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            List<Feature> features = this.getFeaturesBySubject(subject);
            Feature featureDB = this.getFeatureById(feature.getId());
            if ((features == null || !features.contains(feature))
                    && (featureDB != null && featureDB.getName().equals(feature.getName()))) {
                sql = "INSERT INTO FEATURES_TO_SUBJECTS (FEATURE_ID, SUBJECT_ID) VALUES (?, ?)";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(1, feature.getId());
                preparedStatement.setInt(2, subject.getId());
                return preparedStatement.executeUpdate();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer saveFeature(Feature feature) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (feature.getName() == null) {
                return null;
            }
            if (feature.getId() == null || this.getFeatureById(feature.getId()) == null) {
                if (this.getFeatureByName(feature.getName()) == null) {
                    sql = "INSERT INTO FEATURES (NAME) VALUES (?)";
                    preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                } else {
                    return null;
                }
            } else {
                sql = "UPDATE FEATURES SET NAME = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(2, feature.getId());
            }
            preparedStatement.setString(1, feature.getName());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error FeatureDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteFeature(Feature feature) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM FEATURES WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, feature.getId());
            preparedStatement.setString(2, feature.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteFeatureById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM FEATURES WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteFeaturesFromRoom(Room room) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM FEATURES_TO_ROOMS WHERE ROOM_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, room.getId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteFeaturesFromSubject(Subject subject) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM FEATURES_TO_SUBJECTS WHERE SUBJECT_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, subject.getId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
