package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ILessonDao;
import pl.polsl.tukaj.michal.ats.model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class LessonDao extends DBConnection implements ILessonDao {
    public LessonDao() {
        super();
    }

    private List<Lesson> getLessonsByColumnIdAndTimetableId(String columnName, Integer id,
                                                            Integer timetableId) {
        boolean isColumnId = columnName != null && id != null;
        String sql;
        if (isColumnId) {
            sql = "SELECT * FROM LESSONS WHERE TIMETABLE_ID = ? AND " + columnName + " = ?;";
        } else {
            sql = "SELECT * FROM LESSONS WHERE TIMETABLE_ID = ?;";
        }
        List<Lesson> lessons = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetableId);
            if (isColumnId) {
                preparedStatement.setInt(2, id);
            }
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lessons.add(new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getBoolean("even_week"),
                        resultSet.getBoolean("odd_week"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("generated_section_id"),
                        resultSet.getInt("room_id"),
                        resultSet.getInt("timetable_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    @Override
    public List<Lesson> getAllLessons() {
        String sql = "SELECT * FROM LESSONS;";
        List<Lesson> lessons = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lessons.add(new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getBoolean("even_week"),
                        resultSet.getBoolean("odd_week"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("generated_section_id"),
                        resultSet.getInt("room_id"),
                        resultSet.getInt("timetable_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    @Override
    public Lesson getLessonById(Integer id) {
        String sql = "SELECT * FROM LESSONS WHERE ID = ?;";
        Lesson lessons = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lessons = new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getBoolean("even_week"),
                        resultSet.getBoolean("odd_week"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("generated_section_id"),
                        resultSet.getInt("room_id"),
                        resultSet.getInt("timetable_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    @Override
    public List<Lesson> getLessonsByTimetableId(Integer timetableId) {
        return this.getLessonsByColumnIdAndTimetableId(null, null, timetableId);
    }

    @Override
    public List<Lesson> getLessonsByTimetable(Timetable timetable) {
        return this.getLessonsByTimetableId(timetable.getId());
    }

    @Override
    public List<Lesson> getLessonsByTeacherIdAndTimetableId(Integer teacherId, Integer timetableId) {
        String sql = "SELECT LESSONS.* FROM LESSONS " +
                "JOIN LESSONS_TO_TEACHERS LTT ON LESSONS.ID = LTT.LESSON_ID " +
                "WHERE TIMETABLE_ID = ? AND TEACHER_ID = ?;";
        List<Lesson> lessons = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetableId);
            preparedStatement.setInt(2, teacherId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lessons.add(new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getBoolean("even_week"),
                        resultSet.getBoolean("odd_week"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("generated_section_id"),
                        resultSet.getInt("room_id"),
                        resultSet.getInt("timetable_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    @Override
    public List<Lesson> getLessonsByTeacherAndTimetable(Teacher teacher, Timetable timetable) {
        return this.getLessonsByTeacherIdAndTimetableId(teacher.getId(), timetable.getId());
    }

    @Override
    public List<Lesson> getLessonsByGeneratedSectionIdAndTimetableId(Integer generatedSectionId, Integer timetableId) {
        String columnName = "GENERATED_SECTION_ID";
        return this.getLessonsByColumnIdAndTimetableId(columnName, generatedSectionId, timetableId);
    }

    @Override
    public List<Lesson> getLessonsByGeneratedSectionAndTimetable(GeneratedSection generatedSection,
                                                                 Timetable timetable) {
        return this.getLessonsByGeneratedSectionIdAndTimetableId(generatedSection.getId(), timetable.getId());
    }

    @Override
    public List<Lesson> getLessonsBySectionIdAndTimetableId(Integer sectionId, Integer timetableId) {
        String sql = "SELECT LESSONS.* FROM LESSONS " +
                "JOIN GENERATED_SECTIONS ON LESSONS.GENERATED_SECTION_ID = GENERATED_SECTIONS.ID " +
                "WHERE TIMETABLE_ID = ? AND SECTION_ID = ?;";
        List<Lesson> lessons = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetableId);
            preparedStatement.setInt(2, sectionId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lessons.add(new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getBoolean("even_week"),
                        resultSet.getBoolean("odd_week"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("generated_section_id"),
                        resultSet.getInt("room_id"),
                        resultSet.getInt("timetable_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    @Override
    public List<Lesson> getLessonsBySectionAndTimetable(Section section, Timetable timetable) {
        return this.getLessonsBySectionIdAndTimetableId(section.getId(), timetable.getId());
    }

    @Override
    public List<Lesson> getLessonsByGroupIdAndTimetableId(Integer groupId, Integer timetableId) {
        String sql = "SELECT LESSONS.* FROM LESSONS " +
                "JOIN GENERATED_SECTIONS ON LESSONS.GENERATED_SECTION_ID = GENERATED_SECTIONS.ID " +
                "LEFT JOIN SECTIONS ON GENERATED_SECTIONS.SECTION_ID = SECTIONS.ID " +
                "LEFT JOIN SEMESTERS S ON SECTIONS.SEMESTER_ID = S.ID " +
                "LEFT JOIN GROUPS G on S.ID = G.SEMESTER_ID " +
                "WHERE TIMETABLE_ID = ? " +
                "AND (GENERATED_SECTIONS.GROUP_ID = ? " +
                "OR SECTIONS.GROUP_ID = ?" +
                "OR G.ID = ?);";
        List<Lesson> lessons = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetableId);
            preparedStatement.setInt(2, groupId);
            preparedStatement.setInt(3, groupId);
            preparedStatement.setInt(4, groupId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lessons.add(new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getBoolean("even_week"),
                        resultSet.getBoolean("odd_week"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("generated_section_id"),
                        resultSet.getInt("room_id"),
                        resultSet.getInt("timetable_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    @Override
    public List<Lesson> getLessonsByGroupAndTimetable(Group group, Timetable timetable) {
        return this.getLessonsByGroupIdAndTimetableId(group.getId(), timetable.getId());
    }

    @Override
    public List<Lesson> getLessonsBySemesterIdAndTimetableId(Integer semesterId, Integer timetableId) {
        String sql = "SELECT LESSONS.* FROM LESSONS " +
                "JOIN GENERATED_SECTIONS ON LESSONS.GENERATED_SECTION_ID = GENERATED_SECTIONS.ID " +
                "LEFT JOIN SECTIONS ON GENERATED_SECTIONS.SECTION_ID = SECTIONS.ID " +
                "LEFT JOIN GROUPS G1 ON SECTIONS.GROUP_ID = G1.ID " +
                "LEFT JOIN GROUPS G2 ON GENERATED_SECTIONS.GROUP_ID = G2.ID " +
                "WHERE TIMETABLE_ID = ? " +
                "AND (SECTIONS.SEMESTER_ID = ? " +
                "OR G1.SEMESTER_ID = ? " +
                "OR G2.SEMESTER_ID = ?);";
        List<Lesson> lessons = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetableId);
            preparedStatement.setInt(2, semesterId);
            preparedStatement.setInt(3, semesterId);
            preparedStatement.setInt(4, semesterId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                lessons.add(new Lesson(
                        resultSet.getInt("id"),
                        resultSet.getInt("week_day"),
                        resultSet.getString("from_hour"),
                        resultSet.getString("to_hour"),
                        resultSet.getBoolean("even_week"),
                        resultSet.getBoolean("odd_week"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("generated_section_id"),
                        resultSet.getInt("room_id"),
                        resultSet.getInt("timetable_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lessons;
    }

    @Override
    public List<Lesson> getLessonsBySemesterAndTimetable(Semester semester, Timetable timetable) {
        return this.getLessonsBySemesterIdAndTimetableId(semester.getId(), timetable.getId());
    }

    @Override
    public List<Lesson> getLessonsByRoomIdAndTimetableId(Integer roomId, Integer timetableId) {
        String columnName = "ROOM_ID";
        return this.getLessonsByColumnIdAndTimetableId(columnName, roomId, timetableId);
    }

    @Override
    public List<Lesson> getLessonsByRoomAndTimetable(Room room, Timetable timetable) {
        return this.getLessonsByRoomIdAndTimetableId(room.getId(), timetable.getId());
    }

    @Override
    public Integer saveLesson(Lesson lesson) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (lesson.getId() == null || this.getLessonById(lesson.getId()) == null) {
                sql = "INSERT INTO LESSONS " +
                        "(WEEK_DAY, FROM_HOUR, TO_HOUR, EVEN_WEEK, ODD_WEEK, " +
                        "SUBJECT_ID, GENERATED_SECTION_ID, ROOM_ID, TIMETABLE_ID) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql);
            } else {
                sql = "UPDATE LESSONS SET WEEK_DAY = ?, FROM_HOUR = ?, TO_HOUR = ?, EVEN_WEEK = ?, ODD_WEEK = ?," +
                        "SUBJECT_ID = ?, GENERATED_SECTION_ID = ?, ROOM_ID = ?, TIMETABLE_ID = ? " +
                        "WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(11, lesson.getId());
            }
            preparedStatement.setInt(1, lesson.getWeekDay().getValue());
            preparedStatement.setString(2, lesson.getFromHour().format(
                    DateTimeFormatter.ofPattern("HH:mm")
            ));
            preparedStatement.setString(3, lesson.getToHour().format(
                    DateTimeFormatter.ofPattern("HH:mm")
            ));
            preparedStatement.setBoolean(4, lesson.getEvenWeek());
            preparedStatement.setBoolean(5, lesson.getOddWeek());
            preparedStatement.setInt(6, lesson.getSubjectId());
            preparedStatement.setInt(7, lesson.getGeneratedSectionId());
            preparedStatement.setInt(8, lesson.getRoomId());
            preparedStatement.setInt(9, lesson.getTimetableId());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    } else {
                        throw new SQLException("Error LessonDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void saveLessons(List<Lesson> lessons) {
        lessons.forEach(lesson -> {
            lesson.setId(this.saveLesson(lesson));
            lesson.getTeachers().forEach(teacher -> new TeacherDao().addTeacherToLesson(teacher, lesson));
        });
    }

    @Override
    public Integer deleteLessonById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM LESSONS WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteLesson(Lesson lesson) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM LESSONS WHERE ID = ? AND TIMETABLE_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, lesson.getId());
            preparedStatement.setInt(2, lesson.getTimetableId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteLessonByTimetableId(Integer timetableId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM LESSONS WHERE TIMETABLE_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetableId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteLessonByTimetable(Timetable timetable) {
        return this.deleteLessonByTimetableId(timetable.getId());
    }
}
