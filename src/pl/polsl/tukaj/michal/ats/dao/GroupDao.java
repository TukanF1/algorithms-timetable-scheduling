package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.IGroupDao;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GroupDao extends DBConnection implements IGroupDao {
    public GroupDao() {
        super();
    }

    @Override
    public List<Group> getAllGroups() {
        List<Group> groups = new ArrayList<>();
        String sql = "SELECT * FROM GROUPS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                groups.add(new Group(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("semester_id"),
                        resultSet.getInt("number_of_people")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groups;
    }

    @Override
    public Group getGroupById(Integer id) {
        Group group = null;
        String sql = "SELECT * FROM GROUPS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                group = new Group(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("semester_id"),
                        resultSet.getInt("number_of_people")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return group;
    }

    @Override
    public List<Group> getGroupsBySemesterId(Integer semesterId) {
        String sql = "SELECT * FROM GROUPS WHERE SEMESTER_ID = ?;";
        List<Group> groups = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, semesterId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                groups.add(new Group(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("semester_id"),
                        resultSet.getInt("number_of_people")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groups;
    }

    @Override
    public List<Group> getGroupsBySemester(Semester semester) {
        return this.getGroupsBySemesterId(semester.getId());
    }

    @Override
    public List<Group> getGroupsBySubject(Subject subject) {
        String sql = "SELECT GROUPS.* FROM GROUPS " +
                "JOIN SUBJECTS_TO_GROUPS ON GROUPS.ID = SUBJECTS_TO_GROUPS.GROUP_ID " +
                "WHERE SUBJECTS_TO_GROUPS.SUBJECT_ID = ?;";
        List<Group> groups = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, subject.getId());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                groups.add(new Group(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("semester_id"),
                        resultSet.getInt("number_of_people")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groups;
    }

    @Override
    public Group getGroupBySectionId(Integer sectionId) {
        String sql = "SELECT GROUPS.* FROM GROUPS " +
                "JOIN SECTIONS ON GROUPS.ID = GROUP_ID " +
                "WHERE SECTIONS.ID = ? " +
                "LIMIT 1;";
        Group group = null;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, sectionId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                group = new Group(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("semester_id"),
                        resultSet.getInt("number_of_people")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return group;
    }

    @Override
    public Group getGroupBySection(Section section) {
        return this.getGroupBySectionId(section.getId());
    }

    @Override
    public Integer saveGroup(Group group) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (group.getId() == null || this.getGroupById(group.getId()) == null) {
                sql = "INSERT INTO GROUPS (NAME, SEMESTER_ID, NUMBER_OF_PEOPLE) VALUES (?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE GROUPS SET NAME = ?, SEMESTER_ID = ?, NUMBER_OF_PEOPLE = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(4, group.getId());
            }
            preparedStatement.setString(1, group.getName());
            preparedStatement.setInt(2, group.getSemesterId());
            preparedStatement.setInt(3, group.getNumberOfPeople());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error GroupDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer addGroupToSemester(Group group, Semester semester) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            group.setSemesterId(semester.getId());
            if (!this.getGroupsBySemester(semester).contains(group)) {
                sql = "INSERT INTO GROUPS (NAME, SEMESTER_ID, NUMBER_OF_PEOPLE) VALUES (?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setString(1, group.getName());
                preparedStatement.setInt(2, semester.getId());
                preparedStatement.setInt(3, group.getNumberOfPeople());
                return preparedStatement.executeUpdate();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteGroup(Group group) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM GROUPS WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, group.getId());
            preparedStatement.setString(2, group.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteGroupById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM GROUPS WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteGroupsBySemesterId(Integer semesterId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM GROUPS WHERE SEMESTER_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, semesterId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteGroupsBySemester(Semester semester) {
        return this.deleteGroupsBySemesterId(semester.getId());
    }
}
