package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ISectionDao;
import pl.polsl.tukaj.michal.ats.model.Group;
import pl.polsl.tukaj.michal.ats.model.Section;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SectionDao extends DBConnection implements ISectionDao {
    public SectionDao() {
        super();
    }

    @Override
    public List<Section> getAllSections() {
        List<Section> sections = new ArrayList<>();
        String sql = "SELECT * FROM SECTIONS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sections.add(new Section(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        (Integer) resultSet.getObject("group_id"),
                        resultSet.getInt("number_of_people"),
                        (Integer) resultSet.getObject("semester_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sections;
    }

    @Override
    public Section getSectionById(Integer id) {
        Section section = null;
        String sql = "SELECT * FROM SECTIONS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                section = new Section(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        (Integer) resultSet.getObject("group_id"),
                        resultSet.getInt("number_of_people"),
                        (Integer) resultSet.getObject("semester_id")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return section;
    }

    private List<Section> getSectionsBySqlId(String sql, Integer id) {
        List<Section> sections = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sections.add(new Section(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        (Integer) resultSet.getObject("group_id"),
                        resultSet.getInt("number_of_people"),
                        (Integer) resultSet.getObject("semester_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sections;
    }

    @Override
    public List<Section> getSectionsByGroupId(Integer groupId) {
        String sql = "SELECT * FROM SECTIONS WHERE GROUP_ID = ?;";
        return this.getSectionsBySqlId(sql, groupId);
    }

    @Override
    public List<Section> getSectionsByGroup(Group group) {
        return this.getSectionsByGroupId(group.getId());
    }

    @Override
    public List<Section> getSectionsBySemesterId(Integer semesterId) {
        String sql = "SELECT * FROM SECTIONS WHERE SEMESTER_ID = ?;";
        return this.getSectionsBySqlId(sql, semesterId);
    }

    @Override
    public List<Section> getSectionsBySemester(Semester semester) {
        return this.getSectionsBySemesterId(semester.getId());
    }

    @Override
    public List<Section> getSectionsBySubject(Subject subject) {
        String sql = "SELECT SECTIONS.* FROM SECTIONS " +
                "JOIN SUBJECTS_TO_GROUPS ON SECTIONS.ID = SUBJECTS_TO_GROUPS.SECTION_ID " +
                "WHERE SUBJECTS_TO_GROUPS.SUBJECT_ID = ?;";
        List<Section> sections = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, subject.getId());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                sections.add(new Section(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        (Integer) resultSet.getObject("group_id"),
                        resultSet.getInt("number_of_people"),
                        (Integer) resultSet.getObject("semester_id")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sections;
    }

    @Override
    public Integer saveSection(Section section) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (section.getId() == null || this.getSectionById(section.getId()) == null) {
                sql = "INSERT INTO SECTIONS (NAME, GROUP_ID, NUMBER_OF_PEOPLE) VALUES (?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE SECTIONS SET NAME = ?, GROUP_ID = ?, NUMBER_OF_PEOPLE = ? WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(4, section.getId());
            }
            preparedStatement.setString(1, section.getName());
            preparedStatement.setInt(2, section.getGroupId());
            preparedStatement.setInt(3, section.getNumberOfPeople());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error SectionDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer addSectionToGroup(Section section, Group group) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            group.setSemesterId(group.getId());
            if (!this.getSectionsByGroup(group).contains(section)) {
                sql = "INSERT INTO SECTIONS (NAME, GROUP_ID, NUMBER_OF_PEOPLE) VALUES (?, ?, ?)";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setString(1, section.getName());
                preparedStatement.setInt(2, group.getId());
                preparedStatement.setInt(3, section.getNumberOfPeople());
                return preparedStatement.executeUpdate();
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSection(Section section) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SECTIONS WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, section.getId());
            preparedStatement.setString(2, section.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSectionById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SECTIONS WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSectionsByGroupId(Integer groupId) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SECTIONS WHERE GROUP_ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, groupId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSectionsByGroup(Group group) {
        return this.deleteSectionsByGroupId(group.getId());
    }
}
