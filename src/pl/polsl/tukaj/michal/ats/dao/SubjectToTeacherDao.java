package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ISubjectToTeacherDao;
import pl.polsl.tukaj.michal.ats.model.Subject;
import pl.polsl.tukaj.michal.ats.model.SubjectToTeacher;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SubjectToTeacherDao extends DBConnection implements ISubjectToTeacherDao {
    public SubjectToTeacherDao() {
        super();
    }

    @Override
    public List<SubjectToTeacher> getAllSubjectsToTeachers() {
        List<SubjectToTeacher> subjectsToTeachers = new ArrayList<>();
        String sql = "SELECT * FROM SUBJECTS_TO_TEACHERS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                subjectsToTeachers.add(new SubjectToTeacher(
                        resultSet.getInt("id"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("teacher_id"),
                        resultSet.getInt("min_groups_leading"),
                        resultSet.getInt("max_groups_leading")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjectsToTeachers;
    }

    @Override
    public SubjectToTeacher getSubjectToTeacherById(Integer id) {
        SubjectToTeacher subjectToTeacher = null;
        String sql = "SELECT * FROM SUBJECTS_TO_TEACHERS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                subjectToTeacher = new SubjectToTeacher(
                        resultSet.getInt("id"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("teacher_id"),
                        resultSet.getInt("min_groups_leading"),
                        resultSet.getInt("max_groups_leading")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjectToTeacher;
    }

    public List<SubjectToTeacher> getSubjectsToTeachersBySubjectId(Integer subjectId) {
        List<SubjectToTeacher> subjectsToTeachers = new ArrayList<>();
        String sql = "SELECT * FROM SUBJECTS_TO_TEACHERS WHERE SUBJECT_ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, subjectId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                subjectsToTeachers.add(new SubjectToTeacher(
                        resultSet.getInt("id"),
                        resultSet.getInt("subject_id"),
                        resultSet.getInt("teacher_id"),
                        resultSet.getInt("min_groups_leading"),
                        resultSet.getInt("max_groups_leading")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjectsToTeachers;
    }

    public List<SubjectToTeacher> getSubjectsToTeachersBySubject(Subject subject) {
        return this.getSubjectsToTeachersBySubjectId(subject.getId());
    }

    @Override
    public Integer saveSubjectToTeacher(SubjectToTeacher subjectToTeacher) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (subjectToTeacher.getId() == null || this.getSubjectToTeacherById(subjectToTeacher.getId()) == null) {
                sql = "INSERT INTO SUBJECTS_TO_TEACHERS " +
                        "(SUBJECT_ID, TEACHER_ID, MIN_GROUPS_LEADING, MAX_GROUPS_LEADING) " +
                        "VALUES (?, ?, ?, ?);";
                preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            } else {
                sql = "UPDATE SUBJECTS_TO_TEACHERS " +
                        "SET SUBJECT_ID = ?, TEACHER_ID = ?, MIN_GROUPS_LEADING = ?, MAX_GROUPS_LEADING = ? " +
                        "WHERE ID = ?;";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(5, subjectToTeacher.getId());
            }
            preparedStatement.setInt(1, subjectToTeacher.getSubjectId());
            preparedStatement.setInt(2, subjectToTeacher.getTeacherId());
            preparedStatement.setInt(3, subjectToTeacher.getMinGroupsLeading());
            preparedStatement.setInt(4, subjectToTeacher.getMaxGroupsLeading());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error SubjectToTeacherDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSubjectToTeacher(SubjectToTeacher subjectToTeacher) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SUBJECTS_TO_TEACHERS WHERE ID = ? AND SUBJECT_ID = ? AND TEACHER_ID = ?;";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, subjectToTeacher.getId());
            preparedStatement.setInt(2, subjectToTeacher.getSubjectId());
            preparedStatement.setInt(3, subjectToTeacher.getTeacherId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSubjectToTeacherById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SUBJECTS_TO_TEACHERS WHERE ID = ?;";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
