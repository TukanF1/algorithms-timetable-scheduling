package pl.polsl.tukaj.michal.ats.dao;

import pl.polsl.tukaj.michal.ats.dao.interfaces.ISubjectDao;
import pl.polsl.tukaj.michal.ats.model.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SubjectDao extends DBConnection implements ISubjectDao {
    public SubjectDao() {
        super();
    }

    @Override
    public List<Subject> getAllSubjects() {
        List<Subject> subjects = new ArrayList<>();
        String sql = "SELECT * FROM SUBJECTS;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                subjects.add(new Subject(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("duration"),
                        resultSet.getInt("type"),
                        resultSet.getBoolean("every_two_week"),
                        resultSet.getInt("lessons_in_week"),
                        resultSet.getInt("teachers_in_one_lesson")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjects;
    }

    @Override
    public Subject getSubjectById(Integer id) {
        Subject subject = null;
        String sql = "SELECT * FROM SUBJECTS WHERE ID = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                subject = new Subject(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("duration"),
                        resultSet.getInt("type"),
                        resultSet.getBoolean("every_two_week"),
                        resultSet.getInt("lessons_in_week"),
                        resultSet.getInt("teachers_in_one_lesson")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subject;
    }

    @Override
    public Subject getSubjectByName(String name) {
        Subject subject = null;
        String sql = "SELECT * FROM SUBJECTS WHERE NAME = ?;";
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                subject = new Subject(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("duration"),
                        resultSet.getInt("type"),
                        resultSet.getBoolean("every_two_week"),
                        resultSet.getInt("lessons_in_week"),
                        resultSet.getInt("teachers_in_one_lesson")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subject;
    }

    private List<Subject> getSubjectsBySqlAndId(String sql, Integer id) {
        List<Subject> subjects = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                subjects.add(new Subject(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("duration"),
                        resultSet.getInt("type"),
                        resultSet.getBoolean("every_two_week"),
                        resultSet.getInt("lessons_in_week"),
                        resultSet.getInt("teachers_in_one_lesson")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjects;
    }

    @Override
    public List<Subject> getSubjectsByTeacher(Teacher teacher) {
        String sql = "SELECT SUBJECTS.* FROM SUBJECTS " +
                "JOIN SUBJECTS_TO_TEACHERS ON SUBJECTS.ID = SUBJECTS_TO_TEACHERS.SUBJECT_ID " +
                "WHERE SUBJECTS_TO_TEACHERS.TEACHER_ID = ?;";
        return getSubjectsBySqlAndId(sql, teacher.getId());
    }

    @Override
    public List<Subject> getSubjectsByGroup(Group group) {
        String sql = "SELECT SUBJECTS.* FROM SUBJECTS " +
                "JOIN SUBJECTS_TO_GROUPS ON SUBJECTS.ID = SUBJECTS_TO_GROUPS.SUBJECT_ID " +
                "WHERE SUBJECTS_TO_GROUPS.GROUP_ID = ?;";
        return getSubjectsBySqlAndId(sql, group.getId());
    }

    @Override
    public List<Subject> getSubjectsBySection(Section section) {
        String sql = "SELECT SUBJECTS.* FROM SUBJECTS " +
                "JOIN SUBJECTS_TO_GROUPS ON SUBJECTS.ID = SUBJECTS_TO_GROUPS.SUBJECT_ID " +
                "WHERE SUBJECTS_TO_GROUPS.SECTION_ID = ?;";
        return getSubjectsBySqlAndId(sql, section.getId());
    }

    @Override
    public List<Subject> getSubjectsNotAddedByTimetableId(Integer timetableId) {
        String sql = "SELECT SUBJECTS.* FROM SUBJECTS " +
                "JOIN SUBJECTS_NOT_ADDED SNA on SUBJECTS.ID = SNA.SUBJECT_ID " +
                "WHERE TIMETABLE_ID = ?;";
        List<Subject> subjects = new ArrayList<>();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, timetableId);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                subjects.add(new Subject(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("duration"),
                        resultSet.getInt("type"),
                        resultSet.getBoolean("every_two_week"),
                        resultSet.getInt("lessons_in_week"),
                        resultSet.getInt("teachers_in_one_lesson")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return subjects;
    }

    @Override
    public List<Subject> getSubjectsNotAddedByTimetable(Timetable timetable) {
        return this.getSubjectsNotAddedByTimetableId(timetable.getId());
    }

    @Override
    public Integer addSubjectIdToNotAddedInTimetableById(Integer subjectId, Integer timetableId) {
        if (subjectId != null && timetableId != null) {
            String sql = "INSERT INTO SUBJECTS_NOT_ADDED (SUBJECT_ID, TIMETABLE_ID) VALUES (?, ?)";
            PreparedStatement preparedStatement;
            List<Subject> subjectsNotAdded = this.getSubjectsNotAddedByTimetableId(timetableId);
            Subject subject = this.getSubjectById(subjectId);
            if (!subjectsNotAdded.contains(subject)) {
                try {
                    preparedStatement = this.connection.prepareStatement(sql);
                    preparedStatement.setInt(1, subjectId);
                    preparedStatement.setInt(2, timetableId);
                    return preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return 0;
    }

    @Override
    public Integer addSubjectToNotAddedInTimetable(Subject subject, Timetable timetable) {
        return this.addSubjectIdToNotAddedInTimetableById(subject.getId(), timetable.getId());
    }

    @Override
    public Integer saveSubject(Subject subject) {
        PreparedStatement preparedStatement;
        String sql;
        try {
            if (subject.getId() == null || this.getSubjectById(subject.getId()) == null) {
                if (this.getSubjectByName(subject.getName()) == null) {
                    sql = "INSERT INTO SUBJECTS (NAME, DURATION, TYPE, EVERY_TWO_WEEK, LESSONS_IN_WEEK) "
                            + "VALUES (?, ?, ?, ?, ?)";
                    preparedStatement = this.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                } else {
                    return null;
                }
            } else {
                sql = "UPDATE SUBJECTS SET NAME = ?, DURATION = ?, TYPE = ?, EVERY_TWO_WEEK = ?, LESSONS_IN_WEEK = ? "
                        + "WHERE ID = ?";
                preparedStatement = this.connection.prepareStatement(sql);
                preparedStatement.setInt(6, subject.getId());
            }
            preparedStatement.setString(1, subject.getName());
            preparedStatement.setInt(2, (int) subject.getDuration().toMinutes());
            preparedStatement.setInt(3, subject.getType().getValue());
            preparedStatement.setBoolean(4, subject.getEveryTwoWeek());
            preparedStatement.setInt(5, subject.getLessonsInWeek());
            if (preparedStatement.executeUpdate() != 0) {
                try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        return generatedKeys.getInt(1);
                    }
                    else {
                        throw new SQLException("Error SubjectDao. No ID obtained.");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSubjectIdFromNotAddedInTimetableById(Integer subjectId, Integer timetableId) {
        String sql = "DELETE FROM SUBJECTS_NOT_ADDED WHERE SUBJECT_ID = ? AND TIMETABLE_ID = ?";
        PreparedStatement preparedStatement;
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, subjectId);
            preparedStatement.setInt(2, timetableId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSubjectFromNotAddedInTimetable(Subject subject, Timetable timetable) {
        return this.deleteSubjectIdFromNotAddedInTimetableById(subject.getId(), timetable.getId());
    }

    @Override
    public Integer deleteSubject(Subject subject) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SUBJECTS WHERE ID = ? AND NAME = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, subject.getId());
            preparedStatement.setString(2, subject.getName());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Integer deleteSubjectById(Integer id) {
        PreparedStatement preparedStatement;
        String sql = "DELETE FROM SUBJECTS WHERE ID = ?";
        try {
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
