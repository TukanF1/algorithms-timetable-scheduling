package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class FieldOfStudy {
    private Integer id;
    private String name;

    private List<Semester> semesters;

    public FieldOfStudy() {
        this.id = null;
        this.name = null;
        this.semesters = Collections.synchronizedList(new ArrayList<>());
    }

    public FieldOfStudy(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.semesters = Collections.synchronizedList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addSemester(Semester semester) {
        return this.semesters.add(semester);
    }

    public boolean addSemesters(List<Semester> semesters) {
        return this.semesters.addAll(semesters);
    }

    public boolean removeSemester(Semester semester) {
        return this.semesters.remove(semester);
    }

    public void clearSemesters() {
        this.semesters.clear();
    }

    public List<Semester> getSemesters() {
        return semesters;
    }

    @Override
    public String toString() {
        return "Field of study: {\n\tid: " + this.id + ",\n\tname: " + this.name + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FieldOfStudy that = (FieldOfStudy) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
