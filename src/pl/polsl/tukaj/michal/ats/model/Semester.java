package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Semester {
    private Integer id;
    private Integer semester;
    private Integer fieldOfStudyId;

    private FieldOfStudy fieldOfStudy;
    private String fieldOfStudyName;

    private List<Group> groups;
    private List<Section> sections;
    private List<SemesterAvailability> semesterAvailabilities;

    public Semester() {
        this.id = null;
        this.semester = null;
        this.fieldOfStudyId = null;

        this.fieldOfStudy = null;
        this.fieldOfStudyName = null;
        this.groups = Collections.synchronizedList(new ArrayList<>());
        this.sections = Collections.synchronizedList(new ArrayList<>());
        this.semesterAvailabilities = Collections.synchronizedList(new ArrayList<>());
    }

    public Semester(Integer id, Integer semester, Integer fieldOfStudyId) {
        this.id = id;
        this.semester = semester;
        this.fieldOfStudyId = fieldOfStudyId;

        this.fieldOfStudy = null;
        this.groups = Collections.synchronizedList(new ArrayList<>());
        this.sections = Collections.synchronizedList(new ArrayList<>());
        this.semesterAvailabilities = Collections.synchronizedList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSemester() {
        return semester;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    public Integer getFieldOfStudyId() {
        return fieldOfStudyId;
    }

    public void setFieldOfStudyId(Integer fieldOfStudyId) {
        this.fieldOfStudyId = fieldOfStudyId;
    }

    public FieldOfStudy getFieldOfStudy() {
        return fieldOfStudy;
    }

    public void setFieldOfStudy(FieldOfStudy fieldOfStudy) {
        this.fieldOfStudy = fieldOfStudy;
        if (fieldOfStudy != null) {
            this.setFieldOfStudyId(fieldOfStudy.getId());
            this.fieldOfStudyName = fieldOfStudy.getName();
        }
    }

    public String getFieldOfStudyName() {
        return fieldOfStudyName;
    }

    public void setFieldOfStudyName(String fieldOfStudyName) {
        this.fieldOfStudyName = fieldOfStudyName;
    }

    public boolean addGroup(Group group) {
        return this.groups.add(group);
    }

    public boolean addGroups(List<Group> groups) {
        return this.groups.addAll(groups);
    }

    public boolean removeGroup(Group group) {
        return this.groups.remove(group);
    }

    public void clearGroups() {
        this.groups.clear();
    }

    public List<Group> getGroups() {
        return groups;
    }

    public boolean addSection(Section section) {
        return this.sections.add(section);
    }

    public boolean addSections(List<Section> sections) {
        return this.sections.addAll(sections);
    }

    public boolean removeSection(Section section) {
        return this.sections.remove(section);
    }

    public void clearSection() {
        this.sections.clear();
    }

    public List<Section> getSections() {
        return sections;
    }

    public boolean addSemesterAvailability(SemesterAvailability semesterAvailability) {
        return this.semesterAvailabilities.add(semesterAvailability);
    }

    public boolean addSemesterAvailabilities(List<SemesterAvailability> semesterAvailabilities) {
        return this.semesterAvailabilities.addAll(semesterAvailabilities);
    }

    public boolean removeSemesterAvailability(SemesterAvailability semesterAvailability) {
        return this.semesterAvailabilities.remove(semesterAvailability);
    }

    public void clearSemesterAvailabilities() {
        this.semesterAvailabilities.clear();
    }

    public List<SemesterAvailability> getSemesterAvailabilities() {
        return semesterAvailabilities;
    }

    @Override
    public String toString() {
        return "Semester: {\n\tid: " + this.id + ",\n\tsemester: " + this.semester
                + ",\n\tfieldOfStudyId: " + this.fieldOfStudyId + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Semester semester = (Semester) o;

        return Objects.equals(id, semester.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
