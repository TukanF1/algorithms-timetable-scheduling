package pl.polsl.tukaj.michal.ats.model;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Subject {
    private Integer id;
    private String name;
    private Duration duration;
    private SubjectType type;
    private Boolean everyTwoWeek;
    private Integer lessonsInWeek;
    private Integer teachersInOneLesson;

    private List<Section> sections;
    private List<Group> groups;
    private List<Feature> features;
    private List<SubjectToTeacher> subjectsToTeachers;
    private List<Lesson> lessons;
    private List<Timetable> notAddedToTimetables;

    public Subject() {
        this.id = null;
        this.name = null;
        this.duration = null;
        this.type = null;
        this.everyTwoWeek = null;
        this.lessonsInWeek = null;
        this.teachersInOneLesson = null;

        this.sections = Collections.synchronizedList(new ArrayList<>());
        this.groups = Collections.synchronizedList(new ArrayList<>());
        this.features = Collections.synchronizedList(new ArrayList<>());
        this.subjectsToTeachers = Collections.synchronizedList(new ArrayList<>());
        this.lessons = Collections.synchronizedList(new ArrayList<>());
        this.notAddedToTimetables = Collections.synchronizedList(new ArrayList<>());
    }

    public Subject(Integer id, String name, Duration duration, SubjectType type, Boolean everyTwoWeek,
                   Integer lessonsInWeek, Integer teachersInOneLesson) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.type = type;
        this.everyTwoWeek = everyTwoWeek;
        this.lessonsInWeek = lessonsInWeek;
        this.teachersInOneLesson = teachersInOneLesson;

        this.sections = Collections.synchronizedList(new ArrayList<>());
        this.groups = Collections.synchronizedList(new ArrayList<>());
        this.features = Collections.synchronizedList(new ArrayList<>());
        this.subjectsToTeachers = Collections.synchronizedList(new ArrayList<>());
        this.lessons = Collections.synchronizedList(new ArrayList<>());
        this.notAddedToTimetables = Collections.synchronizedList(new ArrayList<>());
    }

    public Subject(Integer id, String name, Integer duration, Integer type, Boolean everyTwoWeek,
                   Integer lessonsInWeek, Integer teachersInOneLesson) {
        this(id, name, Duration.ofMinutes(duration), SubjectType.valueOf(type), everyTwoWeek, lessonsInWeek,
                teachersInOneLesson);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public SubjectType getType() {
        return type;
    }

    public void setType(SubjectType type) {
        this.type = type;
    }

    public Boolean getEveryTwoWeek() {
        return everyTwoWeek;
    }

    public void setEveryTwoWeek(Boolean everyTwoWeek) {
        this.everyTwoWeek = everyTwoWeek;
    }

    public Integer getLessonsInWeek() {
        return lessonsInWeek;
    }

    public void setLessonsInWeek(Integer lessonsInWeek) {
        this.lessonsInWeek = lessonsInWeek;
    }

    public Integer getTeachersInOneLesson() {
        return teachersInOneLesson;
    }

    public void setTeachersInOneLesson(Integer teachersInOneLesson) {
        this.teachersInOneLesson = teachersInOneLesson;
    }

    public boolean addSection(Section section) {
        return this.sections.add(section);
    }

    public boolean addSections(List<Section> sections) {
        return this.sections.addAll(sections);
    }

    public boolean removeSection(Section section) {
        return this.sections.remove(section);
    }

    public void clearSection() {
        this.sections.clear();
    }

    public List<Section> getSections() {
        return sections;
    }

    public boolean addGroup(Group group) {
        return this.groups.add(group);
    }

    public boolean addGroups(List<Group> groups) {
        return this.groups.addAll(groups);
    }

    public boolean removeGroup(Group group) {
        return this.groups.remove(group);
    }

    public void clearGroups() {
        this.groups.clear();
    }

    public List<Group> getGroups() {
        return groups;
    }

    public boolean addFeature(Feature feature) {
        return this.features.add(feature);
    }

    public boolean addFeatures(List<Feature> features) {
        return this.features.addAll(features);
    }

    public boolean removeFeature(Feature feature) {
        return this.features.remove(feature);
    }

    public void clearFeatures() {
        this.features.clear();
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public boolean addSubjectToTeacher(SubjectToTeacher subjectToTeacher) {
        return this.subjectsToTeachers.add(subjectToTeacher);
    }

    public boolean addSubjectToTeachers(List<SubjectToTeacher> subjectsToTeachers) {
        return this.subjectsToTeachers.addAll(subjectsToTeachers);
    }

    public boolean removeSubjectToTeacher(SubjectToTeacher subjectToTeacher) {
        return this.subjectsToTeachers.remove(subjectToTeacher);
    }

    public void clearSubjectsToTeachers(SubjectToTeacher subjectToTeacher) {
        this.subjectsToTeachers.clear();
    }

    public List<SubjectToTeacher> getSubjectsToTeachers() {
        return subjectsToTeachers;
    }

    public boolean addLesson(Lesson lesson) {
        return this.lessons.add(lesson);
    }

    public boolean addLessons(List<Lesson> lessons) {
        return this.lessons.addAll(lessons);
    }

    public boolean removeLesson(Lesson lesson) {
        return this.lessons.remove(lesson);
    }

    public void clearLessons() {
        this.lessons.clear();
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public boolean addNotAddedToTimetable(Timetable timetable) {
        return this.notAddedToTimetables.add(timetable);
    }

    public boolean addNotAddedToTimetables(List<Timetable> timetables) {
        return this.notAddedToTimetables.addAll(timetables);
    }

    public boolean removeNotAddedToTimetable(Timetable timetable) {
        return this.notAddedToTimetables.remove(timetable);
    }

    public void clearNotAddedToTimetables() {
        this.notAddedToTimetables.clear();
    }

    public List<Timetable> getNotAddedToTimetables() {
        return notAddedToTimetables;
    }

    @Override
    public String toString() {
        return "Subject: {\n\tid: " + this.id + ",\n\tname: " + this.name +
                ",\n\tduration: " + Math.floor(this.duration.get(ChronoUnit.SECONDS) / 60.0) +
                ",\n\ttype: " + this.type + ",\n\teveryTwoWeek: " + this.everyTwoWeek +
                ",\n\tlessonsInWeek: " + this.lessonsInWeek +
                ",\n\tteachersInOneLesson: " + this.teachersInOneLesson + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subject subject = (Subject) o;

        return Objects.equals(id, subject.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
