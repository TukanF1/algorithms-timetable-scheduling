package pl.polsl.tukaj.michal.ats.model;

public interface ISection {
    Integer getId();

    void setId(Integer id);

    String getName();

    void setName(String name);

    Integer getNumberOfPeople();

    void setNumberOfPeople(Integer numberOfPeople);
}
