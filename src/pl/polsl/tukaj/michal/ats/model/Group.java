package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Group implements ISection {
    private Integer id;
    private String name;
    private Integer semesterId;
    private Integer numberOfPeople;

    private Semester semester;

    private List<Subject> subjects;
    private List<Section> sections;
    private List<GeneratedSection> generatedSections;

    public Group() {
        this.id = null;
        this.name = null;
        this.semesterId = null;
        this.numberOfPeople = null;

        this.semester = null;
        this.subjects = Collections.synchronizedList(new ArrayList<>());
        this.sections = Collections.synchronizedList(new ArrayList<>());
        this.generatedSections = Collections.synchronizedList(new ArrayList<>());
    }

    public Group(Integer id, String name, Integer semesterId, Integer numberOfPeople) {
        this.id = id;
        this.name = name;
        this.semesterId = semesterId;
        this.numberOfPeople = numberOfPeople;

        this.semester = null;
        this.subjects = Collections.synchronizedList(new ArrayList<>());
        this.sections = Collections.synchronizedList(new ArrayList<>());
        this.generatedSections = Collections.synchronizedList(new ArrayList<>());
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Integer getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(Integer semesterId) {
        this.semesterId = semesterId;
    }

    @Override
    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    @Override
    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
        if (semester != null) {
            this.setSemesterId(semester.getId());
        }
    }

    public boolean addSubject(Subject subject) {
        return this.subjects.add(subject);
    }

    public boolean addSubjects(List<Subject> subjects) {
        return this.subjects.addAll(subjects);
    }

    public boolean removeSubject(Subject subject) {
        return this.subjects.remove(subject);
    }

    public void clearSubjects() {
        this.subjects.clear();
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public boolean addSection(Section section) {
        return this.sections.add(section);
    }

    public boolean addSections(List<Section> sections) {
        return this.sections.addAll(sections);
    }

    public boolean removeSection(Section section) {
        return this.sections.remove(section);
    }

    public void clearSection() {
        this.sections.clear();
    }

    public List<Section> getSections() {
        return sections;
    }

    public boolean addGeneratedSections(List<GeneratedSection> generatedSections) {
        return this.generatedSections.addAll(generatedSections);
    }

    public boolean addGeneratedSection(GeneratedSection generatedSection) {
        return this.generatedSections.add(generatedSection);
    }

    public boolean removeGeneratedSection(GeneratedSection generatedSection) {
        return this.generatedSections.remove(generatedSection);
    }

    public void clearGeneratedSections() {
        this.subjects.clear();
    }

    public List<GeneratedSection> getGeneratedSections() {
        return generatedSections;
    }

    @Override
    public String toString() {
        return "Group: {\n\tid: " + this.id + ",\n\tname: " + this.name
                + ",\n\tsemesterId: " + this.semesterId + ",\n\tnumberOfPeople: " + this.numberOfPeople + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return Objects.equals(id, group.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
