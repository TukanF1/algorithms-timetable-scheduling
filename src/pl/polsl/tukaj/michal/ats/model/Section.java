package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Section implements ISection {
    private Integer id;
    private String name;
    private Integer groupId;
    private Integer numberOfPeople;
    private Integer semesterId;

    private Group group;
    private Semester semester;

    private List<Subject> subjects;
    private List<GeneratedSection> generatedSections;

    public Section() {
        this.id = null;
        this.name = null;
        this.groupId = null;
        this.numberOfPeople = null;
        this.semesterId = null;

        this.group = null;
        this.semester = null;
        this.subjects = Collections.synchronizedList(new ArrayList<>());
        this.generatedSections = Collections.synchronizedList(new ArrayList<>());
    }

    public Section(Integer id, String name, Integer groupId, Integer numberOfPeople, Integer semesterId) {
        this.id = id;
        this.name = name;
        this.groupId = groupId;
        this.numberOfPeople = numberOfPeople;
        this.semesterId = semesterId;

        this.group = null;
        this.semester = null;
        this.subjects = Collections.synchronizedList(new ArrayList<>());
        this.generatedSections = Collections.synchronizedList(new ArrayList<>());
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @Override
    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    @Override
    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public Integer getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(Integer semesterId) {
        this.semesterId = semesterId;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
        if (group != null) {
            this.setGroupId(group.getId());
        }
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
        if (semester != null) {
            this.setSemesterId(semester.getId());
        }
    }

    public boolean addSubject(Subject subject) {
        return this.subjects.add(subject);
    }

    public boolean addSubjects(List<Subject> subjects) {
        return this.subjects.addAll(subjects);
    }

    public boolean removeSubject(Subject subject) {
        return this.subjects.remove(subject);
    }

    public void clearSubjects() {
        this.subjects.clear();
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public boolean addGeneratedSection(GeneratedSection generatedSection) {
        return this.generatedSections.add(generatedSection);
    }

    public boolean addGeneratedSections(List<GeneratedSection> generatedSections) {
        return this.generatedSections.addAll(generatedSections);
    }

    public boolean removeGeneratedSection(GeneratedSection generatedSection) {
        return this.generatedSections.remove(generatedSection);
    }

    public void clearGeneratedSections() {
        this.generatedSections.clear();
    }

    public List<GeneratedSection> getGeneratedSections() {
        return generatedSections;
    }

    @Override
    public String toString() {
        return "Section: {\n\tid: " + this.id + ",\n\tname: " + this.name
                + ",\n\tgroupId: " + this.groupId + ",\n\tnumberOfPeople: " + this.numberOfPeople
                + ",\n\tsemesterId: " + this.semesterId + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Section section = (Section) o;

        return id != null ? id.equals(section.id) : section.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
