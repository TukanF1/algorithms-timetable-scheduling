package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Timetable {
    private Integer id;
    private String name;

    private List<Lesson> lessons;
    private List<Subject> notAddedSubjects;

    public Timetable() {
        this.id = null;
        this.name = null;

        this.lessons = Collections.synchronizedList(new ArrayList<>());
        this.notAddedSubjects = Collections.synchronizedList(new ArrayList<>());
    }

    public Timetable(Integer id, String name) {
        this.id = id;
        this.name = name;

        this.lessons = Collections.synchronizedList(new ArrayList<>());
        this.notAddedSubjects = Collections.synchronizedList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addLesson(Lesson lesson) {
        return this.lessons.add(lesson);
    }

    public boolean removeLesson(Lesson lesson) {
        return this.lessons.remove(lesson);
    }

    public void clearLessons() {
        this.lessons.clear();
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public boolean addNotAddedSubject(Subject subject) {
        return this.notAddedSubjects.add(subject);
    }

    public boolean removeNotAddedSubject(Subject subject) {
        return this.notAddedSubjects.remove(subject);
    }

    public void clearNotAddedSubjects() {
        this.notAddedSubjects.clear();
    }

    public List<Subject> getNotAddedSubjects() {
        return notAddedSubjects;
    }

    @Override
    public String toString() {
        return "Timetable: {\n\tid: " + this.id + ",\n\tname: " + this.name + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Timetable timetable = (Timetable) o;

        return Objects.equals(id, timetable.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
