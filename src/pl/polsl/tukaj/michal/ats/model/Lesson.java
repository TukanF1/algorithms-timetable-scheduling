package pl.polsl.tukaj.michal.ats.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.*;

public class Lesson implements Comparable<Lesson> {
    private Integer id;
    private DayOfWeek weekDay;
    private LocalTime fromHour;
    private LocalTime toHour;
    private Boolean evenWeek;
    private Boolean oddWeek;
    private Integer subjectId;
    private Integer generatedSectionId;
    private Integer roomId;
    private Integer timetableId;

    private String teacherName = "";
    private String subjectName;
    private String sectionName;
    private String roomName;

    private Set<Teacher> teachers;
    private Subject subject;
    private GeneratedSection generatedSection;
    private Room room;
    private Timetable timetable;

    public Lesson() {
        this.id = null;
        this.weekDay = null;
        this.fromHour = null;
        this.toHour = null;
        this.evenWeek = null;
        this.oddWeek = null;
        this.subjectId = null;
        this.generatedSectionId = null;
        this.roomId = null;
        this.timetableId = null;

        this.teachers = Collections.synchronizedSet(new HashSet<>());
        this.subject = null;
        this.generatedSection = null;
        this.room = null;
        this.timetable = null;
    }

    public Lesson(Integer id, DayOfWeek weekDay, LocalTime fromHour, LocalTime toHour, Boolean evenWeek, Boolean oddWeek,
                  Integer subjectId, Integer generatedSectionId, Integer roomId,
                  Integer timetableId) {
        this.id = id;
        this.weekDay = weekDay;
        this.fromHour = fromHour;
        this.toHour = toHour;
        this.evenWeek = evenWeek;
        this.oddWeek = oddWeek;
        this.subjectId = subjectId;
        this.generatedSectionId = generatedSectionId;
        this.roomId = roomId;
        this.timetableId = timetableId;

        this.teachers = new HashSet<>();
        this.subject = null;
        this.generatedSection = null;
        this.room = null;
        this.timetable = null;
    }

    public Lesson(Integer id, Integer weekDayInteger,
                  String fromHourString, String toHourString, Boolean evenWeek, Boolean oddWeek,
                  Integer subjectId, Integer generatedSectionId, Integer roomId,
                  Integer timetableId) {

        this.id = id;
        this.weekDay = DayOfWeek.of(weekDayInteger);
        try {
            this.fromHour = LocalTime.parse(fromHourString);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.fromHour = null;
        }
        try {
            this.toHour = LocalTime.parse(toHourString);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.toHour = null;
        }
        this.evenWeek = evenWeek;
        this.oddWeek = oddWeek;
        this.subjectId = subjectId;
        this.generatedSectionId = generatedSectionId;
        this.roomId = roomId;
        this.timetableId = timetableId;

        this.teachers = new HashSet<>();
        this.subject = null;
        this.generatedSection = null;
        this.room = null;
        this.timetable = null;
    }

    /**
     * Sprawdzenie czy zajęcia mogą potencjalnie ze sobą kolidować.
     * Brany pod uwagę jest wyłącznie informacja o przynależności do sekcji, grupy lub semestru.
     *
     * @param lesson zajęcia z którymi te (this) będą porównywane.
     * @return true jeśli występuje potencjalna kolizja, false w przeciwnym wypadku.
     */
    public boolean isPotentialCollisionWith(Lesson lesson) {
        // Zajęcia nie kolidują same ze sobą.
        if (Objects.equals(this, lesson)) {
            return false;
        }
        // Sekcje związane z tym samym przedmiotem mogą mieć zajęcia o tym samym czasie.
        if (Objects.equals(this.getSubject(), lesson.getSubject())) {
            return false;
        }

        boolean potentialCollision = false;

        Section section = this.getGeneratedSection().getSection();
        Group group = this.getGeneratedSection().getGroup();

        Section sectionOfLesson = lesson.getGeneratedSection().getSection();
        Group groupOfLesson = lesson.getGeneratedSection().getGroup();

        // Zajęcia są podpięte pod sekcję.
        if (section != null) {
            Group groupOfSection = section.getGroup();
            Semester semesterOfSection = section.getSemester();

            // Sekcja zajęciowa jest podpięta pod grupę.
            if (groupOfSection != null) {

                // Zajęcia dla sekcji.
                if (sectionOfLesson != null) {
                    Group groupOfSectionOfLesson = sectionOfLesson.getGroup();
                    Semester semesterOfSectionOfLesson = sectionOfLesson.getSemester();

                    // Zajęcia dla sekcji podpiętej pod grupę.
                    // Kolizja nie występuje.
                    // Sekcja będąca podpięta pod jakąkolwiek grupę może mieć zajęcia o tym samym czasie co
                    // inna sekcja będąca podpięta pod inną albo nawet tą samą grupę
                    // (w tym wypadku studenci powinni wybrać dla każdego przedmiotu takie sekcje,
                    // których zajęcia nie kolidują ze sobą).
                    if (groupOfSectionOfLesson != null) {
                        potentialCollision = true;
                    }
                    // Zajęcia dla sekcji podpiętej pod semestr.
                    // Kolizja występuje jeśli sekcje należą do tego samego semestru.
                    else if (semesterOfSectionOfLesson != null) {
                        potentialCollision = Objects.equals(
                                groupOfSection.getSemester(), semesterOfSectionOfLesson
                        );
                    }
                }
                // Zajęcia dla grupy.
                // Kolizja występuje jeśli sekcja jest podpięta pod tą grupę.
                else if (groupOfLesson != null) {
                    potentialCollision = Objects.equals(groupOfSection, groupOfLesson);
                }
            }
            // Sekcja zajęciowa jest podpięta pod semestr.
            else if (semesterOfSection != null) {

                // Zajęcia dla sekcji.
                if (sectionOfLesson != null) {
                    Group groupOfSectionOfLesson = sectionOfLesson.getGroup();
                    Semester semesterOfSectionOfLesson = sectionOfLesson.getSemester();

                    // Zajęcia dla sekcji podpiętej pod grupę.
                    // Kolizja występuje jeśli sekcja ta należy do tego samego semestru.
                    if (groupOfSectionOfLesson != null) {
                        potentialCollision = Objects.equals(
                                semesterOfSection, groupOfSectionOfLesson.getSemester()
                        );
                    }
                    // Zajęcia dla sekcji podpiętej pod semestr.
                    // Kolizja występuje jeśli sekcje należą do tego samego semestru.
                    else if (semesterOfSectionOfLesson != null) {
                        potentialCollision = Objects.equals(
                                semesterOfSection, semesterOfSectionOfLesson
                        );
                    }
                }
                // Zajęcia dla grupy
                // Kolizja występuje jeśli sekcja i ta grupa należą do tego samego semestru.
                else if (groupOfLesson != null) {
                    potentialCollision = Objects.equals(semesterOfSection, groupOfLesson.getSemester());
                }
            }
        }
        // Zajęcia są podpięte pod grupę.
        else if (group != null) {

            // Zajęcia dla sekcji.
            if (sectionOfLesson != null) {
                Group groupOfSectionOfLesson = sectionOfLesson.getGroup();
                Semester semesterOfSectionOfLesson = sectionOfLesson.getSemester();

                // Zajęcia dla sekcji podpiętej pod grupę.
                // Kolizja występuje jeśli sekcja ta należy do tej samej grupy.
                if (groupOfSectionOfLesson != null) {
                    potentialCollision = Objects.equals(group, groupOfSectionOfLesson);
                }
                // Zajęcia dla sekcji podpiętej pod semestr.
                // Kolizja występuje jeśli grupa i ta sekcja należą do tego samego semestru.
                else if (semesterOfSectionOfLesson != null) {
                    potentialCollision = Objects.equals(group.getSemester(), semesterOfSectionOfLesson);
                }
            }
            // Zajęcia dla grupy
            // Kolizja występuje jeśli są to te same grupy.
            else if (groupOfLesson != null) {
                potentialCollision = Objects.equals(group, groupOfLesson);
            }
        }
        return potentialCollision;
    }

    /**
     * Zwraca zbiór grup powiązanych z tymi zajęciami.
     * @return zbiór powiązanych grup.
     */
    public Set<Group> getCorrelatedGroups() {
        Set<Group> groups = new HashSet<>();

        GeneratedSection generatedSection = this.getGeneratedSection();
        Section section = generatedSection.getSection();
        Group group = generatedSection.getGroup();
        if (section != null && group == null) {
            Semester semester = section.getSemester();
            group = section.getGroup();
            if (group == null && semester != null) {
                groups.addAll(semester.getGroups());
            }
        }
        if (group != null) {
            groups.add(group);
        }

        return groups;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DayOfWeek getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(DayOfWeek weekDay) {
        this.weekDay = weekDay;
    }

    public LocalTime getFromHour() {
        return fromHour;
    }

    public void setFromHour(LocalTime fromHour) {
        this.fromHour = fromHour;
    }

    public LocalTime getToHour() {
        return toHour;
    }

    public void setToHour(LocalTime toHour) {
        this.toHour = toHour;
    }

    public Boolean getEvenWeek() {
        return evenWeek;
    }

    public void setEvenWeek(Boolean evenWeek) {
        this.evenWeek = evenWeek;
    }

    public Boolean getOddWeek() {
        return oddWeek;
    }

    public void setOddWeek(Boolean oddWeek) {
        this.oddWeek = oddWeek;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getGeneratedSectionId() {
        return generatedSectionId;
    }

    public void setGeneratedSectionId(Integer generatedSectionId) {
        this.generatedSectionId = generatedSectionId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getTimetableId() {
        return timetableId;
    }

    public void setTimetableId(Integer timetableId) {
        this.timetableId = timetableId;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
        List<Teacher> teachersList = new ArrayList<>(teachers);
        for (int i = 0; i < teachersList.size(); ++i) {
            if (i > 0) {
                this.teacherName += ", ";
            } else {
                this.teacherName = "";
            }
            this.teacherName += teachersList.get(i).getFullName();
        }
    }

    public void addTeacher(Teacher teacher) {
        if (teacher != null) {
            if (this.teachers.add(teacher)) {
                if (!this.teacherName.isEmpty()) {
                    this.teacherName += ", ";
                }
                this.teacherName += teacher.getFullName();
            }
        } else {
            System.out.println("TEACHER IS NULL! (addTeacher to Lesson).");
        }
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
        if (subject != null) {
            this.setSubjectId(subject.getId());
            this.subjectName = subject.getName();
        }
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public GeneratedSection getGeneratedSection() {
        return generatedSection;
    }

    public void setGeneratedSection(GeneratedSection generatedSection) {
        this.generatedSection = generatedSection;
        if (generatedSection != null) {
            this.setGeneratedSectionId(generatedSection.getId());
            this.sectionName = generatedSection.getName();
        }
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
        if (room != null) {
            this.setRoomId(room.getId());
            this.roomName = room.getName();
        }
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Timetable getTimetable() {
        return timetable;
    }

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
        if (timetable != null) {
            this.setTimetableId(timetable.getId());
        }
    }

    @Override
    public String toString() {
        return "Lesson: {\n\tweekDay: " + this.weekDay
                + ",\n\tfromHour: " + this.fromHour + ",\n\ttoHour: " + this.toHour
                + ",\n\tevenWeek: " + this.evenWeek + ",\n\toddWeek: " + this.oddWeek
                + ",\n\tteachersSize: " + this.teachers.size() + ",\n\tsubjectId: " + this.subjectId
                + ",\n\troomId: " + this.roomId
                + ",\n\tgeneratedSectionId: " + this.generatedSectionId + ",\n\ttimetableId: " + this.timetableId
                + "\n}";
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (!Lesson.class.isAssignableFrom(object.getClass())) {
            return false;
        }

        final Lesson lesson = (Lesson) object;
        if (this.id != null || lesson.id != null) {
            return Objects.equals(this.id, lesson.id);
        }
        return Objects.equals(this.weekDay, lesson.weekDay) && Objects.equals(this.fromHour, lesson.fromHour)
                && Objects.equals(this.toHour, lesson.toHour) && Objects.equals(this.evenWeek, lesson.evenWeek)
                && Objects.equals(this.oddWeek, lesson.oddWeek) && this.teachers.containsAll(lesson.teachers)
                && Objects.equals(this.subjectId, lesson.subjectId)
                && Objects.equals(this.generatedSection, lesson.generatedSection)
                && Objects.equals(this.roomId, lesson.roomId);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (weekDay != null ? weekDay.hashCode() : 0);
        result = 31 * result + (fromHour != null ? fromHour.hashCode() : 0);
        result = 31 * result + (toHour != null ? toHour.hashCode() : 0);
        result = 31 * result + (evenWeek != null ? evenWeek.hashCode() : 0);
        result = 31 * result + (oddWeek != null ? oddWeek.hashCode() : 0);
        result = 31 * result;
        for (Teacher teacher : teachers) {
            result += teacher.hashCode();
        }
        result = 31 * result + (subjectId != null ? subjectId.hashCode() : 0);
        result = 31 * result + (generatedSection != null ? generatedSection.hashCode() : 0);
        result = 31 * result + (roomId != null ? roomId.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Lesson other) {
        int result;

        Section thisSection = this.getGeneratedSection().getSection();
        Group thisGroup = this.getGeneratedSection().getGroup();
        Section otherSection = other.getGeneratedSection().getSection();
        Group otherGroup = other.getGeneratedSection().getGroup();
        Semester thisSemester = null;
        Semester otherSemester = null;

        if (thisSection != null) {
            thisGroup = thisSection.getGroup();
            if (thisGroup != null) {
                thisSemester = thisSection.getGroup().getSemester();
            } else {
                thisSemester = thisSection.getSemester();
            }
        } else if (thisGroup != null) {
            thisSemester = thisGroup.getSemester();
        }

        if (otherSection != null) {
            otherGroup = otherSection.getGroup();
            if (otherGroup != null) {
                otherSemester = otherSection.getGroup().getSemester();
            } else {
                otherSemester = otherSection.getSemester();
            }
        } else if (otherGroup != null) {
            otherSemester = otherGroup.getSemester();
        }

        // Porównanie po semestrach.
        if (Objects.equals(thisSemester, otherSemester)) {
            // Porównanie po grupach
            if (thisGroup == null || otherGroup == null || Objects.equals(thisGroup, otherGroup)) {
                // Porównanie po dniach tygodnia.
                if (Objects.equals(this.weekDay, other.weekDay)) {
                    // Porównanie po godzinie rozpoczęcia zajęć.
                    if (Objects.equals(this.fromHour, other.fromHour)) {
                        result = 0;
                    } else if (this.fromHour.isBefore(other.fromHour)) {
                        result = -1;
                    } else {
                        result = 1;
                    }
                } else {
                    int weekDayResult = this.weekDay.compareTo(other.weekDay);
                    result = Integer.compare(weekDayResult, 0);
                }
            } else {
                result = Integer.compare(thisGroup.getId(), otherGroup.getId());
            }
        } else if (thisSemester != null && otherSemester != null) {
            result = Integer.compare(thisSemester.getId(), otherSemester.getId());
        } else {
            result = 0;
            System.out.println("ERROR ZERO");
        }
        return result;
    }
}
