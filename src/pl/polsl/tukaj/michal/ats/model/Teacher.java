package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Teacher {
    private Integer id;
    private String fullName;
    private Boolean virtual;

    private List<SubjectToTeacher> subjectsToTeachers;
    private List<TeacherAvailability> teacherAvailabilities;
    private List<Lesson> lessons;

    public Teacher() {
        this.id = null;
        this.fullName = null;
        this.virtual = null;

        this.subjectsToTeachers = Collections.synchronizedList(new ArrayList<>());
        this.teacherAvailabilities = Collections.synchronizedList(new ArrayList<>());
        this.lessons = Collections.synchronizedList(new ArrayList<>());
    }

    public Teacher(Integer id, String name, Boolean virtual) {
        this.id = id;
        this.fullName = name;
        this.virtual = virtual;

        this.subjectsToTeachers = Collections.synchronizedList(new ArrayList<>());
        this.teacherAvailabilities = Collections.synchronizedList(new ArrayList<>());
        this.lessons = Collections.synchronizedList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Boolean getVirtual() {
        return virtual;
    }

    public void setVirtual(Boolean virtual) {
        this.virtual = virtual;
    }

    public boolean addSubjectToTeacher(SubjectToTeacher subjectToTeacher) {
        return this.subjectsToTeachers.add(subjectToTeacher);
    }

    public boolean addSubjectsToTeachers(List<SubjectToTeacher> subjectsToTeachers) {
        return this.subjectsToTeachers.addAll(subjectsToTeachers);
    }

    public boolean removeSubjectToTeacher(SubjectToTeacher subjectToTeacher) {
        return this.subjectsToTeachers.remove(subjectToTeacher);
    }

    public void clearSubjectsToTeachers(SubjectToTeacher subjectToTeacher) {
        this.subjectsToTeachers.clear();
    }

    public List<SubjectToTeacher> getSubjectsToTeachers() {
        return subjectsToTeachers;
    }

    public boolean addTeacherAvailability(TeacherAvailability teacherAvailability) {
        return this.teacherAvailabilities.add(teacherAvailability);
    }

    public boolean addTeacherAvailabilities(List<TeacherAvailability> teacherAvailabilities) {
        return this.teacherAvailabilities.addAll(teacherAvailabilities);
    }

    public boolean removeTeacherAvailability(TeacherAvailability teacherAvailability) {
        return this.teacherAvailabilities.remove(teacherAvailability);
    }

    public void clearTeacherAvailabilities() {
        this.teacherAvailabilities.clear();
    }

    public List<TeacherAvailability> getTeacherAvailabilities() {
        return teacherAvailabilities;
    }

    public boolean addLesson(Lesson lesson) {
        return this.lessons.add(lesson);
    }

    public boolean addLessons(List<Lesson> lessons) {
        return this.lessons.addAll(lessons);
    }

    public boolean removeLesson(Lesson lesson) {
        return this.lessons.remove(lesson);
    }

    public void clearLessons() {
        this.lessons.clear();
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    @Override
    public String toString() {
        return "Teacher: {\n\tid: " + this.id + ",\n\tfullName: " + this.fullName +
                ",\n\tvirtual: " + this.virtual + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        return id.equals(teacher.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
