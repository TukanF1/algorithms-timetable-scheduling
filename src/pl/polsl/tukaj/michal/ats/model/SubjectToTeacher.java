package pl.polsl.tukaj.michal.ats.model;

import java.util.Objects;

public class SubjectToTeacher {
    private Integer id;
    private Integer subjectId;
    private Integer teacherId;
    private Integer minGroupsLeading;
    private Integer maxGroupsLeading;

    private Subject subject;
    private Teacher teacher;

    public SubjectToTeacher() {
        this.id = null;
        this.subjectId = null;
        this.teacherId = null;
        this.minGroupsLeading = null;
        this.maxGroupsLeading = null;

        this.subject = null;
        this.teacher = null;
    }

    public SubjectToTeacher(Integer id, Integer subjectId, Integer teacherId,
                            Integer minGroupsLeading, Integer maxGroupsLeading) {
        this.id = id;
        this.subjectId = subjectId;
        this.teacherId = teacherId;
        this.minGroupsLeading = minGroupsLeading;
        this.maxGroupsLeading = maxGroupsLeading;

        this.subject = null;
        this.teacher = null;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Integer subjectId) {
        this.subjectId = subjectId;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getMinGroupsLeading() {
        return minGroupsLeading;
    }

    public void setMinGroupsLeading(Integer minGroupsLeading) {
        this.minGroupsLeading = minGroupsLeading;
    }

    public Integer getMaxGroupsLeading() {
        return maxGroupsLeading;
    }

    public void setMaxGroupsLeading(Integer maxGroupsLeading) {
        this.maxGroupsLeading = maxGroupsLeading;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
        if (subject != null) {
            this.setSubjectId(subject.getId());
        }
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
        if (teacher != null) {
            this.setTeacherId(teacher.getId());
        }
    }

    @Override
    public String toString() {
        return "SubjectToTeacher: {\n\tid: " + this.id + ",\n\tsubjectId: " + this.subjectId +
                ",\n\tteacherId: " + this.teacherId +
                ",\n\tminGroupsLeading: " + this.minGroupsLeading +
                ",\n\tmaxGroupsLeading: " + this.maxGroupsLeading + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectToTeacher that = (SubjectToTeacher) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
