package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class GeneratedSection {
    private Integer id;
    private String name;
    private Integer numberOfPeople;
    private Integer sectionId;
    private Integer groupId;

    private Section section;
    private Group group;

    private List<Lesson> lessons;

    public GeneratedSection() {
        this.id = null;
        this.name = null;
        this.numberOfPeople = null;
        this.sectionId = null;
        this.groupId = null;

        this.section = null;
        this.group = null;
        this.lessons = Collections.synchronizedList(new ArrayList<>());
    }

    public GeneratedSection(Integer id, String name, Integer numberOfPeople, Integer sectionId, Integer groupId) {
        this.id = id;
        this.name = name;
        this.numberOfPeople = numberOfPeople;
        this.sectionId = sectionId;
        this.groupId = groupId;

        this.section = null;
        this.group = null;
        this.lessons = Collections.synchronizedList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(Integer numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
        if (section != null) {
            this.setSectionId(section.getId());
        }
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
        if (group != null) {
            this.setGroupId(group.getId());
        }
    }

    public boolean addLesson(Lesson lesson) {
        return this.lessons.add(lesson);
    }

    public boolean removeLesson(Lesson lesson) {
        return this.lessons.remove(lesson);
    }

    public void clearLessons() {
        this.lessons.clear();
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    @Override
    public String toString() {
        return "GeneratedSection: {\n\tid: " + this.id + ",\n\tname: " + this.name + ",\n\tnumberOfPeople: "
                + this.numberOfPeople + ",\n\tsectionId: " + this.sectionId + ",\n\tgroupId: " + this.groupId + "\n}";
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (!GeneratedSection.class.isAssignableFrom(object.getClass())) {
            return false;
        }

        final GeneratedSection generatedSection = (GeneratedSection) object;
        return (this.id != null && Objects.equals(this.id, generatedSection.id))
                || (Objects.equals(this.name, generatedSection.name)
                && Objects.equals(this.numberOfPeople, generatedSection.numberOfPeople)
                && Objects.equals(this.sectionId, generatedSection.sectionId)
                && Objects.equals(this.groupId, generatedSection.groupId));
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (numberOfPeople != null ? numberOfPeople.hashCode() : 0);
        result = 31 * result + (sectionId != null ? sectionId.hashCode() : 0);
        result = 31 * result + (groupId != null ? groupId.hashCode() : 0);
        return result;
    }
}
