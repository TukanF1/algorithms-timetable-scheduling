package pl.polsl.tukaj.michal.ats.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class SemesterAvailability extends Availability {
    private Integer id;
    private Integer semesterId;

    private Semester semester;

    public SemesterAvailability() {
        super();
        this.id = null;
        this.semesterId = null;
    }

    public SemesterAvailability(Integer id, DayOfWeek weekDay, LocalTime fromHour, LocalTime toHour, Integer semesterId) {
        super(weekDay, fromHour, toHour);
        this.id = id;
        this.semesterId = semesterId;
    }

    public SemesterAvailability(Integer id, Integer weekDay, String fromHour, String toHour, Integer semesterId) {
        this.id = id;
        this.weekDay = DayOfWeek.of(weekDay);
        try {
            this.fromHour = LocalTime.parse(fromHour);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.fromHour = null;
        }
        try {
            this.toHour = LocalTime.parse(toHour);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.toHour = null;
        }
        this.semesterId = semesterId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSemesterId() {
        return semesterId;
    }

    public void setSemesterId(Integer semesterId) {
        this.semesterId = semesterId;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
        if (semester != null) {
            this.setSemesterId(semester.getId());
        }
    }

    @Override
    public String toString() {
        return "SemesterAvailability: {\n\tid: " + this.id + ",\n\tweekDay: " + this.weekDay
                + ",\n\tfromHour: " + this.fromHour + ",\n\ttoHour: " + this.toHour
                + ",\n\tgroupId: " + this.semesterId + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SemesterAvailability that = (SemesterAvailability) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return (id != null ? id.hashCode() : 0);
    }
}
