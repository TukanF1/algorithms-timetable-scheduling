package pl.polsl.tukaj.michal.ats.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class TeacherAvailability extends Availability {
    private Integer id;
    private Integer teacherId;

    private Teacher teacher;

    public TeacherAvailability() {
        super();
        this.id = null;
        this.teacherId = null;

        this.teacher = null;
    }

    public TeacherAvailability(Integer id, DayOfWeek weekDay, LocalTime fromHour, LocalTime toHour, Integer teacherId) {
        super(weekDay, fromHour, toHour);
        this.id = id;
        this.teacherId = teacherId;

        this.teacher = null;
    }

    public TeacherAvailability(Integer id, Integer weekDay, String fromHour, String toHour, Integer teacherId) {
        this.id = id;
        this.weekDay = DayOfWeek.of(weekDay);
        try {
            this.fromHour = LocalTime.parse(fromHour);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.fromHour = null;
        }
        try {
            this.toHour = LocalTime.parse(toHour);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.toHour = null;
        }
        this.teacherId = teacherId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
        if (teacher != null) {
            this.setTeacherId(teacher.getId());
        }
    }

    @Override
    public String toString() {
        return "TeacherAvailability: {\n\tid: " + this.id + ",\n\tweekDay: " + this.weekDay
                + ",\n\tfromHour: " + this.fromHour + ",\n\ttoHour: " + this.toHour
                + ",\n\tteacherId: " + this.teacherId + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TeacherAvailability that = (TeacherAvailability) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return (id != null ? id.hashCode() : 0);
    }
}
