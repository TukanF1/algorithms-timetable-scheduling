package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Room {
    private Integer id;
    private String name;
    private Integer capacity;
    private Boolean virtual;

    private List<Lesson> lessons;
    private List<Feature> features;
    private List<RoomAvailability> roomAvailabilities;

    public Room() {
        this.id = null;
        this.name = null;
        this.capacity = null;
        this.virtual = null;

        this.lessons = Collections.synchronizedList(new ArrayList<>());
        this.features = Collections.synchronizedList(new ArrayList<>());
        this.roomAvailabilities = Collections.synchronizedList(new ArrayList<>());
    }

    public Room(Integer id, String name, Integer capacity, Boolean virtual) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
        this.virtual = virtual;

        this.lessons = Collections.synchronizedList(new ArrayList<>());
        this.features = Collections.synchronizedList(new ArrayList<>());
        this.roomAvailabilities = Collections.synchronizedList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Boolean getVirtual() {
        return virtual;
    }

    public void setVirtual(Boolean virtual) {
        this.virtual = virtual;
    }

    public boolean addLesson(Lesson lesson) {
        return this.lessons.add(lesson);
    }

    public boolean addLessons(List<Lesson> lessons) {
        return this.lessons.addAll(lessons);
    }

    public boolean removeLesson(Lesson lesson) {
        return this.lessons.remove(lesson);
    }

    public void clearLessons() {
        this.lessons.clear();
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public boolean addFeature(Feature feature) {
        return this.features.add(feature);
    }

    public boolean addFeatures(List<Feature> features) {
        return this.features.addAll(features);
    }

    public boolean removeFeature(Feature feature) {
        return this.features.remove(feature);
    }

    public void clearFeatures() {
        this.features.clear();
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public boolean addRoomAvailability(RoomAvailability roomAvailability) {
        return this.roomAvailabilities.add(roomAvailability);
    }

    public boolean addRoomAvailabilities(List<RoomAvailability> roomAvailabilities) {
        return this.roomAvailabilities.addAll(roomAvailabilities);
    }

    public boolean removeRoomAvailability(RoomAvailability roomAvailability) {
        return this.roomAvailabilities.remove(roomAvailability);
    }

    public void clearRoomAvailabilitys() {
        this.roomAvailabilities.clear();
    }

    public List<RoomAvailability> getRoomAvailabilities() {
        return roomAvailabilities;
    }

    @Override
    public String toString() {
        return "Room: {\n\tid: " + this.id + ",\n\tname: " + this.name + ",\n\tcapacity: " + this.capacity +
                ",\n\tvitual: " + this.virtual + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        return id != null ? id.equals(room.id) : room.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
