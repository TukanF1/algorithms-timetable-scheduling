package pl.polsl.tukaj.michal.ats.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;

public class RoomAvailability extends Availability {
    private Integer id;
    private Integer roomId;

    private Room room;

    public RoomAvailability() {
        super();
        this.id = null;
        this.roomId = null;

        this.room = null;
    }

    public RoomAvailability(Integer id, DayOfWeek weekDay, LocalTime fromHour, LocalTime toHour, Integer roomId) {
        super(weekDay, fromHour, toHour);
        this.id = id;
        this.roomId = roomId;

        this.room = null;
    }

    public RoomAvailability(Integer id, Integer weekDay, String fromHour, String toHour, Integer roomId) {

        this.id = id;
        this.weekDay = DayOfWeek.of(weekDay);
        try {
            this.fromHour = LocalTime.parse(fromHour);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.fromHour = null;
        }
        try {
            this.toHour = LocalTime.parse(toHour);
        } catch (DateTimeParseException e) {
            e.printStackTrace();
            this.toHour = null;
        }
        this.roomId = roomId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
        if (room != null) {
            this.setRoomId(room.getId());
        }
    }

    @Override
    public String toString() {
        return "RoomAvailability: {\n\tid: " + this.id + ",\n\tweekDay: " + this.weekDay
                + ",\n\tfromHour: " + this.fromHour + ",\n\ttoHour: " + this.toHour
                + ",\n\troomId: " + this.roomId + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomAvailability that = (RoomAvailability) o;

        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
