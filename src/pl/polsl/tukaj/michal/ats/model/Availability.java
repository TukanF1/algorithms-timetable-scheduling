package pl.polsl.tukaj.michal.ats.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Objects;

public class Availability {
    DayOfWeek weekDay;
    LocalTime fromHour;
    LocalTime toHour;

    public Availability() {
        this.weekDay = null;
        this.fromHour = null;
        this.toHour = null;
    }

    public Availability(DayOfWeek weekDay, LocalTime fromHour, LocalTime toHour) {
        this.weekDay = weekDay;
        this.fromHour = fromHour;
        this.toHour = toHour;
    }

    public DayOfWeek getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(DayOfWeek weekDay) {
        this.weekDay = weekDay;
    }

    public LocalTime getFromHour() {
        return fromHour;
    }

    public void setFromHour(LocalTime fromHour) {
        this.fromHour = fromHour;
    }

    public LocalTime getToHour() {
        return toHour;
    }

    public void setToHour(LocalTime toHour) {
        this.toHour = toHour;
    }

    @Override
    public String toString() {
        return "Availability: {\n\tweekDay: " + this.weekDay + "\n"
                + ",\n\tfromHour: " + this.fromHour + "\n" + ",\n\ttoHour: " + this.toHour + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Availability that = (Availability) o;

        if (weekDay != that.weekDay) return false;
        if (!Objects.equals(fromHour, that.fromHour)) return false;
        return Objects.equals(toHour, that.toHour);
    }

    @Override
    public int hashCode() {
        int result = weekDay != null ? weekDay.hashCode() : 0;
        result = 31 * result + (fromHour != null ? fromHour.hashCode() : 0);
        result = 31 * result + (toHour != null ? toHour.hashCode() : 0);
        return result;
    }
}
