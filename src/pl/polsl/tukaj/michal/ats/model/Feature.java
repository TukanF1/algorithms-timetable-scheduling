package pl.polsl.tukaj.michal.ats.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Feature {
    private Integer id;
    private String name;

    private List<Subject> subjects;
    private List<Room> rooms;

    public Feature() {
        this.id = null;
        this.name = null;

        this.subjects = Collections.synchronizedList(new ArrayList<>());
        this.rooms = Collections.synchronizedList(new ArrayList<>());
    }

    public Feature(Integer id, String name) {
        this.id = id;
        this.name = name;

        this.subjects = Collections.synchronizedList(new ArrayList<>());
        this.rooms = Collections.synchronizedList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addSubject(Subject subject) {
        return this.subjects.add(subject);
    }

    public boolean addSubjects(List<Subject> subjects) {
        return this.subjects.addAll(subjects);
    }

    public boolean removeSubject(Subject subject) {
        return this.subjects.remove(subject);
    }

    public void clearSubjects() {
        this.subjects.clear();
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public boolean addRoom(Room room) {
        return this.rooms.add(room);
    }

    public boolean addRooms(List<Room> rooms) {
        return this.rooms.addAll(rooms);
    }

    public boolean removeRoom(Room room) {
        return this.rooms.remove(room);
    }

    public void clearRooms() {
        this.rooms.clear();
    }

    public List<Room> getRooms() {
        return rooms;
    }

    @Override
    public String toString() {
        return "Feature: {\n\tid: " + this.id + ",\n\tname: " + this.name + "\n}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Feature feature = (Feature) o;

        return Objects.equals(id, feature.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
