package pl.polsl.tukaj.michal.ats.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum typu przedmiotu.
 * LECTURE - wykład.
 * EXERCISE - ćwiczenia tablicowe.
 * LABORATORY - laboratorium.
 * PROJECT - projekt.
 * SEMINAR - seminarium.
 * OTHER - inny.
 */
public enum SubjectType {
    LECTURE(1),
    EXERCISE(2),
    LABORATORY(3),
    PROJECT(4),
    SEMINAR(5),
    OTHER(6);

    private int value;
    private static Map<Integer, SubjectType> map = new HashMap<>();

    SubjectType(int value) {
        this.value = value;
    }

    static {
        for (SubjectType subjectType : SubjectType.values()) {
            map.put(subjectType.value, subjectType);
        }
    }

    public static SubjectType valueOf(int value) {
        return map.get(value);
    }

    public int getValue() {
        return value;
    }
}
