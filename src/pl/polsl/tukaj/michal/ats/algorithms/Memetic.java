package pl.polsl.tukaj.michal.ats.algorithms;

import pl.polsl.tukaj.michal.ats.algorithms.model.Parity;
import pl.polsl.tukaj.michal.ats.algorithms.model.Solution;
import pl.polsl.tukaj.michal.ats.algorithms.model.SubjectWithGroup;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Teacher;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Memetic extends Algorithm {
    private final int generations;
    private final int populationSize;
    private final int innerIterations;
    private final int partOfPopulation;
    private List<Solution> population;
    private Solution bestSolution;

    private final Path logFile = Paths.get("Memetic_" +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")) + ".csv");
    private List<String> lines = new ArrayList<>();

    private Memetic(String timetableName, Integer iterations, Integer sizeOfPopulation,
                    Integer innerIterations, Integer partOfPopulation, List<Semester> semesters) {
        super(timetableName, semesters);
        generations = Math.max(iterations, 1);
        populationSize = Math.max(sizeOfPopulation, 10);
        this.innerIterations = Math.max(innerIterations, 4);
        this.partOfPopulation = Math.max(partOfPopulation, 2);
        population = Collections.synchronizedList(new ArrayList<>(populationSize));
    }

    private void run() {
        System.out.println("MEMETIC STARTED!");

        startTime = LocalDateTime.now();

        for (int i = 0; i < populationSize; ++i) {
            population.add(generateFirstSolution());
        }

        population.parallelStream().forEach(solution -> {
            solution.rate(true, true, true, true);
        });

        Collections.sort(population);

        bestSolution = population.get(0);

        System.out.println("Rate of best solution from the first random generation: " + bestSolution.getRating());

        int iteration = 0;

        for (int i = 0; i < generations && bestSolution.getRating() > 1.5; ++i) {
            population = Genetic.getNewGeneration(population,
                    true, true, true, true);

            Collections.sort(population);

            useTabuSearch(i);

            Collections.shuffle(population);

            useTabuSearch(i);

            Collections.sort(population);

            bestSolution = population.get(0);

            lines.add(++iteration + ";" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + ";" +
                    bestSolution.getRating() + ";" + population.size());

            System.out.println((i + 1) + ": " + bestSolution.getRating() + " : " + population.size());
        }

        endTime = LocalDateTime.now();

        finalSolution = bestSolution;

        System.out.println(iteration + ": " + bestSolution.getRating() + " : " + population.size());

        try {
            Files.write(logFile, lines, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void useTabuSearch(int i) {
        for (int t = 0; t < partOfPopulation && t < population.size(); ++t) {
            int epoche = innerIterations / 4;
            population.get(t).clearRating();
            population.set(t, (Solution) TabuSearch.optimizeSolutionByTabuSearch(
                    epoche, epoche / 2, population.get(t), true
            ).keySet().toArray()[0]);
        }
    }

    static public String generate(String timetableName, Integer iterations, Integer populationSize,
                                  Integer innerIterations, Integer partOfPopulation, List<Semester> semesters) {
        Memetic memetic = new Memetic(timetableName, iterations, populationSize,
                innerIterations, partOfPopulation, semesters);
        memetic.init();
        memetic.run();

        Duration duration = Duration.between(memetic.startTime, memetic.endTime);

        memetic.saveFinalSolutionToDB();

        return String.format("Time table is scheduled with Memetic use.\n"
                        + "Total time of scheduling was %02d:%02d:%02d.%03d.",
                duration.toHours(), duration.toMinutesPart(),
                duration.toSecondsPart(), duration.toNanosPart() / 1_000_000);
    }
}
