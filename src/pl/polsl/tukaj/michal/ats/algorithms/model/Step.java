package pl.polsl.tukaj.michal.ats.algorithms.model;

import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Teacher;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Objects;

/**
 * Reprezentuje pojedynczy krok zmiany w algorytmie.
 * Obiekt tej klasy jest opisem zmiany rozwiązania do rozwiązania sąsiedniego.
 */
public class Step {
    // Informacja, której części rozwiązania dotyczyła zmiana.
    private PartOfSolution partOfSolution;
    // Obiekt starego klucza mapy, pod którym był przechowywany przeniesiony obiekt SubjectWithGroup.
    private Object oldKey;
    // Obiekt nowego klucza mapy, pod który został wstawiony obiekt SubjectWithGroup.
    private Object newKey;

    public Step(PartOfSolution partOfSolution, Object oldKey, Object newKey) {
        this.partOfSolution = partOfSolution;
        this.oldKey = oldKey;
        this.newKey = newKey;
    }

    public PartOfSolution getPartOfSolution() {
        return partOfSolution;
    }

    public Object getOldKey() {
        return oldKey;
    }

    public Object getNewKey() {
        return newKey;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (!Step.class.isAssignableFrom(object.getClass())) {
            return false;
        }

        final Step step = (Step) object;

        if (Objects.equals(this.partOfSolution, step.partOfSolution)) {
            switch (this.partOfSolution) {
                case TEACHERS:
                    if (this.oldKey instanceof Teacher && step.oldKey instanceof Teacher
                            && this.newKey instanceof Teacher && step.newKey instanceof Teacher) {
                        return Objects.equals(this.oldKey, step.oldKey) && Objects.equals(this.newKey, step.newKey);
                    }
                    break;
                case ROOMS:
                    if (this.oldKey instanceof Room && step.oldKey instanceof Room
                            && this.newKey instanceof Room && step.newKey instanceof Room) {
                        return Objects.equals(this.oldKey, step.oldKey) && Objects.equals(this.newKey, step.newKey);
                    }
                    break;
                case START_TIMES:
                    if (this.oldKey instanceof LocalTime && step.oldKey instanceof LocalTime
                            && this.newKey instanceof LocalTime && step.newKey instanceof LocalTime) {
                        return Objects.equals(this.oldKey, step.oldKey) && Objects.equals(this.newKey, step.newKey);
                    }
                    break;
                case WEEK_DAYS:
                    if (this.oldKey instanceof DayOfWeek && step.oldKey instanceof DayOfWeek
                            && this.newKey instanceof DayOfWeek && step.newKey instanceof DayOfWeek) {
                        return Objects.equals(this.oldKey, step.oldKey) && Objects.equals(this.newKey, step.newKey);
                    }
                    break;
                case WEEK_PARITIES:
                    if (this.oldKey instanceof Parity && step.oldKey instanceof Parity
                            && this.newKey instanceof Parity && step.newKey instanceof Parity) {
                        return Objects.equals(this.oldKey, step.oldKey) && Objects.equals(this.newKey, step.newKey);
                    }
                    break;
                default:
                    return false;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = partOfSolution != null ? partOfSolution.hashCode() : 0;
        result = 31 * result + (oldKey != null ? oldKey.hashCode() : 0);
        result = 31 * result + (newKey != null ? newKey.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        switch (this.partOfSolution) {
            case TEACHERS:
                if (this.oldKey instanceof Teacher) {
                    Teacher oldKey = (Teacher) this.oldKey;
                    Teacher newKey = (Teacher) this.newKey;
                    return "oldKey: " + oldKey + "\nnewKey: " + newKey;
                }
                break;
            case ROOMS:
                if (this.oldKey instanceof Room) {
                    Room oldKey = (Room) this.oldKey;
                    Room newKey = (Room) this.newKey;
                    return "oldKey: " + oldKey + "\nnewKey: " + newKey;
                }
                break;
            case START_TIMES:
                if (this.oldKey instanceof LocalTime) {
                    LocalTime oldKey = (LocalTime) this.oldKey;
                    LocalTime newKey = (LocalTime) this.newKey;
                    return "oldKey: " + oldKey + "\nnewKey: " + newKey;
                }
                break;
            case WEEK_DAYS:
                if (this.oldKey instanceof DayOfWeek) {
                    DayOfWeek oldKey = (DayOfWeek) this.oldKey;
                    DayOfWeek newKey = (DayOfWeek) this.newKey;
                    return "oldKey: " + oldKey + "\nnewKey: " + newKey;
                }
                break;
            case WEEK_PARITIES:
                if (this.oldKey instanceof Parity) {
                    Parity oldKey = (Parity) this.oldKey;
                    Parity newKey = (Parity) this.newKey;
                    return "oldKey: " + oldKey + "\nnewKey: " + newKey;
                }
                break;
            default:
                return this.partOfSolution.name();
        }
        return this.partOfSolution.name();
    }
}
