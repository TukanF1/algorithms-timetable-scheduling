package pl.polsl.tukaj.michal.ats.algorithms.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Enum parzystości.
 * EVEN - parzysty.
 * ODD - nieparzysty.
 */
public enum Parity {
    EVEN(true),
    ODD(false);

    private boolean value;
    private static Map<Boolean, Parity> map = new HashMap<>();

    Parity(boolean value) {
        this.value = value;
    }

    static {
        for (Parity parity : Parity.values()) {
            map.put(parity.value, parity);
        }
    }

    public static Parity valueOf(boolean value) {
        return map.get(value);
    }

    public boolean getValue() {
        return value;
    }

    public boolean isEven() {
        return getValue();
    }

    public boolean isOdd() {
        return !getValue();
    }
}
