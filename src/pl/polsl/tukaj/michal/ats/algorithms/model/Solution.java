package pl.polsl.tukaj.michal.ats.algorithms.model;

import pl.polsl.tukaj.michal.ats.algorithms.Algorithm;
import pl.polsl.tukaj.michal.ats.model.*;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Klasa odpowiedzialna za reprezentację rozwiązania.
 * Na jej podstawie obiektu tej klasy powinny być generowane obiekty zajęć (Lesson).
 */
public class Solution implements Comparable<Solution> {
    /**
     * Zajęcia wygenerowane na podstawie tego rozwiązania. Domyślnie null.
     */
    private List<Lesson> lessons;
    /**
     * Zbiór wszystkich obiektów SubjectWithGroup, które reprezentują powiązanie grupy z przedmiotem.
     */
    private Set<SubjectWithGroup> subjectsWithGroups;
    /**
     * Reprezentacja rozwiązania względem nauczycieli.
     */
    private Map<Teacher, Set<SubjectWithGroup>> teachers;
    /**
     * Reprezentacja rozwiązania względem pokoi.
     */
    private Map<Room, Set<SubjectWithGroup>> rooms;
    /**
     * Reprezentacja rozwiązania względem czasu rozpoczęcia zajęć.
     */
    private Map<LocalTime, Set<SubjectWithGroup>> startTimes;
    /**
     * Reprezentacja rozwiązania względem dni tygodnia.
     */
    private Map<DayOfWeek, Set<SubjectWithGroup>> weekDays;
    /**
     * Reprezentacja rozwiązania względem parzystości tygodnia.
     */
    private Map<Parity, Set<SubjectWithGroup>> weekParities;

    /**
     * Łączna liczba złych powiązań nauczyciela z przedmiotem -
     * takich, gdzie nauczyciel nie może prowadzić danego przedmiotu.
     */
    private int numberOfWrongLinksTeacherToSubjects;
    /**
     * Łączna liczba brakujących nauczycieli podczas zajęć.
     * Gdy zajęcia danego przedmiotu mają być prowadzone przez 3 osoby, a jest do ich przypisana jedna to
     * liczba ta jest zwiększana o 2.
     */
    private int numberOfLackTeachers;
    /**
     * Łączny czas w prowadzeniu zajęć dla wszystkich nauczycieli wykraczający poza ich dostępność.
     */
    private Duration timeOfTeachersWithAvailabilityFailure;
    /**
     * Łączny czas nachodzenia na siebie zajęć prowadzonych przez tego samego nauczyciela (nie dotyczy wirtualnego).
     */
    private Duration timeOfTeachersLessonsCollisions;
    /**
     * Łączna liczba prowadzonych sekcji wykraczających poza wymagane minimum lub maksimum.
     * Przykład: gdy nauczyciel powinien prowadzić dwie sekcje z danego przedmiotu, a nie prowadzi żadnej,
     * to wartość ta jest zwiększana o dwa.
     */
    private int sumOfSectionsLeadingByTeacherDifference;
    /**
     * Łączna liczba złych powiązań sali z przedmiotem -
     * takich, gdzie przedmiot nie może się odbywać w danej sali.
     */
    private int numberOfWrongLinksSubjectsToRoom;
    /**
     * Łączny czas nachodzenia na siebie zajęć prowadzonych w tej samej sali (nie dotyczy wirtualnego).
     */
    private Duration timeOfRoomsLessonsCollisions;
    /**
     * Łączny czas odbywania się zajęć dla wszystkich sal wykraczający poza ich dostępność.
     */
    private Duration timeOfRoomsWithAvailabilityFailure;
    /**
     * Suma wszystkich nadwyżek liczby osób w salach, które nie pomieszczą całej sekcji
     * (lub kilku sekcji dla pokoju wirtualnego).
     */
    private int sumOfExcesses;
    /**
     * Łączny czas odbywania się zajęć dla wszystkich sekcji wykraczający poza ich dostępność.
     */
    private Duration timeOfSectionsWithAvailabilityFailure;
    /**
     * Łączny czas nachodzenia na siebie zajęć dla tej samej sekcji.
     */
    private Duration timeSectionsLessonsCollisions;
    /**
     * Średnia liczba dni z zajęciami względem grup.
     */
    private double averageDaysWithLessons;
    /**
     * Średnia godzina rozpoczynania zajęć (minuta dnia).
     */
    private double averageStartTimeOfLessons;

    /**
     * Zmienna przechowująca wartość funkcji oceny tego rozwiązania.
     * Im wartość większa, tym rozwiązanie jest gorsze.
     * Wartość oceny idealnego rozwiązania jest jak najbliższa zeru.
     */
    private double rating = Double.MAX_VALUE;

    private boolean teachersAreRated = false;
    private boolean roomsAreRated = false;
    private boolean sectionsAreRated = false;
    private boolean extrasAreRated = false;

    public Solution() {
        this.teachers = new ConcurrentHashMap<>();
        this.rooms = new ConcurrentHashMap<>();
        this.startTimes = new ConcurrentHashMap<>();
        this.weekDays = new ConcurrentHashMap<>();
        this.weekParities = new ConcurrentHashMap<>();

        this.subjectsWithGroups = Collections.synchronizedSet(new HashSet<>());

        this.lessons = null;

        this.numberOfWrongLinksTeacherToSubjects = 0;
        this.numberOfLackTeachers = 0;
        this.timeOfTeachersWithAvailabilityFailure = Duration.ZERO;
        this.timeOfTeachersLessonsCollisions = Duration.ZERO;
        this.sumOfSectionsLeadingByTeacherDifference = 0;

        this.numberOfWrongLinksSubjectsToRoom = 0;
        this.timeOfRoomsWithAvailabilityFailure = Duration.ZERO;
        this.timeOfRoomsLessonsCollisions = Duration.ZERO;
        this.sumOfExcesses = 0;

        this.timeOfSectionsWithAvailabilityFailure = Duration.ZERO;
        this.timeSectionsLessonsCollisions = Duration.ZERO;

        this.averageDaysWithLessons = 0.;
        this.averageStartTimeOfLessons = 0.;
    }

    /**
     * Wyznacza wartości oceniające przyporządkowanie nauczycieli do wygenerowanych zajęć wg tego rozwiązania.
     */
    private void rateTeachers() {
        if (this.teachersAreRated) {
            return;
        }

        AtomicInteger numberOfWrongLinksToSubjects = new AtomicInteger();
        AtomicInteger numberOfLackTeachers = new AtomicInteger();
        AtomicReference<Duration> timeOfTeachersWithAvailabilityFailure = new AtomicReference<>(Duration.ZERO);
        AtomicReference<Duration> timeOfTeacherLessonsCollisions = new AtomicReference<>(Duration.ZERO);
        AtomicInteger sumOfSectionsLeadingDifference = new AtomicInteger();

        this.lessons.forEach(lesson -> {
            // Czy nauczyciel może prowadzić zajęcia?
            lesson.getTeachers().forEach(teacher -> {
                boolean teacherCanLeading = teacher.getSubjectsToTeachers().stream()
                        .anyMatch(
                                subjectToTeacher -> Objects.equals(lesson.getSubject(), subjectToTeacher.getSubject())
                        );
                if (!teacherCanLeading) {
                    numberOfWrongLinksToSubjects.getAndIncrement();
                }
            });

            // Czy do przedmiotu zostało przypisanych mniej nauczycieli niż jest to wymagane?
            // Przypadek zachodzi jeśli w reprezentacji rozwiązania mapy Teachers do jednego nauczyciela zostało
            // przypisanych kilka tych samych obiektów GroupsWithSubject różniących się tylko kolejnym numerem
            // nauczyciela. Czyli gdy chcielibyśmy do jednych zajęć przypisać tego samego nauczyciela kilka razy.
            // Nie zostanie on przypisany kilka razy przy generowaniu zajęć (lessons), ponieważ jeśli dany nauczyciel
            // jest już przypisany do zajęć to nie może być dodany drugi raz.
            if (lesson.getTeachers().size() < lesson.getSubject().getTeachersInOneLesson()) {
                numberOfLackTeachers.incrementAndGet();
            }

            // Czy prowadzone zajęcia mogą zostać umieszczone w jego grafiku?
            lesson.getTeachers().forEach(teacher -> {
                AtomicReference<Duration> lessonDurationOutOfRange = new AtomicReference<>(
                        Duration.between(lesson.getFromHour(), lesson.getToHour())
                );
                teacher.getTeacherAvailabilities().stream()
                        .filter(
                                teacherAvailability -> Objects
                                        .equals(teacherAvailability.getWeekDay(), lesson.getWeekDay())
                        ).forEach(teacherAvailability -> {
                    Duration timeToSubtract = Duration.ZERO;
                    if (lesson.getFromHour().isBefore(teacherAvailability.getToHour())
                            && lesson.getToHour().isAfter(teacherAvailability.getFromHour())) {
                        if (lesson.getFromHour().isBefore(teacherAvailability.getFromHour())
                                && lesson.getToHour().isAfter(teacherAvailability.getToHour())) {
                            timeToSubtract = timeToSubtract.plus(
                                    Duration.between(teacherAvailability.getFromHour(), teacherAvailability.getToHour())
                            );
                        } else if (lesson.getFromHour().isBefore(teacherAvailability.getFromHour())) {
                            timeToSubtract = timeToSubtract.plus(
                                    Duration.between(teacherAvailability.getFromHour(), lesson.getToHour())
                            );
                        } else if (lesson.getToHour().isAfter(teacherAvailability.getToHour())) {
                            timeToSubtract = timeToSubtract.plus(
                                    Duration.between(lesson.getFromHour(), teacherAvailability.getToHour())
                            );
                        } else {
                            timeToSubtract = Duration.between(lesson.getFromHour(), lesson.getToHour());
                        }
                    }

                    Duration finalTimeToSubtract = timeToSubtract;
                    lessonDurationOutOfRange.getAndUpdate(duration -> {
                        duration = duration.minus(finalTimeToSubtract);
                        if (duration.toMinutes() < 0) {
                            duration = Duration.ZERO;
                        }
                        return duration;
                    });
                });

                timeOfTeachersWithAvailabilityFailure.accumulateAndGet(lessonDurationOutOfRange.get(), Duration::plus);
            });

            // Czy następuje kolizja z innymi zajęciami prowadzonymi przez tego nauczyciela?
            lesson.getTeachers().forEach(teacher -> {
                if (!teacher.getVirtual()) {
                    AtomicReference<Duration> lessonDurationCollisions = new AtomicReference<>(Duration.ZERO);

                    this.lessons.stream().filter(
                            lesson1 -> (lesson1.getTeachers().contains(teacher)
                                    && Objects.equals(lesson.getWeekDay(), lesson1.getWeekDay())
                                    && (Objects.equals(lesson.getEvenWeek(), lesson1.getEvenWeek())
                                    || Objects.equals(lesson.getOddWeek(), lesson1.getOddWeek()))
                                    && !Objects.equals(lesson, lesson1))
                    ).forEach(lesson1 -> {
                        boolean everyWeek = false;
                        if (Objects.equals(lesson.getEvenWeek(), lesson1.getEvenWeek())
                                && Objects.equals(lesson.getOddWeek(), lesson1.getOddWeek())) {
                            everyWeek = true;
                        }

                        Duration timeToAdd = Duration.ZERO;
                        LocalTime lesson1FromHour = lesson1.getFromHour();
                        // Zapewnienie warunku przerwy o długości "time precision" po każdych zajęciach.
                        LocalTime lesson1ToHour = lesson1.getToHour().plus(Algorithm.timePrecision);
                        if (lesson.getFromHour().isBefore(lesson1ToHour)
                                && lesson.getToHour().isAfter(lesson1FromHour)) {
                            if (lesson.getFromHour().isBefore(lesson1FromHour)
                                    && lesson.getToHour().isAfter(lesson1ToHour)) {
                                timeToAdd = timeToAdd.plus(Duration.between(lesson1FromHour, lesson1ToHour));
                            } else if (lesson.getFromHour().isBefore(lesson1FromHour)) {
                                timeToAdd = timeToAdd.plus(Duration.between(lesson1FromHour, lesson.getToHour()));
                            } else if (lesson.getToHour().isAfter(lesson1ToHour)) {
                                timeToAdd = timeToAdd.plus(Duration.between(lesson.getFromHour(), lesson1ToHour));
                            } else {
                                timeToAdd = Duration.between(lesson.getFromHour(), lesson.getToHour());
                            }
                        }
                        if (!everyWeek) {
                            timeToAdd = timeToAdd.dividedBy(2);
                        }

                        lessonDurationCollisions.getAndAccumulate(timeToAdd, Duration::plus);
                    });

                    timeOfTeacherLessonsCollisions.getAndAccumulate(lessonDurationCollisions.get(), Duration::plus);
                }
            });
        });

        // Czy liczba grup (GROUPS) prowadzonych przez nauczyciela z danego przedmiotu spełnia jego wymagania?
        this.teachers.keySet().parallelStream().forEach(teacher -> {
            teacher.getSubjectsToTeachers().forEach(subjectToTeacher -> {
                Integer minGroupsLeading = subjectToTeacher.getMinGroupsLeading();
                Integer maxGroupsLeading = subjectToTeacher.getMaxGroupsLeading();
                if (maxGroupsLeading == null) {
                    maxGroupsLeading = Integer.MAX_VALUE;
                }

                Set<Group> groupsLeading = new HashSet<>();
                this.lessons.stream()
                        .filter(
                                lesson -> Objects.equals(lesson.getSubject(), subjectToTeacher.getSubject())
                                        && lesson.getTeachers().contains(teacher)
                        ).forEach(lesson -> {
                    GeneratedSection generatedSection = lesson.getGeneratedSection();
                    if (generatedSection.getGroup() != null) {
                        groupsLeading.add(generatedSection.getGroup());
                    } else {
                        Section section = generatedSection.getSection();
                        if (section != null) {
                            if (section.getGroup() != null) {
                                groupsLeading.add(section.getGroup());
                            } else if (section.getSemester() != null) {
                                groupsLeading.addAll(section.getSemester().getGroups());
                            }
                        }
                    }
                });
                Integer numberOfGroupsLeading = groupsLeading.size();

                if (numberOfGroupsLeading < minGroupsLeading) {
                    sumOfSectionsLeadingDifference.getAndAdd(minGroupsLeading - numberOfGroupsLeading);
                } else if (numberOfGroupsLeading > maxGroupsLeading) {
                    sumOfSectionsLeadingDifference.getAndAdd(numberOfGroupsLeading - maxGroupsLeading);
                }
            });
        });

        this.numberOfWrongLinksTeacherToSubjects = numberOfWrongLinksToSubjects.get();
        this.numberOfLackTeachers = numberOfLackTeachers.get();
        this.timeOfTeachersWithAvailabilityFailure = timeOfTeachersWithAvailabilityFailure.get();
        this.timeOfTeachersLessonsCollisions = timeOfTeacherLessonsCollisions.get();
        this.sumOfSectionsLeadingByTeacherDifference = sumOfSectionsLeadingDifference.get();

        this.teachersAreRated = true;
    }

    /**
     * Wyznacza wartości oceniające przyporządkowanie wygenerowanych zajęć (wg tego rozwiązania) do sal.
     */
    private void rateRooms() {
        if (this.roomsAreRated) {
            return;
        }

        AtomicInteger numberOfWrongLinksSubjectsToRoom = new AtomicInteger();
        AtomicReference<Duration> timeOfRoomsWithAvailabilityFailure = new AtomicReference<>(Duration.ZERO);
        AtomicReference<Duration> timeOfTeacherLessonsCollisions = new AtomicReference<>(Duration.ZERO);
        AtomicInteger sumOfExcesses = new AtomicInteger();

        this.lessons.parallelStream().forEach(lesson -> {
            // Czy sala spełnia wymagania dla przedmiotu?
            boolean goodRoom = lesson.getRoom().getFeatures().containsAll(lesson.getSubject().getFeatures());
            if (!goodRoom) {
                numberOfWrongLinksSubjectsToRoom.getAndIncrement();
            }

            AtomicReference<Duration> lessonDurationOutOfRange = new AtomicReference<>(
                    Duration.between(lesson.getFromHour(), lesson.getToHour())
            );

            lesson.getRoom().getRoomAvailabilities().stream()
                    .filter(
                            roomAvailability -> Objects.equals(roomAvailability.getWeekDay(), lesson.getWeekDay())
                    ).forEach(roomAvailability -> {
                Duration timeToSubtract = Duration.ZERO;
                if (lesson.getFromHour().isBefore(roomAvailability.getToHour())
                        && lesson.getToHour().isAfter(roomAvailability.getFromHour())) {
                    if (lesson.getFromHour().isBefore(roomAvailability.getFromHour())
                            && lesson.getToHour().isAfter(roomAvailability.getToHour())) {
                        timeToSubtract = timeToSubtract.plus(
                                Duration.between(roomAvailability.getFromHour(), roomAvailability.getToHour())
                        );
                    } else if (lesson.getFromHour().isBefore(roomAvailability.getFromHour())) {
                        timeToSubtract = timeToSubtract.plus(
                                Duration.between(roomAvailability.getFromHour(), lesson.getToHour())
                        );
                    } else if (lesson.getToHour().isAfter(roomAvailability.getToHour())) {
                        timeToSubtract = timeToSubtract.plus(
                                Duration.between(lesson.getFromHour(), roomAvailability.getToHour())
                        );
                    } else {
                        timeToSubtract = Duration.between(lesson.getFromHour(), lesson.getToHour());
                    }
                }

                Duration finalTimeToSubtract = timeToSubtract;
                lessonDurationOutOfRange.getAndUpdate(duration -> {
                    duration = duration.minus(finalTimeToSubtract);
                    if (duration.toMinutes() < 0) {
                        duration = Duration.ZERO;
                    }
                    return duration;
                });
            });

            timeOfRoomsWithAvailabilityFailure.accumulateAndGet(lessonDurationOutOfRange.get(), Duration::plus);

            if (!lesson.getRoom().getVirtual()) {
                // Czy następuje kolizja z innymi zajęciami prowadzonymi w tej samej sali?
                AtomicReference<Duration> lessonDurationCollisions = new AtomicReference<>(Duration.ZERO);

                this.lessons.stream().filter(
                        lesson1 -> Objects.equals(lesson.getRoom(), lesson1.getRoom())
                                && Objects.equals(lesson.getWeekDay(), lesson1.getWeekDay())
                                && (Objects.equals(lesson.getEvenWeek(), lesson1.getEvenWeek())
                                || Objects.equals(lesson.getOddWeek(), lesson1.getOddWeek()))
                                && !Objects.equals(lesson, lesson1)
                ).forEach(lesson1 -> {
                    boolean everyWeek = false;
                    if (Objects.equals(lesson.getEvenWeek(), lesson1.getEvenWeek())
                            && Objects.equals(lesson.getOddWeek(), lesson1.getOddWeek())) {
                        everyWeek = true;
                    }

                    Duration timeToAdd = Duration.ZERO;
                    if (lesson.getFromHour().isBefore(lesson1.getToHour())
                            && lesson.getToHour().isAfter(lesson1.getFromHour())) {
                        if (lesson.getFromHour().isBefore(lesson1.getFromHour())
                                && lesson.getToHour().isAfter(lesson1.getToHour())) {
                            timeToAdd = timeToAdd.plus(Duration.between(lesson1.getFromHour(), lesson1.getToHour()));
                        } else if (lesson.getFromHour().isBefore(lesson1.getFromHour())) {
                            timeToAdd = timeToAdd.plus(Duration.between(lesson1.getFromHour(), lesson.getToHour()));
                        } else if (lesson.getToHour().isAfter(lesson1.getToHour())) {
                            timeToAdd = timeToAdd.plus(Duration.between(lesson.getFromHour(), lesson1.getToHour()));
                        } else {
                            timeToAdd = Duration.between(lesson.getFromHour(), lesson.getToHour());
                        }
                    }
                    if (!everyWeek) {
                        timeToAdd = timeToAdd.dividedBy(2);
                    }

                    lessonDurationCollisions.getAndAccumulate(timeToAdd, Duration::plus);
                });

                timeOfTeacherLessonsCollisions.getAndAccumulate(lessonDurationCollisions.get(), Duration::plus);

                // Czy sala pomieści daną grupę zajęciową?
                Integer numberOfPeople = lesson.getGeneratedSection().getNumberOfPeople();
                Integer capacity = lesson.getRoom().getCapacity();
                if (numberOfPeople > capacity) {
                    sumOfExcesses.getAndAdd(numberOfPeople - capacity);
                }
            }
        });

        // Czy pomieszczenia wirtualne pomieszczą daną grupę zajęciową?
        this.rooms.keySet().parallelStream()
                .filter(Room::getVirtual)
                .forEach(room -> {
                    this.lessons.stream()
                            .filter(lesson -> Objects.equals(room, lesson.getRoom()))
                            .forEach(lesson -> {
                                Integer numberOfPeople = lesson.getGeneratedSection().getNumberOfPeople();
                                Integer capacity = room.getCapacity();

                                if (numberOfPeople > capacity) {
                                    sumOfExcesses.getAndAdd(numberOfPeople - capacity);
                                } else {
                                    AtomicInteger allSectionsSize = new AtomicInteger(numberOfPeople);

                                    this.lessons.stream().filter(
                                            lesson1 -> Objects.equals(room, lesson1.getRoom())
                                                    && Objects.equals(lesson.getWeekDay(), lesson1.getWeekDay())
                                                    && (Objects.equals(lesson.getEvenWeek(), lesson1.getEvenWeek())
                                                    || Objects.equals(lesson.getOddWeek(), lesson1.getOddWeek()))
                                                    && !Objects.equals(lesson, lesson1)
                                    ).forEach(lesson1 -> {
                                        if (lesson.getFromHour().isBefore(lesson1.getToHour())
                                                && lesson.getToHour().isAfter(lesson1.getFromHour())) {
                                            Integer numberOfPeople1 = lesson1.getGeneratedSection().getNumberOfPeople();
                                            allSectionsSize.addAndGet(numberOfPeople1);
                                        }
                                    });

                                    if (allSectionsSize.get() > capacity) {
                                        sumOfExcesses.getAndAdd(allSectionsSize.get() - capacity);
                                    }
                                }
                            });
                });

        this.numberOfWrongLinksSubjectsToRoom = numberOfWrongLinksSubjectsToRoom.get();
        this.timeOfRoomsWithAvailabilityFailure = timeOfRoomsWithAvailabilityFailure.get();
        this.timeOfRoomsLessonsCollisions = timeOfTeacherLessonsCollisions.get();
        this.sumOfExcesses = sumOfExcesses.get();

        this.roomsAreRated = true;
    }

    /**
     * Wyznacza wartości oceniające wygenerowane zajęcia względem sekcji.
     */
    private void rateSections(boolean rateExtras) {
        if ((!rateExtras && this.sectionsAreRated) || (rateExtras && this.sectionsAreRated && this.extrasAreRated)) {
            return;
        }

        AtomicReference<Duration> timeOfSectionsWithAvailabilityFailure = new AtomicReference<>(Duration.ZERO);
        AtomicReference<Duration> timeSectionsLessonsCollisions = new AtomicReference<>(Duration.ZERO);
        AtomicLong sumStartTimeOfLessons = new AtomicLong();
        Map<Group, Set<DayOfWeek>> mapGroupToAverageDaysWithLessons = new ConcurrentHashMap<>();
        long sumDaysWithLessons = 0;

        this.lessons.parallelStream().forEach(lesson -> {
            if (!this.sectionsAreRated) {
                // Czy sekcja może mieć o danym czasie zajęcia?
                Section section = lesson.getGeneratedSection().getSection();
                Group group = lesson.getGeneratedSection().getGroup();
                List<SemesterAvailability> semesterAvailabilities = null;

                AtomicReference<Duration> lessonDurationOutOfRange = new AtomicReference<>(
                        Duration.between(lesson.getFromHour(), lesson.getToHour())
                );

                if (section != null) {
                    Group groupOfSection = section.getGroup();
                    Semester semesterOfSection = section.getSemester();
                    if (groupOfSection != null) {
                        semesterAvailabilities = groupOfSection.getSemester().getSemesterAvailabilities();
                    } else if (semesterOfSection != null) {
                        semesterAvailabilities = semesterOfSection.getSemesterAvailabilities();
                    }
                } else if (group != null) {
                    semesterAvailabilities = group.getSemester().getSemesterAvailabilities();
                }
                if (semesterAvailabilities != null) {
                    semesterAvailabilities.stream()
                            .filter(semesterAvailability -> Objects.equals(
                                    semesterAvailability.getWeekDay(), lesson.getWeekDay()
                            )).forEach(semesterAvailability -> {
                        Duration timeToSubtract = Duration.ZERO;
                        if (lesson.getFromHour().isBefore(semesterAvailability.getToHour())
                                && lesson.getToHour().isAfter(semesterAvailability.getFromHour())) {
                            if (lesson.getFromHour().isBefore(semesterAvailability.getFromHour())
                                    && lesson.getToHour().isAfter(semesterAvailability.getToHour())) {
                                timeToSubtract = timeToSubtract.plus(
                                        Duration.between(
                                                semesterAvailability.getFromHour(), semesterAvailability.getToHour()
                                        )
                                );
                            } else if (lesson.getFromHour().isBefore(semesterAvailability.getFromHour())) {
                                timeToSubtract = timeToSubtract.plus(
                                        Duration.between(semesterAvailability.getFromHour(), lesson.getToHour())
                                );
                            } else if (lesson.getToHour().isAfter(semesterAvailability.getToHour())) {
                                timeToSubtract = timeToSubtract.plus(
                                        Duration.between(lesson.getFromHour(), semesterAvailability.getToHour())
                                );
                            } else {
                                timeToSubtract = Duration.between(lesson.getFromHour(), lesson.getToHour());
                            }
                        }

                        Duration finalTimeToSubtract = timeToSubtract;
                        lessonDurationOutOfRange.getAndUpdate(duration -> {
                            duration = duration.minus(finalTimeToSubtract);
                            if (duration.toMinutes() < 0) {
                                duration = Duration.ZERO;
                            }
                            return duration;
                        });
                    });

                    timeOfSectionsWithAvailabilityFailure.accumulateAndGet(
                            lessonDurationOutOfRange.get(),
                            Duration::plus
                    );
                }

                // Wyliczanie sumy czasu kolizji zajęć względem grup.
                AtomicReference<Duration> lessonDurationCollisions = new AtomicReference<>(Duration.ZERO);

                this.lessons.stream().filter(
                        lesson1 -> Objects.equals(lesson.getWeekDay(), lesson1.getWeekDay())
                                && (Objects.equals(lesson.getEvenWeek(), lesson1.getEvenWeek())
                                || Objects.equals(lesson.getOddWeek(), lesson1.getOddWeek()))
                ).filter(
                        lesson::isPotentialCollisionWith
                ).forEach(lesson1 -> {
                    boolean everyWeek = false;
                    if (Objects.equals(lesson.getEvenWeek(), lesson1.getEvenWeek())
                            && Objects.equals(lesson.getOddWeek(), lesson1.getOddWeek())) {
                        everyWeek = true;
                    }

                    Duration timeToAdd = Duration.ZERO;
                    // Zapewnienie warunku przerwy o długości "time precision" po każdych zajęciach.
                    LocalTime lesson1ToHour = lesson1.getToHour().plus(Algorithm.timePrecision);

                    if (lesson.getFromHour().isBefore(lesson1ToHour)
                            && lesson.getToHour().isAfter(lesson1.getFromHour())) {
                        if (lesson.getFromHour().isBefore(lesson1.getFromHour())
                                && lesson.getToHour().isAfter(lesson1ToHour)) {
                            timeToAdd = timeToAdd.plus(Duration.between(lesson1.getFromHour(), lesson1ToHour));
                        } else if (lesson.getFromHour().isBefore(lesson1.getFromHour())) {
                            timeToAdd = timeToAdd.plus(Duration.between(lesson1.getFromHour(), lesson.getToHour()));
                        } else if (lesson.getToHour().isAfter(lesson1ToHour)) {
                            timeToAdd = timeToAdd.plus(Duration.between(lesson.getFromHour(), lesson1ToHour));
                        } else {
                            timeToAdd = Duration.between(lesson.getFromHour(), lesson.getToHour());
                        }
                    }
                    if (!everyWeek) {
                        timeToAdd = timeToAdd.dividedBy(2);
                    }

                    lessonDurationCollisions.getAndAccumulate(timeToAdd, Duration::plus);
                });

                timeSectionsLessonsCollisions.getAndAccumulate(lessonDurationCollisions.get(), Duration::plus);
            }

            // Wyznacza dodatkowe wartości oceniające wygenerowane zajęcia.
            if (!this.extrasAreRated && rateExtras) {
                // Wyliczanie ile grupa ma średnio dni w tygodniu z zajęciami.
                lesson.getCorrelatedGroups().forEach(correlatedGroup -> {
                    mapGroupToAverageDaysWithLessons.putIfAbsent(
                            correlatedGroup, Collections.synchronizedSet(new HashSet<>())
                    );
                    Set<DayOfWeek> daysOfWeek = mapGroupToAverageDaysWithLessons.get(correlatedGroup);
                    daysOfWeek.add(lesson.getWeekDay());
                });

                // Wyliczanie sumy godziny rozpoczęcia zajęć.
                // Im mniej zajęć o późnych porach tym lepiej.
                sumStartTimeOfLessons.getAndAdd(Duration.between(LocalTime.MIDNIGHT, lesson.getFromHour()).toMinutes());
            }
        });

        this.timeOfSectionsWithAvailabilityFailure = timeOfSectionsWithAvailabilityFailure.get();
        this.timeSectionsLessonsCollisions = timeSectionsLessonsCollisions.get();

        this.sectionsAreRated = true;

        if (!this.extrasAreRated && rateExtras) {
            // Wyliczanie średniej godziny rozpoczęcia zajęć.
            this.averageStartTimeOfLessons = (double) sumStartTimeOfLessons.get() / this.lessons.size();

            // Wyliczanie średniej liczby dni z zajęciami.
            // Lepszą sytuacją jest ułożenie planu z jakimś dniem wolnym dla danej grupy.
            for (Group group : mapGroupToAverageDaysWithLessons.keySet()) {
                sumDaysWithLessons += mapGroupToAverageDaysWithLessons.get(group).size();
            }
            this.averageDaysWithLessons = (double) sumDaysWithLessons
                    / mapGroupToAverageDaysWithLessons.keySet().size();

            this.extrasAreRated = true;
        }
    }

    /**
     * Wyznacza wartość funkcji oceny tego rozwiązania.
     *
     * @param extras parametr informujący czy metoda ma obliczać dodatkowe kryteria jakościowe rozwiązania.
     */
    public void rate(boolean teachers, boolean rooms, boolean sections, boolean extras) {
        if (!(teachers || rooms || sections || extras)) {
            this.rating = 0;
            return;
        }

        int w0 = 10_000;
        int w1 = 5_000;
        int w2 = 4_000;
        int w3 = 1_000;
        int w4 = 500;

        int w5 = 10_000;
        int w6 = 4_000;
        int w7 = 1_000;
        int w8 = 750;

        int w9 = 4_000;
        int w10 = 1_000;

        int w11 = 75;
        int w12 = 20;

        this.getLessons();

        if (!teachers) {
            w0 = 0;
            w1 = 0;
            w2 = 0;
            w3 = 0;
            w4 = 0;
        } else {
            this.rateTeachers();
        }

        if (!rooms) {
            w5 = 0;
            w6 = 0;
            w7 = 0;
            w8 = 0;
        } else {
            this.rateRooms();
        }

        if (!sections) {
            w9 = 0;
            w10 = 0;
        } else {
            this.rateSections(extras);
        }

        if (!extras) {
            w11 = 0;
            w12 = 0;
        }

        double x0 = numberOfWrongLinksTeacherToSubjects * 3;
        double x1 = numberOfLackTeachers * 3;
        double x2 = (double) timeOfTeachersWithAvailabilityFailure.toMinutes() / Algorithm.timePrecision.toMinutes();
        double x3 = (double) timeOfTeachersLessonsCollisions.toMinutes() / Algorithm.timePrecision.toMinutes();
        double x4 = sumOfSectionsLeadingByTeacherDifference;

        double x5 = numberOfWrongLinksSubjectsToRoom * 3;
        double x6 = (double) timeOfRoomsWithAvailabilityFailure.toMinutes() / Algorithm.timePrecision.toMinutes();
        double x7 = (double) timeOfRoomsLessonsCollisions.toMinutes() / Algorithm.timePrecision.toMinutes();
        double x8 = sumOfExcesses * 2;

        double x9 = (double) timeOfSectionsWithAvailabilityFailure.toMinutes() / Algorithm.timePrecision.toMinutes();
        double x10 = (double) timeSectionsLessonsCollisions.toMinutes() / Algorithm.timePrecision.toMinutes();

        double x11 = averageDaysWithLessons;
        double x12 = averageStartTimeOfLessons / 60;

        if (x0 < 0 || x1 < 0 || x2 < 0 || x3 < 0 || x4 < 0 || x5 < 0 || x7 < 0 || x8 < 0 || x9 < 0 || x10 < 0 || x11 < 0
                || x12 < 0) {
            System.out.format("ERROR RATE: %.4f, %.4f; %.4f; %.4f; %.4f; %.4f; %.4f; %.4f; %.4f; %.4f; %.4f; %.4f%n",
                    x0, x1, x2, x3, x4, x5, x7, x8, x9, x10, x11, x12);
        }

        this.rating = (x0 * w0 + x1 * w1 + x2 * w2 + x3 * w3 + x4 * w4 + x5 * w5 + x6 * w6 + x7 * w7 + x8 * w8 + x9 * w9
                + x10 * w10 + x11 * w11 + x12 * w12)
                / (w0 + w1 + w2 + w3 + w4 + w5 + w6 + w7 + w8 + w9 + w10 + w11 + w12);
    }

    /**
     * Zwraca obecną wartość oceny rozwiązania.
     * Przed pierwszym wywołaniem tej metody na danym obiekcie wywołaj metodę rate, która obliczy rating.
     *
     * @return rating typu double.
     */
    public double getRating() {
        return rating;
    }

    /**
     * Generuje listę obiektów zajęć (Lesson) na podstawie tego rozwiązania.
     *
     * @return listę obiektów zajęć wygenerowaną na podstawie tego rozwiązania.
     */
    private List<Lesson> forceGenerateLessons() {
        List<Lesson> lessons = Collections.synchronizedList(new ArrayList<>());

        // Zebranie wszystkich obiektów będących połączeniem przedmiotu z sekcją.
        teachers.values().forEach(subjectsWithGroups::addAll);
        rooms.values().forEach(subjectsWithGroups::addAll);
        startTimes.values().forEach(subjectsWithGroups::addAll);
        weekDays.values().forEach(subjectsWithGroups::addAll);
        weekParities.values().forEach(subjectsWithGroups::addAll);

        // Tworzenie listy obiektów zajęć (Lesson).
        subjectsWithGroups.stream()
                .filter(subjectWithGroup -> subjectWithGroup.getTeacherNextNumber() == 0)
                .forEach(subjectWithGroup -> {
                    Lesson lesson = new Lesson();
                    for (Map.Entry<DayOfWeek, Set<SubjectWithGroup>> entry : weekDays.entrySet()) {
                        DayOfWeek dayOfWeek = entry.getKey();
                        Set<SubjectWithGroup> subjectsWithGroupsList = entry.getValue();
                        if (subjectsWithGroupsList.contains(subjectWithGroup)) {
                            lesson.setWeekDay(dayOfWeek);
                            break;
                        }
                    }

                    Set<SubjectWithGroup> subjectsWithGroups = new HashSet<>();
                    subjectsWithGroups.add(subjectWithGroup);
                    int teachersInOneLesson = subjectWithGroup.getSubject().getTeachersInOneLesson();

                    for (int i = 1; i < teachersInOneLesson; ++i) {
                        SubjectWithGroup subjectWithGroupCopy = new SubjectWithGroup();
                        subjectWithGroupCopy.setSubject(subjectWithGroup.getSubject());
                        subjectWithGroupCopy.setGroup(subjectWithGroup.getGroup());
                        subjectWithGroupCopy.setTeacherNextNumber(i);
                        subjectWithGroupCopy.setLessonInWeekNextNumber(subjectWithGroup.getLessonInWeekNextNumber());
                        subjectsWithGroups.add(subjectWithGroupCopy);
                    }
                    int iterator = 0;
                    for (Map.Entry<Teacher, Set<SubjectWithGroup>> entry : teachers.entrySet()) {
                        Teacher teacher = entry.getKey();
                        Set<SubjectWithGroup> intersection = new HashSet<>(entry.getValue());
                        intersection.retainAll(subjectsWithGroups);
                        for (int j = 0; j < intersection.size(); ++j, ++iterator) {
                            lesson.addTeacher(teacher);
                        }
                        if (iterator >= teachersInOneLesson) {
                            break;
                        }
                    }

                    subjectWithGroup.setTeacherNextNumber(0);
                    for (Map.Entry<Room, Set<SubjectWithGroup>> entry : rooms.entrySet()) {
                        Room room = entry.getKey();
                        Set<SubjectWithGroup> subjectsWithGroupsList = entry.getValue();
                        if (subjectsWithGroupsList.contains(subjectWithGroup)) {
                            lesson.setRoom(room);
                            break;
                        }
                    }
                    for (Map.Entry<LocalTime, Set<SubjectWithGroup>> entry : startTimes.entrySet()) {
                        LocalTime startTime = entry.getKey();
                        Set<SubjectWithGroup> subjectsWithGroupsList = entry.getValue();
                        if (subjectsWithGroupsList.contains(subjectWithGroup)) {
                            lesson.setFromHour(startTime);
                            lesson.setToHour(startTime.plus(subjectWithGroup.getSubject().getDuration()));
                            break;
                        }
                    }
                    boolean everyWeek = true;
                    for (Map.Entry<Parity, Set<SubjectWithGroup>> entry : weekParities.entrySet()) {
                        Parity weekParity = entry.getKey();
                        Set<SubjectWithGroup> subjectsWithGroupsList = entry.getValue();
                        if (subjectsWithGroupsList.contains(subjectWithGroup)) {
                            lesson.setEvenWeek(weekParity.isEven());
                            lesson.setOddWeek(weekParity.isOdd());
                            everyWeek = false;
                            break;
                        }
                    }
                    if (everyWeek) {
                        lesson.setEvenWeek(true);
                        lesson.setOddWeek(true);
                    }
                    lesson.setSubject(subjectWithGroup.getSubject());
                    lesson.setGeneratedSection(subjectWithGroup.getGroup());
                    lessons.add(lesson);
                });

        this.lessons = lessons;
        return lessons;
    }

    /**
     * Przeprowadza na sobie operację mutacji.
     */
    public void mutate() {
        int randomGene = (int) (Math.random() * subjectsWithGroups.size());
        SubjectWithGroup subjectWithGroup = (SubjectWithGroup) subjectsWithGroups.toArray()[randomGene];

        switch ((int) (Math.random() * 5)) {
            case 0:
                mutateTeachers(subjectWithGroup);
                return;
            case 1:
                mutateRooms(subjectWithGroup);
                return;
            case 2:
                mutateStartTimes(subjectWithGroup);
                return;
            case 3:
                mutateWeekDays(subjectWithGroup);
                return;
            case 4:
                mutateWeekParities();
        }
        clearRating();
    }

    public void clearRating() {
        this.teachersAreRated = false;
        this.roomsAreRated = false;
        this.sectionsAreRated = false;
        this.extrasAreRated = false;
        this.rating = Double.MAX_VALUE;
    }

    private void mutateTeachers(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<Teacher, Set<SubjectWithGroup>> entry : teachers.entrySet()) {
            Teacher teacher = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();

            if (subjectsWithGroups.remove(subjectWithGroup)) {
                List<Teacher> teacherList = new ArrayList<>(teachers.keySet());
                Teacher randomTeacher;
                int randomIndex;
                do {
                    randomIndex = (int) (Math.random() * teacherList.size());
                    randomTeacher = teacherList.get(randomIndex);
                } while (Objects.equals(randomTeacher, teacher));

                addSubjectWithGroupToTeacher(randomTeacher, subjectWithGroup);
                return;
            }
        }
    }

    private void mutateRooms(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<Room, Set<SubjectWithGroup>> entry : rooms.entrySet()) {
            Room room = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();

            if (subjectsWithGroups.remove(subjectWithGroup)) {
                List<Room> roomList = new ArrayList<>(rooms.keySet());
                Room randomRoom;
                int randomIndex;
                do {
                    randomIndex = (int) (Math.random() * roomList.size());
                    randomRoom = roomList.get(randomIndex);
                } while (Objects.equals(randomRoom, room));

                addSubjectWithGroupToRoom(randomRoom, subjectWithGroup);
                return;
            }
        }
    }

    private void mutateStartTimes(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<LocalTime, Set<SubjectWithGroup>> entry : startTimes.entrySet()) {
            LocalTime startTime = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();

            if (subjectsWithGroups.remove(subjectWithGroup)) {
                List<LocalTime> startTimesList = new ArrayList<>(startTimes.keySet());
                LocalTime randomStartTime;
                int randomIndex;
                do {
                    randomIndex = (int) (Math.random() * startTimesList.size());
                    randomStartTime = startTimesList.get(randomIndex);
                } while (Objects.equals(randomStartTime, startTime));

                addSubjectWithGroupToStartTime(randomStartTime, subjectWithGroup);
                return;
            }
        }
    }

    private void mutateWeekDays(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<DayOfWeek, Set<SubjectWithGroup>> entry : weekDays.entrySet()) {
            DayOfWeek weekDay = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();

            if (subjectsWithGroups.remove(subjectWithGroup)) {
                List<DayOfWeek> weekDaysList = new ArrayList<>(weekDays.keySet());
                DayOfWeek randomWeekDay;
                int randomIndex;
                do {
                    randomIndex = (int) (Math.random() * weekDaysList.size());
                    randomWeekDay = weekDaysList.get(randomIndex);
                } while (Objects.equals(randomWeekDay, weekDay));

                addSubjectWithGroupToWeekDay(randomWeekDay, subjectWithGroup);
                return;
            }
        }
    }

    private void mutateWeekParities() {
        Set<SubjectWithGroup> subjectsWithGroupsSet;
        List<SubjectWithGroup> subjectsWithGroupsList;
        SubjectWithGroup subjectWithGroup;
        switch ((int) (Math.random() * 2)) {
            case 0:
                subjectsWithGroupsSet = weekParities.get(Parity.EVEN);
                if (!subjectsWithGroupsSet.isEmpty()) {
                    subjectsWithGroupsList = new ArrayList<>(subjectsWithGroupsSet);

                    int randomIndex = (int) (Math.random() * subjectsWithGroupsList.size());
                    subjectWithGroup = subjectsWithGroupsList.get(randomIndex);

                    addSubjectWithGroupToWeekParity(Parity.ODD, subjectWithGroup);
                    if (!subjectsWithGroupsSet.remove(subjectWithGroup)) {
                        System.out.println("ERROR REMOVE FROM EVEN WEEK - 1.");
                    }
                } else if (!(subjectsWithGroupsSet = weekParities.get(Parity.ODD)).isEmpty()) {
                    subjectsWithGroupsList = new ArrayList<>(subjectsWithGroupsSet);

                    int randomIndex = (int) (Math.random() * subjectsWithGroupsList.size());
                    subjectWithGroup = subjectsWithGroupsList.get(randomIndex);

                    addSubjectWithGroupToWeekParity(Parity.EVEN, subjectWithGroup);
                    if (!subjectsWithGroupsSet.remove(subjectWithGroup)) {
                        System.out.println("ERROR REMOVE FROM ODD WEEK - 2.");
                    }
                } else {
                    System.out.println("SETS ARE EMPTY!! - 3");
                }
                return;
            case 1:
                subjectsWithGroupsSet = weekParities.get(Parity.ODD);
                if (!subjectsWithGroupsSet.isEmpty()) {
                    subjectsWithGroupsList = new ArrayList<>(subjectsWithGroupsSet);

                    int randomIndex = (int) (Math.random() * subjectsWithGroupsList.size());
                    subjectWithGroup = subjectsWithGroupsList.get(randomIndex);

                    addSubjectWithGroupToWeekParity(Parity.EVEN, subjectWithGroup);
                    if (!subjectsWithGroupsSet.remove(subjectWithGroup)) {
                        System.out.println("ERROR REMOVE FROM ODD WEEK - 4");
                    }
                } else if (!(subjectsWithGroupsSet = weekParities.get(Parity.EVEN)).isEmpty()) {
                    subjectsWithGroupsList = new ArrayList<>(subjectsWithGroupsSet);

                    int randomIndex = (int) (Math.random() * subjectsWithGroupsList.size());
                    subjectWithGroup = subjectsWithGroupsList.get(randomIndex);

                    addSubjectWithGroupToWeekParity(Parity.ODD, subjectWithGroup);
                    if (!subjectsWithGroupsSet.remove(subjectWithGroup)) {
                        System.out.println("ERROR REMOVE FROM EVEN WEEK - 5");
                    }
                } else {
                    System.out.println("SETS ARE EMPTY!! - 6");
                }
        }
    }

    public List<Lesson> getLessons() {
        if (this.lessons == null) {
            return this.forceGenerateLessons();
        }
        return lessons;
    }

    public List<Lesson> getLessonsAndForceGenerate() {
        return this.forceGenerateLessons();
    }

    public Set<SubjectWithGroup> getSubjectsWithGroups() {
        return subjectsWithGroups;
    }

    public Map<Teacher, Set<SubjectWithGroup>> getTeachers() {
        return teachers;
    }

    public Teacher getTeacherWhereIsSubjectWithGroup(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<Teacher, Set<SubjectWithGroup>> entry : teachers.entrySet()) {
            Teacher teacher = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
            if (subjectsWithGroups.contains(subjectWithGroup)) {
                return teacher;
            }
        }
        return null;
    }

    public Map<Room, Set<SubjectWithGroup>> getRooms() {
        return rooms;
    }

    public Room getRoomWhereIsSubjectWithGroup(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<Room, Set<SubjectWithGroup>> entry : rooms.entrySet()) {
            Room room = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
            if (subjectsWithGroups.contains(subjectWithGroup)) {
                return room;
            }
        }
        return null;
    }

    public LocalTime getStartTimeWhereIsSubjectWithGroup(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<LocalTime, Set<SubjectWithGroup>> entry : startTimes.entrySet()) {
            LocalTime startTime = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
            if (subjectsWithGroups.contains(subjectWithGroup)) {
                return startTime;
            }
        }
        return null;
    }

    public DayOfWeek getWeekDayWhereIsSubjectWithGroup(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<DayOfWeek, Set<SubjectWithGroup>> entry : weekDays.entrySet()) {
            DayOfWeek weekDay = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
            if (subjectsWithGroups.contains(subjectWithGroup)) {
                return weekDay;
            }
        }
        return null;
    }

    public Parity getWeekParityWhereIsSubjectWithGroup(SubjectWithGroup subjectWithGroup) {
        for (Map.Entry<Parity, Set<SubjectWithGroup>> entry : weekParities.entrySet()) {
            Parity weekParity = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
            if (subjectsWithGroups.contains(subjectWithGroup)) {
                return weekParity;
            }
        }
        return null;
    }

    public Map<LocalTime, Set<SubjectWithGroup>> getStartTimes() {
        return startTimes;
    }

    public Map<DayOfWeek, Set<SubjectWithGroup>> getWeekDays() {
        return weekDays;
    }

    public Map<Parity, Set<SubjectWithGroup>> getWeekParities() {
        return weekParities;
    }

    public Duration getTimeOfTeachersLessonsCollisions() {
        return timeOfTeachersLessonsCollisions;
    }

    public Duration getTimeOfRoomsLessonsCollisions() {
        return timeOfRoomsLessonsCollisions;
    }

    public Duration getTimeSectionsLessonsCollisions() {
        return timeSectionsLessonsCollisions;
    }

    public List<Object> removeSubjectWithGroup(SubjectWithGroup subjectWithGroup) {
        List<Object> correlatedObjects = new ArrayList<>();

        for (Map.Entry<Teacher, Set<SubjectWithGroup>> entry : teachers.entrySet()) {
            Teacher teacher = entry.getKey();
            Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
            if (subjectsWithGroups.remove(subjectWithGroup)) {
                correlatedObjects.add(teacher);
                break;
            }
        }

        if (subjectWithGroup.getTeacherNextNumber() == 0) {
            for (Map.Entry<Room, Set<SubjectWithGroup>> entry : rooms.entrySet()) {
                Room room = entry.getKey();
                Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
                if (subjectsWithGroups.remove(subjectWithGroup)) {
                    correlatedObjects.add(room);
                }
            }

            for (Map.Entry<LocalTime, Set<SubjectWithGroup>> entry : startTimes.entrySet()) {
                LocalTime startTime = entry.getKey();
                Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
                if (subjectsWithGroups.remove(subjectWithGroup)) {
                    correlatedObjects.add(startTime);
                    break;
                }
            }

            for (Map.Entry<DayOfWeek, Set<SubjectWithGroup>> entry : weekDays.entrySet()) {
                DayOfWeek weekDay = entry.getKey();
                Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
                if (subjectsWithGroups.remove(subjectWithGroup)) {
                    correlatedObjects.add(weekDay);
                    break;
                }
            }

            for (Map.Entry<Parity, Set<SubjectWithGroup>> entry : weekParities.entrySet()) {
                Parity weekParity = entry.getKey();
                Set<SubjectWithGroup> subjectsWithGroups = entry.getValue();
                if (subjectsWithGroups.remove(subjectWithGroup)) {
                    correlatedObjects.add(weekParity);
                    break;
                }
            }
        }
        return correlatedObjects;
    }

    public void addTeacher(Teacher teacher) {
        teachers.putIfAbsent(teacher, Collections.synchronizedSet(new HashSet<>()));
    }

    public void addTeachers(List<Teacher> teachers) {
        teachers.forEach(this::addTeacher);
    }

    public Set<SubjectWithGroup> deleteTeacher(Teacher teacher) {
        return teachers.remove(teacher);
    }

    public void clearTeachers() {
        teachers.clear();
    }

    public boolean addSubjectWithGroupToTeacher(Teacher teacher, SubjectWithGroup subjectWithGroup) {
        if (!teachers.containsKey(teacher)) {
            this.addTeacher(teacher);
        }
        return teachers.get(teacher).add(subjectWithGroup);
    }

    public boolean addSubjectsWithGroupsToTeacher(Teacher teacher, Set<SubjectWithGroup> subjectsWithGroups) {
        if (!teachers.containsKey(teacher)) {
            this.addTeacher(teacher);
        }
        return teachers.get(teacher).addAll(subjectsWithGroups);
    }

    public void putAllToTeachers(Map<Teacher, Set<SubjectWithGroup>> teachers) {
        teachers.forEach((teacher, subjectsWithGroups) -> {
            Set<SubjectWithGroup> subjectsWithGroupsList = Collections.synchronizedSet(new HashSet<>());
            try {
                this.teachers.putIfAbsent(
                        teacher,
                        subjectsWithGroupsList
                );
                subjectsWithGroupsList.addAll(subjectsWithGroups);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean removeSubjectWithGroupFromTeacher(Teacher teacher, SubjectWithGroup subjectWithGroup) {
        if (teachers.containsKey(teacher)) {
            return teachers.get(teacher).remove(subjectWithGroup);
        }
        return false;
    }

    public void clearSubjectsWithGroupsInTeacher(Teacher teacher, SubjectWithGroup subjectWithGroup) {
        if (teachers.containsKey(teacher)) {
            teachers.get(teacher).clear();
        }
    }

    public void addRoom(Room room) {
        rooms.putIfAbsent(room, Collections.synchronizedSet(new HashSet<>()));
    }

    public void addRooms(List<Room> rooms) {
        rooms.forEach(this::addRoom);
    }

    public Set<SubjectWithGroup> deleteRoom(Room room) {
        return rooms.remove(room);
    }

    public void clearRooms() {
        rooms.clear();
    }

    public boolean addSubjectWithGroupToRoom(Room room, SubjectWithGroup subjectWithGroup) {
        if (!rooms.containsKey(room)) {
            this.addRoom(room);
        }
        return rooms.get(room).add(subjectWithGroup);
    }

    public boolean addSubjectsWithGroupsToRoom(Room room, Set<SubjectWithGroup> subjectsWithGroups) {
        if (!rooms.containsKey(room)) {
            this.addRoom(room);
        }
        return rooms.get(room).addAll(subjectsWithGroups);
    }

    public void putAllToRooms(Map<Room, Set<SubjectWithGroup>> rooms) {
        rooms.forEach((room, subjectsWithGroups) -> {
            Set<SubjectWithGroup> subjectsWithGroupsList = Collections.synchronizedSet(new HashSet<>());
            try {
                this.rooms.putIfAbsent(
                        room,
                        subjectsWithGroupsList
                );
                subjectsWithGroupsList.addAll(subjectsWithGroups);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean removeSubjectWithGroupFromRoom(Room room, SubjectWithGroup subjectWithGroup) {
        if (rooms.containsKey(room)) {
            return rooms.get(room).remove(subjectWithGroup);
        }
        return false;
    }

    public void clearSubjectsWithGroupsInRoom(Room room, SubjectWithGroup subjectWithGroup) {
        if (rooms.containsKey(room)) {
            rooms.get(room).clear();
        }
    }

    public void addStartTime(LocalTime startTime) {
        startTimes.putIfAbsent(startTime, Collections.synchronizedSet(new HashSet<>()));
    }

    public void addStartTimes(List<LocalTime> startTimes) {
        startTimes.forEach(this::addStartTime);
    }

    public Set<SubjectWithGroup> deleteStartTime(LocalTime startTime) {
        return startTimes.remove(startTime);
    }

    public void clearStartTimes() {
        startTimes.clear();
    }

    public boolean addSubjectWithGroupToStartTime(LocalTime startTime, SubjectWithGroup subjectWithGroup) {
        if (!startTimes.containsKey(startTime)) {
            this.addStartTime(startTime);
        }
        // Dla prawidłowego działania programu zajęcia nie mogą się kończyć o północy lub później.
        // Jeśli takie zajęcie chce się wstawić to jest przesuwane "w górę" tak by nie kończyło się później niż
        // o 24:00 minus timePrecision (w domyśle 15 min., czyli domyślnie nie później niż o 23.45).
        if (startTime.plus(subjectWithGroup.getSubject().getDuration()).isBefore(startTime)) {
            return addSubjectWithGroupToStartTime(startTime.minus(Algorithm.timePrecision), subjectWithGroup);
        }
        return startTimes.get(startTime).add(subjectWithGroup);
    }

    public void addSubjectsWithGroupsToStartTime(LocalTime startTime, Set<SubjectWithGroup> subjectsWithGroups) {
        if (!startTimes.containsKey(startTime)) {
            this.addStartTime(startTime);
        }
        subjectsWithGroups.forEach(subjectWithGroup -> addSubjectWithGroupToStartTime(startTime, subjectWithGroup));
    }

    public void putAllToStartTimes(Map<LocalTime, Set<SubjectWithGroup>> startTimes) {
        startTimes.forEach((startTime, subjectsWithGroups) -> {
            Set<SubjectWithGroup> subjectsWithGroupsList = Collections.synchronizedSet(new HashSet<>());
            try {
                this.startTimes.putIfAbsent(
                        startTime,
                        subjectsWithGroupsList
                );
                subjectsWithGroupsList.addAll(subjectsWithGroups);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean removeSubjectWithGroupFromStartTime(LocalTime startTime, SubjectWithGroup subjectWithGroup) {
        if (startTimes.containsKey(startTime)) {
            return startTimes.get(startTime).remove(subjectWithGroup);
        }
        return false;
    }

    public void clearSubjectsWithGroupsInStartTime(LocalTime startTime, SubjectWithGroup subjectWithGroup) {
        if (startTimes.containsKey(startTime)) {
            startTimes.get(startTime).clear();
        }
    }

    public void addWeekDay(DayOfWeek weekDay) {
        weekDays.putIfAbsent(weekDay, Collections.synchronizedSet(new HashSet<>()));
    }

    public void addWeekDays(List<DayOfWeek> weekDays) {
        weekDays.forEach(this::addWeekDay);
    }

    public Set<SubjectWithGroup> deleteWeekDay(DayOfWeek weekDay) {
        return weekDays.remove(weekDay);
    }

    public void clearWeekDays() {
        weekDays.clear();
    }

    public boolean addSubjectWithGroupToWeekDay(DayOfWeek weekDay, SubjectWithGroup subjectWithGroup) {
        if (!weekDays.containsKey(weekDay)) {
            this.addWeekDay(weekDay);
        }
        return weekDays.get(weekDay).add(subjectWithGroup);
    }

    public boolean addSubjectsWithGroupsToWeekDay(DayOfWeek weekDay, Set<SubjectWithGroup> subjectsWithGroups) {
        if (!weekDays.containsKey(weekDay)) {
            this.addWeekDay(weekDay);
        }
        return weekDays.get(weekDay).addAll(subjectsWithGroups);
    }

    public void putAllToWeekDays(Map<DayOfWeek, Set<SubjectWithGroup>> weekDays) {
        weekDays.forEach((weekDay, subjectsWithGroups) -> {
            Set<SubjectWithGroup> subjectsWithGroupsList = Collections.synchronizedSet(new HashSet<>());
            try {
                this.weekDays.putIfAbsent(
                        weekDay,
                        subjectsWithGroupsList
                );
                subjectsWithGroupsList.addAll(subjectsWithGroups);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean removeSubjectWithGroupFromWeekDay(DayOfWeek weekDay, SubjectWithGroup subjectWithGroup) {
        if (weekDays.containsKey(weekDay)) {
            return weekDays.get(weekDay).remove(subjectWithGroup);
        }
        return false;
    }

    public void clearSubjectsWithGroupsInWeekDay(DayOfWeek weekDay, SubjectWithGroup subjectWithGroup) {
        if (weekDays.containsKey(weekDay)) {
            weekDays.get(weekDay).clear();
        }
    }

    public void addWeekParity(Parity parity) {
        weekParities.putIfAbsent(parity, Collections.synchronizedSet(new HashSet<>()));
    }

    public void addWeekParities(List<Parity> parities) {
        parities.forEach(this::addWeekParity);
    }

    public Set<SubjectWithGroup> deleteWeekParity(Parity parity) {
        return weekParities.remove(parity);
    }

    public boolean addSubjectWithGroupToWeekParity(Parity weekParity, SubjectWithGroup subjectWithGroup) {
        if (!weekParities.containsKey(weekParity)) {
            this.addWeekParity(weekParity);
        }
        return weekParities.get(weekParity).add(subjectWithGroup);
    }

    public boolean addSubjectsWithGroupsToWeekParity(Parity weekParity, List<SubjectWithGroup> subjectsWithGroups) {
        if (!weekParities.containsKey(weekParity)) {
            this.addWeekParity(weekParity);
        }
        return weekParities.get(weekParity).addAll(subjectsWithGroups);
    }

    public void putAllToWeekParities(Map<Parity, Set<SubjectWithGroup>> weekParities) {
        weekParities.forEach((weekParity, subjectsWithGroups) -> {
            Set<SubjectWithGroup> subjectsWithGroupsList = Collections.synchronizedSet(new HashSet<>());
            try {
                this.weekParities.putIfAbsent(
                        weekParity,
                        subjectsWithGroupsList
                );
                subjectsWithGroupsList.addAll(subjectsWithGroups);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    public boolean removeSubjectWithGroupFromWeekParity(Parity weekParity, SubjectWithGroup subjectWithGroup) {
        if (weekParities.containsKey(weekParity)) {
            return weekParities.get(weekParity).remove(subjectWithGroup);
        }
        return false;
    }

    public void clearSubjectsWithGroupsInWeekParity(Parity weekParity, SubjectWithGroup subjectWithGroup) {
        if (weekParities.containsKey(weekParity)) {
            weekParities.get(weekParity).clear();
        }
    }

    public Solution getCopy() {
        Solution solution = new Solution();

        solution.putAllToTeachers(this.teachers);
        solution.putAllToRooms(this.rooms);
        solution.putAllToStartTimes(this.startTimes);
        solution.putAllToWeekDays(this.weekDays);
        solution.putAllToWeekParities(this.weekParities);

        return solution;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("{");
        this.getTeachers().forEach((teacher, subjectsWithGroups) -> {
            stringBuilder.append("\n\tTeacher(")
                    .append(teacher.getId())
                    .append(", ")
                    .append(teacher.getFullName())
                    .append("): {\n\t\t");
            stringBuilder.append("subjectsWithGroups: {");
            subjectsWithGroups.forEach(subjectWithGroup -> {
                stringBuilder.append("\n\t\t\t{\n\t\t\t\tSubject: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getSubject().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getSubject().getName())
                        .append("\n\t\t\t\t},\n\t\t\t\t");
                stringBuilder.append("GeneratedSection: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getGroup().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getGroup().getName())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("numberOfPeople: ")
                        .append(subjectWithGroup.getGroup().getNumberOfPeople())
                        .append("\n\t\t\t},");
            });
            stringBuilder.append("\n\t\t},\n\t},");
        });
        stringBuilder.append("\n}\n");

        stringBuilder.append("\n");
        stringBuilder.append("{");
        this.getRooms().forEach((room, subjectsWithGroups) -> {
            stringBuilder.append("\n\tRoom(")
                    .append(room.getId())
                    .append(", ")
                    .append(room.getName())
                    .append(", ")
                    .append(room.getCapacity())
                    .append("): {\n\t\t");
            stringBuilder.append("subjectsWithGroups: {");
            subjectsWithGroups.forEach(subjectWithGroup -> {
                stringBuilder.append("\n\t\t\t{\n\t\t\t\tSubject: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getSubject().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getSubject().getName())
                        .append("\n\t\t\t\t},\n\t\t\t\t");
                stringBuilder.append("GeneratedSection: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getGroup().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getGroup().getName())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("numberOfPeople: ")
                        .append(subjectWithGroup.getGroup().getNumberOfPeople())
                        .append("\n\t\t\t},");
            });
            stringBuilder.append("\n\t\t},\n\t},");
        });
        stringBuilder.append("\n}\n");

        stringBuilder.append("\n");
        stringBuilder.append("{");
        this.getStartTimes().forEach((startTime, subjectsWithGroups) -> {
            stringBuilder.append("\n\tStartTime(")
                    .append(startTime)
                    .append("): {\n\t\t");
            stringBuilder.append("subjectsWithGroups: {");
            subjectsWithGroups.forEach(subjectWithGroup -> {
                stringBuilder.append("\n\t\t\t{\n\t\t\t\tSubject: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getSubject().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getSubject().getName())
                        .append("\n\t\t\t\t},\n\t\t\t\t");
                stringBuilder.append("GeneratedSection: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getGroup().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getGroup().getName())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("numberOfPeople: ")
                        .append(subjectWithGroup.getGroup().getNumberOfPeople())
                        .append("\n\t\t\t},");
            });
            stringBuilder.append("\n\t\t},\n\t},");
        });
        stringBuilder.append("\n}\n");

        stringBuilder.append("\n");
        stringBuilder.append("{");
        this.getWeekDays().forEach((weekDay, subjectsWithGroups) -> {
            stringBuilder.append("\n\tWeekDay(")
                    .append(weekDay)
                    .append("): {\n\t\t");
            stringBuilder.append("subjectsWithGroups: {");
            subjectsWithGroups.forEach(subjectWithGroup -> {
                stringBuilder.append("\n\t\t\t{\n\t\t\t\tSubject: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getSubject().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getSubject().getName())
                        .append("\n\t\t\t\t},\n\t\t\t\t");
                stringBuilder.append("GeneratedSection: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getGroup().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getGroup().getName())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("numberOfPeople: ")
                        .append(subjectWithGroup.getGroup().getNumberOfPeople())
                        .append("\n\t\t\t},");
            });
            stringBuilder.append("\n\t\t},\n\t},");
        });
        stringBuilder.append("\n}\n");

        stringBuilder.append("\n");
        stringBuilder.append("{");
        this.getWeekParities().forEach((weekParity, subjectsWithGroups) -> {
            stringBuilder.append("\n\tWeekParities(")
                    .append(weekParity)
                    .append("): {\n\t\t");
            stringBuilder.append("subjectsWithGroups: {");
            subjectsWithGroups.forEach(subjectWithGroup -> {
                stringBuilder.append("\n\t\t\t{\n\t\t\t\tSubject: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getSubject().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getSubject().getName())
                        .append("\n\t\t\t\t},\n\t\t\t\t");
                stringBuilder.append("GeneratedSection: {\n\t\t\t\t\t");
                stringBuilder.append("id: ")
                        .append(subjectWithGroup.getGroup().getId())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("name: ")
                        .append(subjectWithGroup.getGroup().getName())
                        .append(",\n\t\t\t\t\t");
                stringBuilder.append("numberOfPeople: ")
                        .append(subjectWithGroup.getGroup().getNumberOfPeople())
                        .append("\n\t\t\t},");
            });
            stringBuilder.append("\n\t\t},\n\t},");
        });
        stringBuilder.append("\n}\n");

        stringBuilder.append("\n");
        stringBuilder.append("Teachers with number of connected subjects:\n");
        this.getTeachers().forEach((teacher, subjectsWithGroups) -> {
            stringBuilder.append(teacher.getFullName())
                    .append(": ")
                    .append(subjectsWithGroups.size())
                    .append("\n");
        });

        stringBuilder.append("\n");
        stringBuilder.append("Rooms with number of connected subjects:\n");
        this.getRooms().forEach((room, subjectsWithGroups) -> {
            stringBuilder.append(room.getName())
                    .append(": ")
                    .append(subjectsWithGroups.size())
                    .append("\n");
        });

        stringBuilder.append("\n");
        stringBuilder.append("StartTimes with number of connected subjects:\n");
        this.getStartTimes().forEach((startTime, subjectsWithGroups) -> {
            stringBuilder.append(startTime)
                    .append(": ")
                    .append(subjectsWithGroups.size())
                    .append("\n");
        });

        stringBuilder.append("\n");
        stringBuilder.append("WeekDays with number of connected subjects:\n");
        this.getWeekDays().forEach((weekDay, subjectsWithGroups) -> {
            stringBuilder.append(weekDay)
                    .append(": ")
                    .append(subjectsWithGroups.size())
                    .append("\n");
        });

        stringBuilder.append("\n");
        stringBuilder.append("WeekParities with number of connected subjects:\n");
        this.getWeekParities().forEach((weekParity, subjectsWithGroups) -> {
            stringBuilder.append(weekParity)
                    .append(": ")
                    .append(subjectsWithGroups.size())
                    .append("\n");
        });

        return stringBuilder.toString();
    }


    @Override
    public int compareTo(Solution other) {
        return Double.compare(this.rating, other.rating);
    }
}
