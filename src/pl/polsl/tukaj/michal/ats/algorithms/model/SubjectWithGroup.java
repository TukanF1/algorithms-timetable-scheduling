package pl.polsl.tukaj.michal.ats.algorithms.model;

import pl.polsl.tukaj.michal.ats.model.GeneratedSection;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.util.Objects;

/**
 * Klasa wiążąca przedmiot z wygenerowaną sekcją.
 */
public class SubjectWithGroup implements Comparable<SubjectWithGroup> {
    private Subject subject;
    private GeneratedSection group;
    // Kolejny numer kopii tego obiektu (takiego samego połączenia przedmiotu z wygenerowaną grupą)
    // ze względu na możliwość prowadzenia zajęć z danego przedmiotu z daną grupą przez kilku nauczycieli jednocześnie.
    private Integer teacherNextNumber;
    // Kolejny numer kopii tego obiektu (takiego samego połączenia przedmiotu z wygenerowaną grupą)
    // ze względu na możliwość prowadzenia zajęć z danego przedmiotu z daną grupą kilka razy w tygodniu.
    private Integer lessonInWeekNextNumber;

    public SubjectWithGroup() {
        this.subject = null;
        this.group = null;
        this.teacherNextNumber = 0;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public GeneratedSection getGroup() {
        return group;
    }

    public void setGroup(GeneratedSection group) {
        this.group = group;
    }

    public Integer getTeacherNextNumber() {
        return teacherNextNumber;
    }

    public void setTeacherNextNumber(Integer teacherNextNumber) {
        this.teacherNextNumber = teacherNextNumber;
    }

    public Integer getLessonInWeekNextNumber() {
        return lessonInWeekNextNumber;
    }

    public void setLessonInWeekNextNumber(Integer lessonInWeekNextNumber) {
        this.lessonInWeekNextNumber = lessonInWeekNextNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubjectWithGroup that = (SubjectWithGroup) o;

        if (!Objects.equals(teacherNextNumber, that.teacherNextNumber)) return false;
        if (!Objects.equals(lessonInWeekNextNumber, that.lessonInWeekNextNumber)) return false;
        if (!Objects.equals(subject, that.subject)) return false;
        return Objects.equals(group, that.group);
    }

    @Override
    public int hashCode() {
        int result = subject != null ? subject.hashCode() : 0;
        result = 31 * result + (group != null ? group.hashCode() : 0);
        result = 31 * result + (teacherNextNumber != null ? teacherNextNumber.hashCode() : 0);
        result = 31 * result + (lessonInWeekNextNumber != null ? lessonInWeekNextNumber.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(SubjectWithGroup other) {
        return Integer.compare(this.teacherNextNumber, other.teacherNextNumber);
    }
}
