package pl.polsl.tukaj.michal.ats.algorithms.model;

/**
 * Dana wartość reprezentuje część rozwiązania będącego częścią klasy Solution.
 */
public enum PartOfSolution {
    TEACHERS,
    ROOMS,
    START_TIMES,
    WEEK_DAYS,
    WEEK_PARITIES;
}
