package pl.polsl.tukaj.michal.ats.algorithms;

import pl.polsl.tukaj.michal.ats.algorithms.model.*;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Teacher;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Genetic extends Algorithm {
    private final int generations;
    private final int populationSize;
    private List<Solution> population;
    private Solution bestSolution;

    private final Path logFile = Paths.get("Genetic_" +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")) + ".csv");
    private List<String> lines = new ArrayList<>();

    private Genetic(String timetableName, Integer iterations, Integer sizeOfPopulation, List<Semester> semesters) {
        super(timetableName, semesters);
        generations = Math.max(iterations, 1);
        populationSize = Math.max(sizeOfPopulation, 10);
        population = Collections.synchronizedList(new ArrayList<>(populationSize));
    }

    private void run() {
        System.out.println("GENETIC STARTED!");

        startTime = LocalDateTime.now();

        for (int i = 0; i < populationSize; ++i) {
            population.add(generateFirstSolution());
        }

        population.parallelStream().forEach(solution -> {
            solution.rate(true, true, true, true);
        });

        Collections.sort(population);

        bestSolution = population.get(0);

        System.out.println("Rate of best solution from the first random generation: " + bestSolution.getRating());

        int iteration = 0;

        for (int i = 0; i < generations && bestSolution.getRating() > 2; ++i) {
            population = getNewGeneration(population,
                    true, true, true, true);

            Collections.sort(population);

            bestSolution = population.get(0);

            lines.add(++iteration + ";" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + ";" +
                    bestSolution.getRating() + ";" + population.size());

            if (i % 10 == 0) {
                System.out.println((i + 1) + ": " + bestSolution.getRating() + " : " + population.size());
            }
        }

        endTime = LocalDateTime.now();

        finalSolution = bestSolution;

        System.out.println(iteration + ": " + bestSolution.getRating() + " : " + population.size());

        try {
            Files.write(logFile, lines, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    static void mutatePopulation(List<Solution> population) {
        population.forEach(solution -> {
            if (Math.random() < 0.15) {
                solution.mutate();
            }
        });
    }

    static List<Solution> getNewGeneration(List<Solution> population,
                                           boolean teachersRate, boolean roomsRate,
                                           boolean sectionsRate, boolean extrasRate) {
        population.parallelStream().forEach(
                solution -> solution.rate(teachersRate, roomsRate, sectionsRate, extrasRate)
        );

        List<Solution> parents = selectParents(population);
        List<Solution> children = getChildrenAfterCrossOver(parents);
        List<Solution> newGeneration = Collections.synchronizedList(new ArrayList<>(population.size()));

        parents.parallelStream().forEach(solution -> solution.rate(teachersRate, roomsRate, sectionsRate, extrasRate));

        children.parallelStream().forEach(solution -> solution.rate(teachersRate, roomsRate, sectionsRate, extrasRate));

        mutatePopulation(parents);
        mutatePopulation(children);

        children.parallelStream().forEach(solution -> solution.rate(teachersRate, roomsRate, sectionsRate, extrasRate));

        Collections.sort(parents);
        Collections.sort(children);

        for (int i = 0; i < parents.size() * 0.02; ++i) {
            newGeneration.add(parents.get(i));
        }
        for (int i = 0; i < children.size() * 0.98; ++i) {
            newGeneration.add(children.get(i));
        }
        return newGeneration;
    }

    static List<Solution> selectParents(List<Solution> population) {
        Map<Solution, Double> rouletteWheel = new ConcurrentHashMap<>();

        double sumOfRatings = 0.;
        double maxRating = 0.;
        for (Solution solution : population) {
            sumOfRatings += solution.getRating();
            if (solution.getRating() > maxRating) {
                maxRating = solution.getRating();
            }
        }
        // Przyporządkowanie kołu ruletki kolejnych osobników (rozwiązań)
        // razem z wielkością wycinka jaką zajmują na tym kole (prawdopodobieństwem wylosowania).
        for (Solution solution : population) {
            double probability = (maxRating - solution.getRating()) / (maxRating * population.size() - sumOfRatings);
            Double currentProbability;
            if ((maxRating * population.size() - sumOfRatings) == 0) {
                rouletteWheel.put(solution, 0.);
            } else if ((currentProbability = rouletteWheel.putIfAbsent(solution, probability)) != null) {
                rouletteWheel.put(solution, currentProbability + probability);
            }
        }
        List<Solution> parents = Collections.synchronizedList(new ArrayList<>());

        // Wybór kolejnych rodziców.
        for (int i = 0; i < population.size(); ++i) {
            double randomValue = Math.random();
            double sumOfProbabilities = 0.;

            // Obrót ruletką i przeszukanie po kolejnych jej wycinkach w poszukiwaniu kulki (randomValue).
            for (Map.Entry<Solution, Double> entry : rouletteWheel.entrySet()) {
                Solution solution = entry.getKey();
                Double probability = entry.getValue();

                // Sprawdzenie czy kulka (randomValue) znajduje się właśnie przy tym osobniku (rozwiązaniu).
                if (randomValue < probability + sumOfProbabilities) {
                    parents.add(solution);
                    break;
                } else {
                    sumOfProbabilities += probability;
                }
            }
            if (parents.size() < i + 1) {
                parents.add((Solution) rouletteWheel.keySet().toArray()[0]);
            }
        }
        return parents;
    }

    static List<Solution> getChildrenAfterCrossOver(List<Solution> parents) {
        List<Solution> children = Collections.synchronizedList(new ArrayList<>());
        // Wymieszanie osobników rodzicielskich.
        Collections.shuffle(parents);
        // Dobieranie osobników w pary i ich krzyżowanie.
        // Ignorowanie osobnika bez pary, jeśli taki występuje.
        for (int i = 0; i < parents.size() - 1; ++i) {
            children.addAll(crossOver(parents.get(i), parents.get(++i)));
        }
        return children;
    }

    static List<Solution> crossOver(Solution parentA, Solution parentB) {
        List<Solution> children = new ArrayList<>(2);

        List<SubjectWithGroup> chromosome = new ArrayList<>(parentA.getSubjectsWithGroups());
        Collections.shuffle(chromosome);
        int chromosomeLength = chromosome.size();
        int crossoverPoint = (int) (Math.random() * chromosomeLength);

        Solution childA = parentA.getCopy();
        Solution childB = parentB.getCopy();
        for (int i = crossoverPoint; i < chromosomeLength; ++i) {
            SubjectWithGroup subjectWithGroup = chromosome.get(i);
            List<Object> removedFromA = childA.removeSubjectWithGroup(subjectWithGroup);
            List<Object> removedFromB = childB.removeSubjectWithGroup(subjectWithGroup);

            removedFromA.forEach(gene -> {
                if (gene instanceof Teacher) {
                    childB.addSubjectWithGroupToTeacher((Teacher) gene, subjectWithGroup);
                } else if (gene instanceof Room) {
                    childB.addSubjectWithGroupToRoom((Room) gene, subjectWithGroup);
                } else if (gene instanceof LocalTime) {
                    childB.addSubjectWithGroupToStartTime((LocalTime) gene, subjectWithGroup);
                } else if (gene instanceof DayOfWeek) {
                    childB.addSubjectWithGroupToWeekDay((DayOfWeek) gene, subjectWithGroup);
                } else if (gene instanceof Parity) {
                    childB.addSubjectWithGroupToWeekParity((Parity) gene, subjectWithGroup);
                }
            });

            removedFromB.forEach(gene -> {
                if (gene instanceof Teacher) {
                    childA.addSubjectWithGroupToTeacher((Teacher) gene, subjectWithGroup);
                } else if (gene instanceof Room) {
                    childA.addSubjectWithGroupToRoom((Room) gene, subjectWithGroup);
                } else if (gene instanceof LocalTime) {
                    childA.addSubjectWithGroupToStartTime((LocalTime) gene, subjectWithGroup);
                } else if (gene instanceof DayOfWeek) {
                    childA.addSubjectWithGroupToWeekDay((DayOfWeek) gene, subjectWithGroup);
                } else if (gene instanceof Parity) {
                    childA.addSubjectWithGroupToWeekParity((Parity) gene, subjectWithGroup);
                }
            });
        }

        children.add(childA);
        children.add(childB);

        return children;
    }

    static public String generate(String timetableName, Integer iterations, Integer populationSize,
                                  List<Semester> semesters) {
        Genetic genetic = new Genetic(timetableName, iterations, populationSize, semesters);
        genetic.init();
        genetic.run();

        Duration duration = Duration.between(genetic.startTime, genetic.endTime);

        genetic.saveFinalSolutionToDB();

        return String.format("Time table is scheduled with Genetic use.\n"
                        + "Total time of scheduling was %02d:%02d:%02d.%03d.",
                duration.toHours(), duration.toMinutesPart(),
                duration.toSecondsPart(), duration.toNanosPart() / 1_000_000);
    }
}
