package pl.polsl.tukaj.michal.ats.algorithms;

import pl.polsl.tukaj.michal.ats.algorithms.model.Parity;
import pl.polsl.tukaj.michal.ats.algorithms.model.Solution;
import pl.polsl.tukaj.michal.ats.algorithms.model.SubjectWithGroup;
import pl.polsl.tukaj.michal.ats.dao.*;
import pl.polsl.tukaj.michal.ats.model.*;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Klasa bazowa dla algorytmów.
 */
public class Algorithm {
    LocalDateTime startTime, endTime;

    final static LocalTime firstAvailableTime = LocalTime.of(7, 0);
    final static LocalTime lastAvailableTime = LocalTime.of(22, 0);
    final public static Duration timePrecision = Duration.ofMinutes(15);

    Solution finalSolution;

    TimetableDao timetableDao;
    LessonDao lessonDao;
    GeneratedSectionDao generatedSectionDao;
    FieldOfStudyDao fieldOfStudyDao;
    SemesterDao semesterDao;
    GroupDao groupDao;
    SectionDao sectionDao;
    FeatureDao featureDao;
    RoomDao roomDao;
    TeacherDao teacherDao;
    SubjectToTeacherDao subjectToTeacherDao;
    SubjectDao subjectDao;
    SemesterAvailabilityDao semesterAvailabilityDao;
    RoomAvailabilityDao roomAvailabilityDao;
    TeacherAvailabilityDao teacherAvailabilityDao;

    Timetable timetable;
    List<Lesson> lessons;
    List<GeneratedSection> generatedSections;
    List<FieldOfStudy> fieldsOfStudies;
    List<Semester> semesters;
    List<Group> groups;
    List<Section> sections;
    List<Feature> features;
    List<Room> rooms;
    List<Teacher> teachers;
    List<SubjectToTeacher> subjectsToTeachers;
    List<Subject> subjects;
    List<SemesterAvailability> semesterAvailabilities;
    List<RoomAvailability> roomAvailabilities;
    List<TeacherAvailability> teacherAvailabilities;

    Set<SubjectWithGroup> subjectsWithGroups;

    /**
     * Konstruktor klasy bazowej inicjujący pola klasy algorytm.
     *
     * @param timetableName nazwa planu zajęć
     */
    Algorithm(String timetableName, List<Semester> semestersList) {
        // Inicjacja obiektów odpowiadających za połączenie z bazą danych i reprezentację struktury bazodanowej.
        this.timetableDao = new TimetableDao();
        this.lessonDao = new LessonDao();
        this.generatedSectionDao = new GeneratedSectionDao();
        this.fieldOfStudyDao = new FieldOfStudyDao();
        this.semesterDao = new SemesterDao();
        this.groupDao = new GroupDao();
        this.sectionDao = new SectionDao();
        this.featureDao = new FeatureDao();
        this.roomDao = new RoomDao();
        this.teacherDao = new TeacherDao();
        this.subjectToTeacherDao = new SubjectToTeacherDao();
        this.subjectDao = new SubjectDao();
        this.semesterAvailabilityDao = new SemesterAvailabilityDao();
        this.roomAvailabilityDao = new RoomAvailabilityDao();
        this.teacherAvailabilityDao = new TeacherAvailabilityDao();

        // Inicjacja i pobranie list danych wejściowych z bazy.
        this.timetable = new Timetable(null, timetableName);
        this.lessons = Collections.synchronizedList(new ArrayList<>());
        this.generatedSections = Collections.synchronizedList(new ArrayList<>());

        Set<FieldOfStudy> fieldsOfStudies = new HashSet<>();
        Set<Semester> semesters = new HashSet<>();
        Set<Group> groups = new HashSet<>();
        Set<Section> sections = new HashSet<>();
        Set<SemesterAvailability> semesterAvailabilities = new HashSet<>();

        semestersList.forEach(semester -> {
            fieldsOfStudies.addAll(this.fieldOfStudyDao.getAllBySemesterId(semester.getId()));

            semesters.add(this.semesterDao.getSemesterById(semester.getId()));

            groups.addAll(this.groupDao.getGroupsBySemester(semester));

            sections.addAll(this.sectionDao.getSectionsBySemester(semester));

            semesterAvailabilities.addAll(
                    this.semesterAvailabilityDao.getSemesterAvailabilitiesBySemester(semester)
            );
        });
        groups.forEach(group -> sections.addAll(this.sectionDao.getSectionsByGroup(group)));

        this.fieldsOfStudies = new ArrayList<>(fieldsOfStudies);
        this.semesterAvailabilities = new ArrayList<>(semesterAvailabilities);
        this.semesters = new ArrayList<>(semesters);
        this.groups = new ArrayList<>(groups);
        this.sections = new ArrayList<>(sections);

        this.features = this.featureDao.getAllFeatures();

        this.rooms = this.roomDao.getAllRooms();

        Set<Subject> subjects = new HashSet<>();
        this.groups.forEach(group -> subjects.addAll(this.subjectDao.getSubjectsByGroup(group)));
        this.sections.forEach(section -> subjects.addAll(this.subjectDao.getSubjectsBySection(section)));
        this.subjects = new ArrayList<>(subjects);

        Set<SubjectToTeacher> subjectsToTeachers = new HashSet<>();
        this.subjects.forEach(
                subject -> subjectsToTeachers.addAll(
                        this.subjectToTeacherDao.getSubjectsToTeachersBySubject(subject)
                )
        );
        this.subjectsToTeachers = new ArrayList<>(subjectsToTeachers);

        Set<Teacher> teachers = new HashSet<>();
        this.subjects.forEach(subject -> teachers.addAll(this.teacherDao.getTeachersBySubject(subject)));
        this.teachers = new ArrayList<>(teachers);

        Set<RoomAvailability> roomAvailabilities = new HashSet<>();
        this.rooms.forEach(
                room -> roomAvailabilities.addAll(this.roomAvailabilityDao.getRoomAvailabilitiesByRoom(room))
        );
        this.roomAvailabilities = new ArrayList<>(roomAvailabilities);

        Set<TeacherAvailability> teacherAvailabilities = new HashSet<>();
        this.teachers.forEach(
                teacher -> teacherAvailabilities.addAll(
                        this.teacherAvailabilityDao.getTeacherAvailabilitiesByTeacher(teacher)
                )
        );
        this.teacherAvailabilities = new ArrayList<>(teacherAvailabilities);

        this.subjectsWithGroups = Collections.synchronizedSet(new HashSet<>());
    }

    /**
     * Podczas inicjacji algorytmu następuje wiązanie ze sobą obiektów na podstawie schematu bazy danych
     * oraz wygenerowanie "ostatecznych" sekcji na podstawie wprowadzonych do bazy sekcji i grup.
     */
    void init() {
        // Wymieszanie list danych wejściowych.
        Collections.shuffle(fieldsOfStudies);
        Collections.shuffle(semesters);
        Collections.shuffle(groups);
        Collections.shuffle(sections);
        Collections.shuffle(features);
        Collections.shuffle(rooms);
        Collections.shuffle(subjects);
        Collections.shuffle(subjectsToTeachers);
        Collections.shuffle(teachers);
        Collections.shuffle(semesterAvailabilities);
        Collections.shuffle(roomAvailabilities);
        Collections.shuffle(teacherAvailabilities);

        // Wiązanie kierunków studiów z semestrami.
        Thread threadFieldsOfStudies = new Thread(() -> fieldsOfStudies.parallelStream().forEach(fieldOfStudy -> {
            List<Semester> semesters = this.semesters.parallelStream().filter(
                    semester -> Objects.equals(semester.getFieldOfStudyId(), fieldOfStudy.getId())
            ).collect(Collectors.toList());

            semesters.forEach(semester -> semester.setFieldOfStudy(fieldOfStudy));
            fieldOfStudy.addSemesters(semesters);
        }));
        threadFieldsOfStudies.start();

        // Wiązanie semestrów z grupami, sekcjami i dostępnością (kiedy grupy i sekcje tego semestru mogą mieć zajęcia)
        Thread threadSemesters = new Thread(() -> semesters.parallelStream().forEach(semester -> {
            List<Group> groups = this.groups.parallelStream().filter(
                    group -> Objects.equals(group.getSemesterId(), semester.getId())
            ).collect(Collectors.toList());

            groups.forEach(group -> group.setSemester(semester));
            semester.addGroups(groups);

            List<Section> sections = this.sections.parallelStream().filter(
                    section -> Objects.equals(section.getSemesterId(), semester.getId())
            ).collect(Collectors.toList());

            sections.forEach(section -> section.setSemester(semester));
            semester.addSections(sections);

            List<SemesterAvailability> semesterAvailabilities = this.semesterAvailabilities.parallelStream().filter(
                    semesterAvailability -> Objects.equals(semesterAvailability.getSemesterId(), semester.getId())
            ).collect(Collectors.toList());

            semesterAvailabilities.forEach(
                    semesterAvailability -> semesterAvailability.setSemester(semester)
            );
            semester.addSemesterAvailabilities(semesterAvailabilities);
        }));
        threadSemesters.start();

        // Wiązanie grup z przedmiotami i sekcjami.
        Thread threadGroups = new Thread(() -> groups.parallelStream().forEach(group -> {
            List<Subject> subjectsOfGroup = subjectDao.getSubjectsByGroup(group);
            List<Subject> subjects = this.subjects.parallelStream().filter(
                    subject -> subjectsOfGroup.stream().filter(
                            subjectOfGroup -> Objects.equals(subjectOfGroup.getId(), subject.getId())
                    ).findAny().orElse(null) != null
            ).collect(Collectors.toList());

            subjects.forEach(subject -> subject.addGroup(group));
            group.addSubjects(subjects);

            List<Section> sections = this.sections.parallelStream().filter(
                    section -> Objects.equals(section.getGroupId(), group.getId())
            ).collect(Collectors.toList());

            sections.forEach(section -> section.setGroup(group));
            group.addSections(sections);
        }));
        threadGroups.start();

        // Wiązanie sekcji z przedmiotami.
        Thread threadSections = new Thread(() -> sections.parallelStream().forEach(section -> {
            List<Subject> subjectsOfSection = subjectDao.getSubjectsBySection(section);
            List<Subject> subjects = this.subjects.parallelStream().filter(
                    subject -> subjectsOfSection.stream().filter(
                            subjectOfSection -> Objects.equals(subjectOfSection.getId(), subject.getId())
                    ).findAny().orElse(null) != null
            ).collect(Collectors.toList());

            subjects.forEach(subject -> subject.addSection(section));
            section.addSubjects(subjects);
        }));
        threadSections.start();

        // Wiązanie przedmiotów z wymaganymi cechami pokoi i nauczycielami.
        Thread threadSubjects = new Thread(() -> subjects.parallelStream().forEach(subject -> {
            List<Feature> featuresOfSubject = featureDao.getFeaturesBySubject(subject);
            List<Feature> features = this.features.parallelStream().filter(
                    feature -> featuresOfSubject.stream().filter(
                            featureOfSubject -> Objects.equals(featureOfSubject.getId(), feature.getId())
                    ).findAny().orElse(null) != null
            ).collect(Collectors.toList());

            features.forEach(feature -> feature.addSubject(subject));
            subject.addFeatures(features);

            List<SubjectToTeacher> subjectsToTeachers = this.subjectsToTeachers.parallelStream().filter(
                    subjectToTeacher -> Objects.equals(subjectToTeacher.getSubjectId(), subject.getId())
            ).collect(Collectors.toList());

            subjectsToTeachers.forEach(subjectToTeacher -> subjectToTeacher.setSubject(subject));
            subject.addSubjectToTeachers(subjectsToTeachers);
        }));
        threadSubjects.start();

        // Wiązanie pokoi z dostępnością i ich cechami
        Thread threadRooms = new Thread(() -> rooms.parallelStream().forEach(room -> {
            List<Feature> featuresOfRoom = featureDao.getFeaturesByRoom(room);
            List<Feature> features = this.features.parallelStream().filter(
                    feature -> featuresOfRoom.stream().filter(
                            featureOfRoom -> Objects.equals(featureOfRoom.getId(), feature.getId())
                    ).findAny().orElse(null) != null
            ).collect(Collectors.toList());

            features.forEach(feature -> feature.addRoom(room));
            room.addFeatures(features);

            List<RoomAvailability> roomAvailabilities = this.roomAvailabilities.parallelStream().filter(
                    roomAvailability -> Objects.equals(roomAvailability.getRoomId(), room.getId())
            ).collect(Collectors.toList());

            roomAvailabilities.forEach(roomAvailability -> roomAvailability.setRoom(room));
            room.addRoomAvailabilities(roomAvailabilities);
        }));
        threadRooms.start();

        // Wiązanie nauczycieli z ich dostępnością i przedmiotami.
        Thread threadTeachers = new Thread(() -> teachers.parallelStream().forEach(teacher -> {
            List<SubjectToTeacher> subjectsToTeachers = this.subjectsToTeachers.parallelStream().filter(
                    subjectToTeacher -> Objects.equals(subjectToTeacher.getTeacherId(), teacher.getId())
            ).collect(Collectors.toList());

            subjectsToTeachers.forEach(subjectToTeacher -> subjectToTeacher.setTeacher(teacher));
            teacher.addSubjectsToTeachers(subjectsToTeachers);

            List<TeacherAvailability> teacherAvailabilities = this.teacherAvailabilities.parallelStream().filter(
                    teacherAvailability -> Objects.equals(teacherAvailability.getTeacherId(), teacher.getId())
            ).collect(Collectors.toList());

            teacherAvailabilities.forEach(
                    teacherAvailability -> teacherAvailability.setTeacher(teacher)
            );
            teacher.addTeacherAvailabilities(teacherAvailabilities);
        }));
        threadTeachers.start();

        try {
            threadFieldsOfStudies.join();
            threadSemesters.join();
            threadSections.join();
            threadSubjects.join();
            threadRooms.join();
            threadTeachers.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Generowanie ostatecznych sekcji na bazie grup (tabeli GROUPS)
        // oraz możliwości umieszczenia wygenerowanej sekcji w jakiejkolwiek sali spełniającej wymagania przedmiotu
        // i posiadającej pojemność mogącą pomieścić daną grupę.
        // Dodatkowo tworzenie listy obiektów będących połączeniem
        // wygenerowanej sekcji i przedmiotu (SubjectWithGroups).
        Thread threadGroupsToGeneratedSections = new Thread(() -> groups.parallelStream().forEach(group -> {
            group.getSubjects().forEach(subjectOfGroup -> {
                boolean roomExist = rooms.parallelStream().anyMatch(
                        room -> room.getFeatures().containsAll(subjectOfGroup.getFeatures())
                                && room.getCapacity() >= group.getNumberOfPeople()
                );

                if (roomExist) {
                    for (int i = 0; i < subjectOfGroup.getLessonsInWeek(); ++i) {
                        GeneratedSection generatedSection = new GeneratedSection();
                        generatedSection.setName(
                                group.getName() + " - " + subjectOfGroup.getName() + (i > 0 ? (" - " + i) : "")
                        );
                        generatedSection.setNumberOfPeople(group.getNumberOfPeople());
                        generatedSection.setGroup(group);
                        generatedSections.add(generatedSection);

                        for (int t = 0; t < subjectOfGroup.getTeachersInOneLesson(); ++t) {
                            SubjectWithGroup subjectWithGroup = new SubjectWithGroup();
                            subjectWithGroup.setGroup(generatedSection);
                            subjectWithGroup.setSubject(subjectOfGroup);
                            subjectWithGroup.setTeacherNextNumber(t);
                            subjectWithGroup.setLessonInWeekNextNumber(i);
                            subjectsWithGroups.add(subjectWithGroup);
                        }
                    }
                } else {
                    List<Integer> sizeOfSections = new ArrayList<>();

                    boolean roomWithFeaturesExist = rooms.parallelStream().anyMatch(
                            room -> room.getFeatures().containsAll(subjectOfGroup.getFeatures())
                    );

                    roomExist = this.splitSectionOrGroupAndCheckRoomExist(
                            group, subjectOfGroup, sizeOfSections
                    );

                    if (roomExist) {
                        AtomicInteger subSectionNumber = new AtomicInteger();
                        sizeOfSections.forEach(sizeOfSection -> {
                            for (int i = 0; i < subjectOfGroup.getLessonsInWeek(); ++i) {
                                GeneratedSection generatedSection = new GeneratedSection();
                                generatedSection.setName(
                                        group.getName() + " - " + subjectOfGroup.getName() +
                                                " - Sekcja " + subSectionNumber.incrementAndGet()
                                                + (i > 0 ? (" - " + i) : "")
                                );
                                generatedSection.setNumberOfPeople(sizeOfSection);
                                generatedSection.setGroup(group);
                                generatedSections.add(generatedSection);

                                for (int t = 0; t < subjectOfGroup.getTeachersInOneLesson(); ++t) {
                                    SubjectWithGroup subjectWithGroup = new SubjectWithGroup();
                                    subjectWithGroup.setGroup(generatedSection);
                                    subjectWithGroup.setSubject(subjectOfGroup);
                                    subjectWithGroup.setTeacherNextNumber(t);
                                    subjectWithGroup.setLessonInWeekNextNumber(i);
                                    subjectsWithGroups.add(subjectWithGroup);
                                }
                            }
                        });
                    } else {
                        timetable.addNotAddedSubject(subjectOfGroup);
                        subjectOfGroup.addNotAddedToTimetable(timetable);
                        System.out.println("SUBJECT NOT ADDED.");
                    }
                }
            });
        }));
        threadGroupsToGeneratedSections.start();

        // Generowanie ostatecznych sekcji na bazie sekcji (tabeli SECTIONS)
        // oraz możliwości umieszczenia wygenerowanej sekcji w jakiejkolwiek sali spełniającej wymagania przedmiotu
        // i posiadającej pojemność mogącą pomieścić daną grupę.
        // Dodatkowo tworzenie listy obiektów będących połączeniem
        // wygenerowanej sekcji i przedmiotu (SubjectWithGroups).
        Thread threadSectionsToGeneratedSections = new Thread(() -> sections.parallelStream().forEach(section -> {
            section.getSubjects().forEach(subjectOfSection -> {
                boolean roomExist = rooms.parallelStream().anyMatch(
                        room -> room.getFeatures().containsAll(subjectOfSection.getFeatures())
                                && room.getCapacity() >= section.getNumberOfPeople()
                );

                if (roomExist) {
                    for (int i = 0; i < subjectOfSection.getLessonsInWeek(); ++i) {
                        GeneratedSection generatedSection = new GeneratedSection();
                        if (subjectOfSection.getLessonsInWeek() > 1) {
                            generatedSection.setId(-i);
                        }
                        generatedSection.setName(
                                section.getName() + " - " + subjectOfSection.getName() + (i > 0 ? (" - " + i) : "")
                        );
                        generatedSection.setNumberOfPeople(section.getNumberOfPeople());
                        generatedSection.setSection(section);
                        generatedSections.add(generatedSection);

                        for (int t = 0; t < subjectOfSection.getTeachersInOneLesson(); ++t) {
                            SubjectWithGroup subjectWithGroup = new SubjectWithGroup();
                            subjectWithGroup.setGroup(generatedSection);
                            subjectWithGroup.setSubject(subjectOfSection);
                            subjectWithGroup.setTeacherNextNumber(t);
                            subjectWithGroup.setLessonInWeekNextNumber(i);
                            subjectsWithGroups.add(subjectWithGroup);
                        }
                    }
                } else {
                    List<Integer> sizeOfSections = new ArrayList<>();

                    boolean roomWithFeaturesExist = rooms.parallelStream().anyMatch(
                            room -> room.getFeatures().containsAll(subjectOfSection.getFeatures())
                    );

                    roomExist = this.splitSectionOrGroupAndCheckRoomExist(
                            section, subjectOfSection, sizeOfSections
                    );

                    if (roomExist) {
                        AtomicInteger subSectionNumber = new AtomicInteger();
                        sizeOfSections.forEach(sizeOfSection -> {
                            for (int i = 0; i < subjectOfSection.getLessonsInWeek(); ++i) {
                                GeneratedSection generatedSection = new GeneratedSection();
                                generatedSection.setName(
                                        section.getName() + " - " + subjectOfSection.getName()
                                                + " - Podsekcja " + subSectionNumber.incrementAndGet()
                                                + (i > 0 ? (" - " + i) : "")
                                );
                                generatedSection.setNumberOfPeople(sizeOfSection);
                                generatedSection.setSection(section);
                                generatedSections.add(generatedSection);

                                for (int t = 0; t < subjectOfSection.getTeachersInOneLesson(); ++t) {
                                    SubjectWithGroup subjectWithGroup = new SubjectWithGroup();
                                    subjectWithGroup.setGroup(generatedSection);
                                    subjectWithGroup.setSubject(subjectOfSection);
                                    subjectWithGroup.setTeacherNextNumber(t);
                                    subjectWithGroup.setLessonInWeekNextNumber(i);
                                    subjectsWithGroups.add(subjectWithGroup);
                                }
                            }
                        });
                    } else {
                        timetable.addNotAddedSubject(subjectOfSection);
                        subjectOfSection.addNotAddedToTimetable(timetable);
                        System.out.println("SUBJECT NOT ADDED.");
                    }
                }
            });
        }));
        threadSectionsToGeneratedSections.start();

        try {
            threadGroupsToGeneratedSections.join();
            threadSectionsToGeneratedSections.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Używana w pętli generującej sekcje w przypadku, gdy trzeba sekcję lub grupę podzielić na mniejsze by znaleźć
     * salę.
     *
     * @param section          sekcja lub grupa do podzielenia
     * @param subjectOfSection przedmiot związany z sekcją lub grupą
     * @param sizeOfSections   tablica rozmiarów sekcji po podzieleniu
     * @return true jeśli istnieje sala dla tak podzielonej sekcji lub grupy.
     */
    private boolean splitSectionOrGroupAndCheckRoomExist(ISection section, Subject subjectOfSection,
                                                         List<Integer> sizeOfSections) {
        List<Room> goodRooms = rooms.stream()
                .filter(room -> room.getFeatures().containsAll(subjectOfSection.getFeatures()))
                .sorted((o1, o2) -> Integer.compare(o2.getCapacity(), o1.getCapacity()))
                .collect(Collectors.toList());

        if (goodRooms.isEmpty()) {
            return false;
        }

        Integer restSize = section.getNumberOfPeople();
        sizeOfSections.clear();

        for (int i = 0; i < goodRooms.size() && restSize > 0; ++i) {
            Room room = goodRooms.get(i);
            Integer nextSectionSize = Math.min(room.getCapacity(), restSize);
            sizeOfSections.add(nextSectionSize);
            restSize -= nextSectionSize;
        }

        return true;
    }

    /**
     * Metoda generująca obiekt rozwiązania w sposób losowy.
     *
     * @return obiekt Solution będący logiczną reprezentacją rozwiązania.
     */
    Solution generateFirstSolution() {
        Solution solution = new Solution();

        int teachersSize = teachers.size();
        int roomsSize = rooms.size();

        Thread threadTeachers = new Thread(() -> {
            solution.addTeachers(this.teachers);
            subjectsWithGroups.forEach(subjectWithGroup ->
                    solution.addSubjectWithGroupToTeacher(
                            teachers.get((int) (Math.random() * teachersSize)),
                            subjectWithGroup
                    )
            );
        });

        Thread threadRooms = new Thread(() -> {
            solution.addRooms(this.rooms);
            subjectsWithGroups.stream()
                    .filter(subjectWithGroup -> subjectWithGroup.getTeacherNextNumber() == 0)
                    .forEach(subjectWithGroup -> {
                        solution.addSubjectWithGroupToRoom(
                                rooms.get((int) (Math.random() * roomsSize)),
                                subjectWithGroup
                        );
                    });
        });

        Thread threadStartTimes = new Thread(() -> {
            List<LocalTime> generatedStartTimes = new ArrayList<>();
            for (
                    LocalTime time = firstAvailableTime;
                    !time.isAfter(lastAvailableTime);
                    time = time.plus(timePrecision)
            ) {
                generatedStartTimes.add(time);
            }
            Collections.shuffle(generatedStartTimes);
            int generatedStartTimesSize = generatedStartTimes.size();

            solution.addStartTimes(generatedStartTimes);
            subjectsWithGroups.stream()
                    .filter(subjectWithGroup -> subjectWithGroup.getTeacherNextNumber() == 0)
                    .forEach(subjectWithGroup ->
                            solution.addSubjectWithGroupToStartTime(
                                    generatedStartTimes.get((int) (Math.random() * generatedStartTimesSize)),
                                    subjectWithGroup
                            )
                    );
        });

        Thread threadWeekDays = new Thread(() -> {
            List<DayOfWeek> daysOfWeek = new ArrayList<>(List.of(DayOfWeek.values()));
            Collections.shuffle(daysOfWeek);
            int daysOfWeeksSize = daysOfWeek.size();

            solution.addWeekDays(daysOfWeek);
            subjectsWithGroups.stream()
                    .filter(subjectWithGroup -> subjectWithGroup.getTeacherNextNumber() == 0)
                    .forEach(subjectWithGroup ->
                            solution.addSubjectWithGroupToWeekDay(
                                    daysOfWeek.get((int) (Math.random() * daysOfWeeksSize)),
                                    subjectWithGroup
                            )
                    );
        });

        Thread threadWeekParities = new Thread(() -> {
            List<Parity> paritiesOfWeek = new ArrayList<>(List.of(Parity.values()));
            Collections.shuffle(paritiesOfWeek);
            int paritiesOfWeekSize = paritiesOfWeek.size();

            solution.addWeekParities(paritiesOfWeek);
            subjectsWithGroups.stream()
                    .filter(subjectWithGroup -> subjectWithGroup.getSubject().getEveryTwoWeek())
                    .filter(subjectWithGroup -> subjectWithGroup.getTeacherNextNumber() == 0)
                    .forEach(subjectWithGroup ->
                            solution.addSubjectWithGroupToWeekParity(
                                    paritiesOfWeek.get((int) (Math.random() * paritiesOfWeekSize)),
                                    subjectWithGroup
                            )
                    );
        });

        threadTeachers.start();
        threadRooms.start();
        threadStartTimes.start();
        threadWeekDays.start();
        threadWeekParities.start();

        try {
            threadTeachers.join();
            threadRooms.join();
            threadStartTimes.join();
            threadWeekDays.join();
            threadWeekParities.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return solution;
    }

    void saveFinalSolutionToDB() {
        timetable.setId(timetableDao.saveTimetable(timetable));
        if (timetable.getId() != null) {
            finalSolution.getLessons().forEach(lesson -> lesson.setTimetable(timetable));
            for (Lesson lesson : finalSolution.getLessons()) {
                lesson.setGeneratedSectionId(generatedSectionDao.saveGeneratedSection(lesson.getGeneratedSection()));
                if (lesson.getGeneratedSection() == null) {
                    System.out.println("Error during save generated section. Returned new id from DB id null.");
                    return;
                }
            }
            lessonDao.saveLessons(finalSolution.getLessons());
            timetable.getNotAddedSubjects().forEach(
                    subject -> subjectDao.addSubjectToNotAddedInTimetable(subject, timetable)
            );

            System.out.println("Rzeczywisty czas kolizji " +
                    "(wlicza się też w niego łączny czas braku minimalnych przerw po zajęciach): "
                    + (finalSolution.getTimeOfRoomsLessonsCollisions()
                    .plus(finalSolution.getTimeOfTeachersLessonsCollisions())
                    .plus(finalSolution.getTimeSectionsLessonsCollisions())
                    .toMinutes() / 2.)
            );
        }
    }
}
