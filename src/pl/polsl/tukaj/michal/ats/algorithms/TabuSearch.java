package pl.polsl.tukaj.michal.ats.algorithms;

import pl.polsl.tukaj.michal.ats.algorithms.model.*;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Teacher;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class TabuSearch extends Algorithm {
    private final int epoche;
    private final int tabuTenure = 50;
    private Solution bestSolution, currentSolution;

    private final Path logFile = Paths.get("TabuSearch_" +
            LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss")) + ".csv");

    private TabuSearch(String timetableName, Integer iterations, List<Semester> semesters) {
        super(timetableName, semesters);
        epoche = Math.max(iterations / 4, 1);
    }

    static Map<Solution, Step> generateNeighborhoodOfSolution(Solution solution) {
        boolean changeTeacher = Math.random() < 0.75;
        boolean changeRoom = Math.random() < 0.6;

        Map<Solution, Step> neighborhood = new ConcurrentHashMap<>();
        List<Teacher> teachers = Collections.synchronizedList(new ArrayList<>(solution.getTeachers().keySet()));
        List<Room> rooms = Collections.synchronizedList(new ArrayList<>(solution.getRooms().keySet()));

        Thread threadTeachers = new Thread(() -> {
            if (changeTeacher) {
                int keyId = 0;

                List<SubjectWithGroup> subjectsWithGroups = new ArrayList<>();
                while (subjectsWithGroups.isEmpty()) {
                    keyId = (int) (Math.random() * solution.getTeachers().keySet().size());
                    subjectsWithGroups = new ArrayList<>(solution.getTeachers().get(teachers.get(keyId)));
                }
                Teacher oldTeacherKey = teachers.get(keyId);
                keyId = (int) (Math.random() * subjectsWithGroups.size());
                SubjectWithGroup subjectWithGroup = subjectsWithGroups.get(keyId);

                solution.getTeachers().keySet().parallelStream().filter(
                        newTeacherKey -> !Objects.equals(newTeacherKey, oldTeacherKey)
                ).forEach(newTeacherKey -> {
                    Solution solutionCopy = solution.getCopy();

                    if (solutionCopy.removeSubjectWithGroupFromTeacher(oldTeacherKey, subjectWithGroup)) {
                        if (solutionCopy.addSubjectWithGroupToTeacher(newTeacherKey, subjectWithGroup)) {
                            Step step = new Step(PartOfSolution.TEACHERS, oldTeacherKey, newTeacherKey);
                            neighborhood.putIfAbsent(solutionCopy, step);
                        } else {
                            if (!solutionCopy.addSubjectWithGroupToTeacher(oldTeacherKey, subjectWithGroup)) {
                                System.out.println("ERROR: threadTeachers:");
                                System.out.println("OLD: " + oldTeacherKey);
                                System.out.println("NEW: " + newTeacherKey);
                            }
                        }
                    } else {
                        System.out.println("ERROR: threadTeachers: NOT REMOVED!");
                    }
                });
            }
        });

        Thread threadRooms = new Thread(() -> {
            if (changeRoom) {
                int keyId = 0;

                List<SubjectWithGroup> subjectsWithGroups = new ArrayList<>();
                while (subjectsWithGroups.isEmpty()) {
                    keyId = (int) (Math.random() * rooms.size());
                    subjectsWithGroups = new ArrayList<>(solution.getRooms().get(rooms.get(keyId)));
                }
                Room oldRoomKey = rooms.get(keyId);
                keyId = (int) (Math.random() * subjectsWithGroups.size());
                SubjectWithGroup subjectWithGroup = subjectsWithGroups.get(keyId);

                rooms.parallelStream().filter(
                        newRoomKey -> !Objects.equals(newRoomKey, oldRoomKey)
                ).forEach(newRoomKey -> {
                    Solution solutionCopy = solution.getCopy();

                    if (solutionCopy.removeSubjectWithGroupFromRoom(oldRoomKey, subjectWithGroup)) {
                        if (solutionCopy.addSubjectWithGroupToRoom(newRoomKey, subjectWithGroup)) {
                            Step step = new Step(PartOfSolution.ROOMS, oldRoomKey, newRoomKey);
                            neighborhood.putIfAbsent(solutionCopy, step);
                        } else {
                            if (!solutionCopy.addSubjectWithGroupToRoom(oldRoomKey, subjectWithGroup)) {
                                System.out.println("ERROR: threadRooms:");
                                System.out.println("OLD: " + oldRoomKey);
                                System.out.println("NEW: " + newRoomKey);
                            }
                        }
                    } else {
                        System.out.println("ERROR: threadRooms: NOT REMOVED!");
                    }
                });
            }
        });

        Thread threadStartTimes = new Thread(() -> {
            int keyId = 0;

            List<SubjectWithGroup> subjectsWithGroups = new ArrayList<>();
            LocalTime localTime = null;
            while (subjectsWithGroups.isEmpty()) {
                keyId = (int) (Math.random() * solution.getStartTimes().keySet().size());
                localTime = new ArrayList<>(solution.getStartTimes().keySet()).get(keyId);
                subjectsWithGroups = new ArrayList<>(solution.getStartTimes().get(localTime));
            }
            LocalTime oldStartTimeKey = localTime;
            keyId = (int) (Math.random() * subjectsWithGroups.size());
            SubjectWithGroup subjectWithGroup = subjectsWithGroups.get(keyId);

            solution.getStartTimes().keySet().stream().filter(
                    newStartTimeKey -> !Objects.equals(newStartTimeKey, oldStartTimeKey)
            ).forEach(newStartTimeKey -> {
                Solution solutionCopy = solution.getCopy();

                if (solutionCopy.removeSubjectWithGroupFromStartTime(oldStartTimeKey, subjectWithGroup)) {
                    if (solutionCopy.addSubjectWithGroupToStartTime(newStartTimeKey, subjectWithGroup)) {
                        Step step = new Step(PartOfSolution.START_TIMES, oldStartTimeKey, newStartTimeKey);
                        neighborhood.putIfAbsent(solutionCopy, step);
                    } else {
                        if (!solutionCopy.addSubjectWithGroupToStartTime(oldStartTimeKey, subjectWithGroup)) {
                            System.out.println("ERROR: threadStartTimes:");
                            System.out.println("OLD: " + oldStartTimeKey);
                            System.out.println("NEW: " + newStartTimeKey);
                        }
                    }
                } else {
                    System.out.println("ERROR: threadStartTimes: NOT REMOVED!");
                }
            });
        });

        Thread threadWeekDays = new Thread(() -> {
            int keyId = 0;

            List<SubjectWithGroup> subjectsWithGroups = new ArrayList<>();
            DayOfWeek dayOfWeek = null;
            while (subjectsWithGroups.isEmpty()) {
                keyId = (int) (Math.random() * solution.getWeekDays().keySet().size());
                dayOfWeek = new ArrayList<>(solution.getWeekDays().keySet()).get(keyId);
                subjectsWithGroups = new ArrayList<>(solution.getWeekDays().get(dayOfWeek));
            }
            DayOfWeek oldWeekDayKey = dayOfWeek;
            keyId = (int) (Math.random() * subjectsWithGroups.size());
            SubjectWithGroup subjectWithGroup = subjectsWithGroups.get(keyId);

            solution.getWeekDays().keySet().stream().filter(
                    newWeekDayKey -> !Objects.equals(newWeekDayKey, oldWeekDayKey)
            ).forEach(newWeekDayKey -> {
                Solution solutionCopy = solution.getCopy();

                if (solutionCopy.removeSubjectWithGroupFromWeekDay(oldWeekDayKey, subjectWithGroup)) {
                    if (solutionCopy.addSubjectWithGroupToWeekDay(newWeekDayKey, subjectWithGroup)) {
                        Step step = new Step(PartOfSolution.WEEK_DAYS, oldWeekDayKey, newWeekDayKey);
                        neighborhood.putIfAbsent(solutionCopy, step);
                    } else {
                        if (!solutionCopy.addSubjectWithGroupToWeekDay(oldWeekDayKey, subjectWithGroup)) {
                            System.out.println("ERROR: threadWeekDays:");
                            System.out.println("OLD: " + oldWeekDayKey);
                            System.out.println("NEW: " + newWeekDayKey);
                        }
                    }
                } else {
                    System.out.println("ERROR: threadWeekDays: NOT REMOVED!");
                }
            });
        });

        Thread threadWeekParities = new Thread(() -> {
            int keyId = (int) (Math.random() * solution.getWeekParities().keySet().size());
            Parity oldWeekParityKey = new ArrayList<>(solution.getWeekParities().keySet()).get(keyId);
            List<SubjectWithGroup> subjectsWithGroups =
                    new ArrayList<>(solution.getWeekParities().get(oldWeekParityKey));
            keyId = (int) (Math.random() * subjectsWithGroups.size());
            SubjectWithGroup subjectWithGroup = subjectsWithGroups.get(keyId);

            solution.getWeekParities().keySet().stream().filter(
                    newWeekParityKey -> !Objects.equals(newWeekParityKey, oldWeekParityKey)
            ).forEach(newWeekParityKey -> {
                Solution solutionCopy = solution.getCopy();

                if (solutionCopy.removeSubjectWithGroupFromWeekParity(oldWeekParityKey, subjectWithGroup)) {
                    if (solutionCopy.addSubjectWithGroupToWeekParity(newWeekParityKey, subjectWithGroup)) {
                        Step step = new Step(PartOfSolution.WEEK_PARITIES, oldWeekParityKey, newWeekParityKey);
                        neighborhood.putIfAbsent(solutionCopy, step);
                    } else {
                        if (!solutionCopy.addSubjectWithGroupToWeekParity(oldWeekParityKey, subjectWithGroup)) {
                            System.out.println("ERROR: threadWeekParities:");
                            System.out.println("OLD: " + oldWeekParityKey);
                            System.out.println("NEW: " + newWeekParityKey);
                        }
                    }
                } else {
                    System.out.println("ERROR: threadWeekParities: NOT REMOVED!");
                }
            });
        });

        try {
            threadTeachers.start();
            threadRooms.start();
            threadStartTimes.start();
            threadWeekDays.start();
            threadWeekParities.start();

            threadStartTimes.join();
            threadWeekDays.join();
            threadWeekParities.join();
            threadTeachers.join();
            threadRooms.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return neighborhood;
    }

    private void run() {
        System.out.println("TABU SEARCH STARTED!");

        startTime = LocalDateTime.now();

        bestSolution = currentSolution = generateFirstSolution();
        currentSolution.rate(true, false, false, false);

        System.out.println("Rate of the first random solution: " + currentSolution.getRating());

        Map<Solution, List<String>> solutionWithLines =
                optimizeSolutionByTabuSearch(epoche, tabuTenure, currentSolution, false);

        bestSolution = new ArrayList<>(solutionWithLines.keySet()).get(0);
        List<String> lines = solutionWithLines.get(bestSolution);

        endTime = LocalDateTime.now();

        finalSolution = bestSolution;

        try {
            Files.write(logFile, lines, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    static Map<Solution, List<String>> optimizeSolutionByTabuSearch(int epoche, int tabuTenure,
                                                                    Solution solutionToOptimize, boolean notPrint) {
        LocalTime d, e;
        d = LocalTime.now();

        Solution bestSolution = solutionToOptimize;
        Solution currentSolution = solutionToOptimize;
        Set<Step> tabu = new HashSet<>(tabuTenure + 1);
        List<Step> lastAddedStepsToTabu = new ArrayList<>(tabuTenure + 1);
        Map<Solution, Step> neighborhood;

        List<String> lines = new ArrayList<>();
        int iteration = 0;

        for (int i = 0; i < epoche && bestSolution.getRating() > 5; ++i) {
            neighborhood = generateNeighborhoodOfSolution(currentSolution);

            List<Solution> solutions = Collections.synchronizedList(new ArrayList<>(neighborhood.keySet()));
            solutions.parallelStream().forEach(
                    solution -> solution.rate(true, false, false, false)
            );

            List<Solution> currentAndBestSolutions = checkSolutions(solutions, neighborhood, tabu,
                    lastAddedStepsToTabu, tabuTenure, currentSolution, bestSolution);

            currentSolution = currentAndBestSolutions.get(0);
            bestSolution = currentAndBestSolutions.get(1);

            lines.add(++iteration + ";" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + ";" +
                    currentSolution.getRating());
        }

        e = LocalTime.now();
        Duration f;
        if (!notPrint) {
            System.out.println(1 + ": " + currentSolution.getRating() + " | " + bestSolution.getRating());
            System.out.format((f = Duration.between(d, e)).toSeconds() + ".%03d%n", (f.toNanosPart() / 1_000_000));
        }
        d = LocalTime.now();

        currentSolution.rate(true, true, false, false);
        bestSolution.rate(true, true, false, false);
        if (!notPrint) {
            System.out.println(1 + "a: " + currentSolution.getRating() + " | " + bestSolution.getRating());
        }

        if (currentSolution.getRating() < bestSolution.getRating()) {
            currentSolution = bestSolution;
        }

        for (int i = 0; i < epoche / 2 && bestSolution.getRating() > 4; ++i) {
            neighborhood = generateNeighborhoodOfSolution(currentSolution);

            List<Solution> solutions = Collections.synchronizedList(new ArrayList<>(neighborhood.keySet()));
            solutions.parallelStream().forEach(
                    solution -> solution.rate(true, true, false, false)
            );

            List<Solution> currentAndBestSolutions = checkSolutions(solutions, neighborhood, tabu,
                    lastAddedStepsToTabu, tabuTenure, currentSolution, bestSolution);

            currentSolution = currentAndBestSolutions.get(0);
            bestSolution = currentAndBestSolutions.get(1);

            lines.add(++iteration + ";" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + ";" +
                    currentSolution.getRating());
        }

        e = LocalTime.now();

        if (!notPrint) {
            System.out.println(2 + ": " + currentSolution.getRating() + " | " + bestSolution.getRating());
            System.out.format((f = Duration.between(d, e)).toSeconds() + ".%03d%n", (f.toNanosPart() / 1_000_000));
        }
        d = LocalTime.now();

        currentSolution.rate(true, true, true, false);
        bestSolution.rate(true, true, true, false);
        if (!notPrint) {
            System.out.println(2 + "a: " + currentSolution.getRating() + " | " + bestSolution.getRating());
        }

        if (currentSolution.getRating() < bestSolution.getRating()) {
            currentSolution = bestSolution;
        }

        for (int i = 0; i < epoche / 4 && bestSolution.getRating() > 2; ++i) {
            neighborhood = generateNeighborhoodOfSolution(currentSolution);

            List<Solution> solutions = Collections.synchronizedList(new ArrayList<>(neighborhood.keySet()));
            solutions.parallelStream().forEach(
                    solution -> solution.rate(true, true, true, false)
            );

            List<Solution> currentAndBestSolutions = checkSolutions(solutions, neighborhood, tabu,
                    lastAddedStepsToTabu, tabuTenure, currentSolution, bestSolution);

            currentSolution = currentAndBestSolutions.get(0);
            bestSolution = currentAndBestSolutions.get(1);

            lines.add(++iteration + ";" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + ";" +
                    currentSolution.getRating());
        }

        e = LocalTime.now();

        if (!notPrint) {
            System.out.println(3 + ": " + currentSolution.getRating() + " | " + bestSolution.getRating());
            System.out.format((f = Duration.between(d, e)).toSeconds() + ".%03d%n", (f.toNanosPart() / 1_000_000));
        }
        d = LocalTime.now();

        currentSolution.rate(true, true, true, true);
        bestSolution.rate(true, true, true, true);
        if (!notPrint) {
            System.out.println(3 + "a: " + currentSolution.getRating() + " | " + bestSolution.getRating());
        }

        if (currentSolution.getRating() < bestSolution.getRating()) {
            currentSolution = bestSolution;
        }

        for (int i = 0; i < epoche * 9 / 4 && bestSolution.getRating() > 1; ++i) {
            neighborhood = generateNeighborhoodOfSolution(currentSolution);

            List<Solution> solutions = Collections.synchronizedList(new ArrayList<>(neighborhood.keySet()));
            solutions.parallelStream().forEach(
                    solution -> solution.rate(true, true, true, true)
            );

            List<Solution> currentAndBestSolutions = checkSolutions(solutions, neighborhood, tabu,
                    lastAddedStepsToTabu, tabuTenure, currentSolution, bestSolution);

            currentSolution = currentAndBestSolutions.get(0);
            bestSolution = currentAndBestSolutions.get(1);

            lines.add(++iteration + ";" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + ";" +
                    currentSolution.getRating());
        }
        e = LocalTime.now();

        lines.add(0, "0;" + e + ";" + bestSolution.getRating());

        if (!notPrint) {
            System.out.println(4 + ": " + currentSolution.getRating() + " | " + bestSolution.getRating());
            System.out.format((f = Duration.between(d, e)).toSeconds() + ".%03d%n", (f.toNanosPart() / 1_000_000));
        }

        Map<Solution, List<String>> bestSolutionsWithLogLines = new ConcurrentHashMap<>();
        bestSolutionsWithLogLines.put(bestSolution, lines);

        return bestSolutionsWithLogLines;
    }

    static List<Solution> checkSolutions(List<Solution> solutions, Map<Solution, Step> neighborhood, Set<Step> tabu,
                                         List<Step> lastAddedStepsToTabu, int tabuTenure,
                                         Solution currentSolution, Solution bestSolution) {
        Collections.sort(solutions);

        List<Solution> currentAndBestSolutions = new ArrayList<>();

        for (Solution solution : solutions) {
            Step step = neighborhood.get(solution);

            // Przeprowadzona zmiana nie jest objęta tabu.
            if (tabu.add(step)) {
                lastAddedStepsToTabu.add(step);
                if (lastAddedStepsToTabu.size() > tabuTenure) {
                    // Usuwanie najstarszego elementu zapisanego w tabu.
                    tabu.remove(lastAddedStepsToTabu.remove(0));
                }

                currentSolution = solution;
                // Zostało znalezione nowe najlepsze rozwiązanie.
                if (currentSolution.getRating() < bestSolution.getRating()) {
                    bestSolution = currentSolution;
                }
                break;
            }
            // Kryterium aspiracji.
            else if (solution.getRating() < bestSolution.getRating()) {
                currentSolution = bestSolution = solution;
                break;
            }
        }
        currentAndBestSolutions.add(currentSolution);
        currentAndBestSolutions.add(bestSolution);
        return currentAndBestSolutions;
    }

    static public String generate(String timetableName, Integer iterations, List<Semester> semesters) {
        TabuSearch tabuSearch = new TabuSearch(timetableName, iterations, semesters);
        tabuSearch.init();
        tabuSearch.run();

        Duration duration = Duration.between(tabuSearch.startTime, tabuSearch.endTime);

        tabuSearch.saveFinalSolutionToDB();

        return String.format("Time table is scheduled with Tube Search use.\n"
                + "Total time of scheduling was %02d:%02d:%02d.%03d.", duration.toHours(), duration.toMinutesPart(),
                duration.toSecondsPart(), duration.toNanosPart() / 1_000_000);
    }
}
