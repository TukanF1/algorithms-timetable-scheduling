package pl.polsl.tukaj.michal.ats.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeController extends ViewController implements Initializable {
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    @FXML
    void buttonTimetablesClick(ActionEvent event) {
        try {
            this.showView("timetables", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void buttonRoomsClick(ActionEvent event) {
        try {
            this.showView("rooms", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSubjectsClick(ActionEvent event) {
        try {
            this.showView("subjects", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonTeachersClick(ActionEvent event) {
        try {
            this.showView("teachers", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonGroupsClick(ActionEvent event) {
        try {
            this.showView("fieldsOfStudy", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonGenerateClick(ActionEvent event) {
        try {
            this.showView("selectSemestersToGenerate", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
