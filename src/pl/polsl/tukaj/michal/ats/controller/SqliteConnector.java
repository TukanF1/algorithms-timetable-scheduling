package pl.polsl.tukaj.michal.ats.controller;

import java.sql.*;

public class SqliteConnector {
    private static Connection connection;

    public static Connection getConnection() {
        if (SqliteConnector.connection == null) {
            try {
                Class.forName("org.sqlite.JDBC");
                System.out.println("Connecting to database...");
                SqliteConnector.connection = DriverManager.getConnection("jdbc:sqlite:scheduling.db");
                System.out.println("Connected.");
            } catch (ClassNotFoundException | SQLException ex) {
                ex.printStackTrace();
                SqliteConnector.connection = null;
            }
        }
        return SqliteConnector.connection;
    }
}
