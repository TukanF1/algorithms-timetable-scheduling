package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.FieldOfStudyDao;
import pl.polsl.tukaj.michal.ats.dao.GroupDao;
import pl.polsl.tukaj.michal.ats.dao.SectionDao;
import pl.polsl.tukaj.michal.ats.dao.SemesterDao;
import pl.polsl.tukaj.michal.ats.model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SectionsController extends ViewController implements Initializable, IParametersController {
    private String previousViewName;
    private Semester semester;
    private FieldOfStudy fieldOfStudy;
    private Group group;
    private Subject subjectOfSections;
    private Section sectionOfPrevious;
    private Teacher teacherOfPrevious;
    private Timetable timetable;

    @FXML
    private TableView<Section> tableSections;

    @FXML
    private TableColumn<Section, Integer> columnId;

    @FXML
    private TableColumn<Section, String> columnName;

    @FXML
    private TableColumn<Section, Integer> columnNumberOfPeople;

    @FXML
    private Label sectionsLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.columnNumberOfPeople.setCellValueFactory(new PropertyValueFactory<>("numberOfPeople"));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            switch (this.previousViewName) {
                case "subjects": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        if (this.sectionOfPrevious != null) {
                            parametersController.addParameter(this.sectionOfPrevious);
                        }
                        if (this.teacherOfPrevious != null) {
                            parametersController.addParameter(this.teacherOfPrevious);
                        }
                    }
                    break;
                }
                case "groups": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.group);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                case "semesters": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.semester);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                default:
                    this.showView(this.previousViewName, event);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonLessonsClick(ActionEvent event) {
        try {
            if (this.timetable != null) {
                Section selectedSection = this.tableSections.getSelectionModel().getSelectedItem();
                if (selectedSection != null) {
                    Object controller = this.showViewAndGetController("subjects", event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        List<Object> parameters = new ArrayList<>();
                        parameters.add(selectedSection);
                        parameters.add(this.timetable);
                        parametersController.addParameters(parameters);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSubjectsClick(ActionEvent event) {
        try {
            Section selectedSection = this.tableSections.getSelectionModel().getSelectedItem();
            if (selectedSection != null) {
                Object controller = this.showViewAndGetController("subjects", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    List<Object> parameters = new ArrayList<>();
                    parameters.add(selectedSection);
                    parameters.add(this.subjectOfSections);
                    parametersController.addParameters(parameters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Timetable) {
            this.timetable = (Timetable) parameter;
        } else if (parameter instanceof Semester) {
            this.semester = (Semester) parameter;

            this.previousViewName = "semesters";
        } else if (parameter instanceof FieldOfStudy) {
            this.fieldOfStudy = (FieldOfStudy) parameter;
        } else if (parameter instanceof Group) {
            this.group = (Group) parameter;

            this.previousViewName = "groups";
        } else if (parameter instanceof Subject) {
            this.subjectOfSections = (Subject) parameter;
            this.sectionsLabel.setText(TextConstans.sectionsLabelPrefix + this.subjectOfSections.getName());

            this.previousViewName = "subjects";
        } else if (parameter instanceof Section) {
            Section section = (Section) parameter;
            if (section.getGroupId() != null) {
                this.group = new GroupDao().getGroupById(section.getGroupId());
                this.semester = new SemesterDao().getSemesterById(this.group.getSemesterId());
                if (this.previousViewName == null) {
                    this.previousViewName = "groups";
                }
            } else {
                this.semester = new SemesterDao().getSemesterById(section.getSemesterId());
                this.previousViewName = "semesters";
            }
            this.fieldOfStudy = new FieldOfStudyDao().getFieldOfStudyById(this.semester.getFieldOfStudyId());

            this.sectionOfPrevious = section;
        } else if (parameter instanceof Teacher) {
            this.teacherOfPrevious = (Teacher) parameter;
        }
        if (this.subjectOfSections == null && this.fieldOfStudy != null && this.semester != null
                && this.group != null) {
            this.sectionsLabel.setText(
                    TextConstans.sectionsLabelPrefix + this.fieldOfStudy.getName()
                            + TextConstans.sectionsOfGroupNameLabelPrefixSemester + this.semester.getSemester()
                            + TextConstans.sectionsOfGroupNameLabelPrefixGroup + this.group.getName()
            );
        } else if (this.subjectOfSections == null && this.fieldOfStudy != null && this.semester != null) {
            this.sectionsLabel.setText(
                    TextConstans.sectionsLabelPrefix + this.fieldOfStudy.getName()
                            + TextConstans.sectionsOfGroupNameLabelPrefixSemester + this.semester.getSemester()
            );
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
        switch (this.previousViewName) {
            case "groups":
                this.tableSections.setItems(FXCollections.observableList(
                        new ArrayList<>(new SectionDao().getSectionsByGroup(this.group))
                ));
                break;
            case "subjects":
                this.tableSections.setItems(FXCollections.observableList(
                        new ArrayList<>(new SectionDao().getSectionsBySubject(this.subjectOfSections))
                ));
                break;
            case "semesters":
                this.tableSections.setItems(FXCollections.observableList(
                        new ArrayList<>(new SectionDao().getSectionsBySemester(this.semester))
                ));
                break;
        }
    }
}
