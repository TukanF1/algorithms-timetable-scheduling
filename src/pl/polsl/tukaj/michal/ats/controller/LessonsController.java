package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import pl.polsl.tukaj.michal.ats.dao.*;
import pl.polsl.tukaj.michal.ats.model.*;

import java.io.IOException;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class LessonsController extends ViewController implements Initializable, IParametersController {
    private boolean tableIsInited = false;

    private static class ColumnTimeFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {
        public TableCell<S, T> call(TableColumn<S, T> arg0) {
            return new TableCell<>() {
                @Override
                protected void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    } else {
                        LocalTime localTime = (LocalTime) item;
                        setText(localTime.format(DateTimeFormatter.ofPattern("HH:mm")));
                    }
                }
            };
        }
    }

    private String previousViewName;
    private Timetable timetable;
    private Room room;
    private Teacher teacher;
    private Semester semester;
    private Group group;
    private Section section;

    @FXML
    private TableView<Lesson> tableLessons;

    @FXML
    private TableColumn<Lesson, Integer> columnId;

    @FXML
    private TableColumn<Lesson, DayOfWeek> columnWeekDay;

    @FXML
    private TableColumn<Lesson, Date> columnFromHour;

    @FXML
    private TableColumn<Lesson, Date> columnToHour;

    @FXML
    private TableColumn<Lesson, Boolean> columnEvenWeek;

    @FXML
    private TableColumn<Lesson, Boolean> columnOddWeek;

    @FXML
    private TableColumn<Lesson, String> teacherName;

    @FXML
    private TableColumn<Lesson, String> subjectName;

    @FXML
    private TableColumn<Lesson, String> sectionName;

    @FXML
    private TableColumn<Lesson, String> roomName;

    @FXML
    private Label timetableNameLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnWeekDay.setCellValueFactory(new PropertyValueFactory<>("weekDay"));
        this.columnFromHour.setCellValueFactory(new PropertyValueFactory<>("fromHour"));
        this.columnToHour.setCellValueFactory(new PropertyValueFactory<>("toHour"));
        this.columnFromHour.setCellFactory(new ColumnTimeFormatter<>());
        this.columnToHour.setCellFactory(new ColumnTimeFormatter<>());
        this.columnEvenWeek.setCellValueFactory(new PropertyValueFactory<>("evenWeek"));
        this.columnOddWeek.setCellValueFactory(new PropertyValueFactory<>("oddWeek"));
        this.teacherName.setCellValueFactory(new PropertyValueFactory<>("teacherName"));
        this.subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectName"));
        this.sectionName.setCellValueFactory(new PropertyValueFactory<>("sectionName"));
        this.roomName.setCellValueFactory(new PropertyValueFactory<>("roomName"));

        this.tableLessons.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    private void initTable() {
        if (this.timetable != null) {
            List<Lesson> lessons;
            if (this.semester != null) {
                lessons = new LessonDao().getLessonsBySemesterAndTimetable(semester, timetable);
            } else if (this.group != null) {
                lessons = new LessonDao().getLessonsByGroupAndTimetable(group, timetable);
            } else if (this.section != null) {
                lessons = new LessonDao().getLessonsBySectionAndTimetable(section, timetable);
            } else if (this.teacher != null) {
                lessons = new LessonDao().getLessonsByTeacherAndTimetable(teacher, timetable);
            } else if (this.room != null) {
                lessons = new LessonDao().getLessonsByRoomAndTimetable(room, timetable);
            } else {
                lessons = new LessonDao().getLessonsByTimetable(timetable);
            }
            if (lessons != null) {
                lessons.forEach(lesson -> {
                    if (lesson.getTeachers().isEmpty()) {
                        lesson.setTeachers(new HashSet<>(new TeacherDao().getTeachersByLesson(lesson)));
                    }
                    if (lesson.getSubjectId() != null) {
                        lesson.setSubject(new SubjectDao().getSubjectById(lesson.getSubjectId()));
                    }
                    if (lesson.getGeneratedSectionId() != null) {
                        lesson.setGeneratedSection(
                                new GeneratedSectionDao().getGeneratedSectionById(lesson.getGeneratedSectionId())
                        );
                    }
                    if (lesson.getRoomId() != null) {
                        lesson.setRoom(new RoomDao().getRoomById(lesson.getRoomId()));
                    }
                });
                this.tableLessons.setItems(FXCollections.observableList(lessons));
                this.tableIsInited = true;
            }
        }
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            switch (this.previousViewName) {
                case "timetables": {
                    this.showView(this.previousViewName, event);
                    break;
                }
                case "semesters": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.semester);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                case "groups": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.group);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                case "sections": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.section);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                case "teachers": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.teacher);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                case "rooms": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.room);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                default:
                    this.showView(this.previousViewName, event);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Timetable) {
            this.timetable = (Timetable) parameter;
            if (semester == null && group == null && section == null && teacher == null && room == null) {
                this.previousViewName = "timetables";
            }

            this.timetableNameLabel.setText(
                    TextConstans.lessonsLabelTimetablePrefix + timetable.getName() +
                            TextConstans.lessonsLabelTimetableSuffix
            );
        } else if (parameter instanceof Semester) {
            this.semester = (Semester) parameter;
            this.previousViewName = "semesters";
        } else if (parameter instanceof Group) {
            this.group = (Group) parameter;
            this.previousViewName = "groups";
        } else if (parameter instanceof Section) {
            this.section = (Section) parameter;
            this.previousViewName = "sections";
        } else if (parameter instanceof Teacher) {
            this.teacher = (Teacher) parameter;
            this.previousViewName = "teachers";
        } else if (parameter instanceof Room) {
            this.room = (Room) parameter;
            this.previousViewName = "rooms";
        }
        if (!this.tableIsInited) {
            initTable();
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
