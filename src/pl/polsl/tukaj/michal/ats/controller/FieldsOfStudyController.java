package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import pl.polsl.tukaj.michal.ats.dao.FieldOfStudyDao;
import pl.polsl.tukaj.michal.ats.model.FieldOfStudy;
import pl.polsl.tukaj.michal.ats.model.Timetable;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class FieldsOfStudyController extends ViewController implements Initializable, IParametersController {
    private Timetable timetable;

    @FXML
    private TableView<FieldOfStudy> tableFieldsOfStudy;

    @FXML
    private TableColumn<FieldOfStudy, Integer> columnId;

    @FXML
    private TableColumn<FieldOfStudy, String> columnName;

    @FXML
    private Button buttonSemesters;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnName.setCellValueFactory(new PropertyValueFactory<>("name"));

        this.tableFieldsOfStudy.setItems(FXCollections.observableList((new FieldOfStudyDao()).getAllFieldsOfStudy()));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            this.showView(this.timetable == null ? "home" : "timetables", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSemestersClick(ActionEvent event) {
        try {
            FieldOfStudy selectedFieldOfStudy = tableFieldsOfStudy.getSelectionModel().getSelectedItem();
            if (selectedFieldOfStudy != null) {
                Object controller = this.showViewAndGetController("semesters", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedFieldOfStudy);
                    parametersController.addParameter(timetable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void tableMouseClick(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            if (event.getClickCount() == 2) {
                this.buttonSemesters.fire();
            }
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Timetable) {
            this.timetable = (Timetable) parameter;
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
