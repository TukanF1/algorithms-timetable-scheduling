package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.FieldOfStudyDao;
import pl.polsl.tukaj.michal.ats.dao.SemesterDao;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SelectSemestersToGenerateController extends ViewController implements Initializable {
    @FXML
    private TableView<Semester> tableSemesters;

    @FXML
    private TableColumn<Semester, Integer> columnId;

    @FXML
    private TableColumn<Semester, Integer> columnSemester;

    @FXML
    private TableColumn<Semester, String> columnFieldOfStudyName;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnSemester.setCellValueFactory(new PropertyValueFactory<>("semester"));
        this.columnFieldOfStudyName.setCellValueFactory(new PropertyValueFactory<>("fieldOfStudyName"));
        this.tableSemesters.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        List<Semester> semesters = new ArrayList<>(new SemesterDao().getAllSemesters());
        semesters.forEach(semester -> {
            semester.setFieldOfStudy(new FieldOfStudyDao().getFieldOfStudyBySemester(semester));
        });
        this.tableSemesters.setItems(FXCollections.observableList(semesters));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            this.showView("home", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSelectClick(ActionEvent event) {
        try {
            List<Semester> selectedSemesters = this.tableSemesters.getSelectionModel().getSelectedItems();
            if (!selectedSemesters.isEmpty()) {
                Object controller = this.showViewAndGetController("generateTimetable", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameters(selectedSemesters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
