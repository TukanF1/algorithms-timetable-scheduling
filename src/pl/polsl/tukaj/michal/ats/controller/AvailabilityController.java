package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import pl.polsl.tukaj.michal.ats.dao.SemesterAvailabilityDao;
import pl.polsl.tukaj.michal.ats.dao.RoomAvailabilityDao;
import pl.polsl.tukaj.michal.ats.dao.TeacherAvailabilityDao;
import pl.polsl.tukaj.michal.ats.model.*;

import java.io.IOException;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class AvailabilityController extends ViewController implements Initializable, IParametersController {
    private static class ColumnTimeFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {
        public TableCell<S, T> call(TableColumn<S, T> arg0) {
            return new TableCell<>() {
                @Override
                protected void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    } else {
                        LocalTime localTime = (LocalTime) item;
                        setText(localTime.format(DateTimeFormatter.ofPattern("HH:mm")));
                    }
                }
            };
        }
    }

    private String previousViewName;
    private Semester semester;
    private Timetable timetable;

    @FXML
    private TableView<Availability> tableAvailability;

    @FXML
    private TableColumn<Availability, DayOfWeek> columnDayWeek;

    @FXML
    private TableColumn<Availability, Date> columnFrom;

    @FXML
    private TableColumn<Availability, Date> columnTo;

    @FXML
    private Label availabilityNameLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnDayWeek.setCellValueFactory(new PropertyValueFactory<>("weekDay"));
        this.columnFrom.setCellValueFactory(new PropertyValueFactory<>("fromHour"));
        this.columnTo.setCellValueFactory(new PropertyValueFactory<>("toHour"));

        this.columnFrom.setCellFactory(new ColumnTimeFormatter<>());
        this.columnTo.setCellFactory(new ColumnTimeFormatter<>());
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            if (this.previousViewName.equals("semesters")) {
                Object controller = this.showViewAndGetController(this.previousViewName, event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(this.semester);
                    if (this.timetable != null) {
                        parametersController.addParameter(this.timetable);
                    }
                }
            } else if (this.previousViewName.equals("teachers")) {
                Object controller = this.showViewAndGetController(this.previousViewName, event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    if (this.timetable != null) {
                        parametersController.addParameter(this.timetable);
                    }
                }
            } else if (this.previousViewName.equals("rooms")) {
                Object controller = this.showViewAndGetController(this.previousViewName, event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    if (this.timetable != null) {
                        parametersController.addParameter(this.timetable);
                    }
                }
            } else {
                this.showView("home", event);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Timetable) {
            timetable = (Timetable) parameter;
        } if (parameter instanceof Room) {
            Room room = (Room) parameter;
            this.availabilityNameLabel.setText(TextConstans.availabilityNameLabelPrefix + room.getName());

            this.tableAvailability.setItems(FXCollections.observableList(
                    new ArrayList<>((new RoomAvailabilityDao()).getRoomAvailabilitiesByRoom(room))
            ));
            this.previousViewName = "rooms";
        } else if (parameter instanceof Teacher) {
            Teacher teacher = (Teacher) parameter;
            this.availabilityNameLabel.setText(TextConstans.availabilityNameLabelPrefix + teacher.getFullName());

            this.tableAvailability.setItems(FXCollections.observableList(
                    new ArrayList<>((new TeacherAvailabilityDao()).getTeacherAvailabilitiesByTeacher(teacher))
            ));
            this.previousViewName = "teachers";
        } else if (parameter instanceof Semester) {
            this.semester = (Semester) parameter;
            this.availabilityNameLabel.setText(TextConstans.availabilityNameLabelPrefix + this.semester.getSemester());

            this.tableAvailability.setItems(FXCollections.observableList(
                    new ArrayList<>((new SemesterAvailabilityDao()).getSemesterAvailabilitiesBySemester(this.semester))
            ));
            this.previousViewName = "semesters";
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
