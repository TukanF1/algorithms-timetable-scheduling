package pl.polsl.tukaj.michal.ats.controller;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.InputStream;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Algorithms Timetable Scheduling");
        primaryStage.setHeight(600);
        primaryStage.setWidth(800);
        InputStream iconStream = getClass().getResourceAsStream("/logo_aei.png");
        if (iconStream != null) {
            primaryStage.getIcons().add(new Image(iconStream));
        }
        new ViewController().showView("home", primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
