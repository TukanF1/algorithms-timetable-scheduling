package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.FeatureDao;
import pl.polsl.tukaj.michal.ats.model.Feature;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Subject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class FeaturesController extends ViewController implements Initializable, IParametersController {
    private String previousViewName;

    @FXML
    private TableView<Feature> tableFeatures;

    @FXML
    private TableColumn<Feature, Integer> columnId;

    @FXML
    private TableColumn<Feature, String> columnName;

    @FXML
    private Label featuresParameterNameLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            this.showView(this.previousViewName, event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Room) {
            Room room = (Room) parameter;
            this.featuresParameterNameLabel.setText(TextConstans.featuresParameterNameLabelPrefix + room.getName());

            this.tableFeatures.setItems(FXCollections.observableList(
                    new ArrayList<>((new FeatureDao()).getFeaturesByRoom(room))
            ));

            this.previousViewName = "rooms";
        } else if (parameter instanceof Subject) {
            Subject subject = (Subject) parameter;
            this.featuresParameterNameLabel.setText(TextConstans.featuresParameterNameLabelPrefix
                    + subject.getName() + ", " + subject.getType());

            this.tableFeatures.setItems(FXCollections.observableList(
                    new ArrayList<>((new FeatureDao()).getFeaturesBySubject(subject))
            ));

            this.previousViewName = "subjects";
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
