package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.TeacherDao;
import pl.polsl.tukaj.michal.ats.model.Subject;
import pl.polsl.tukaj.michal.ats.model.Teacher;
import pl.polsl.tukaj.michal.ats.model.Timetable;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class TeachersController extends ViewController implements Initializable, IParametersController {
    private String previousViewName;
    private Subject subjectOfTeachers;
    private Teacher teacherOfPreviousSubjects;
    private Timetable timetable;

    @FXML
    private TableView<Teacher> tableTeachers;

    @FXML
    private TableColumn<Teacher, Integer> columnId;

    @FXML
    private TableColumn<Teacher, String> columnFullName;

    @FXML
    private TableColumn<Teacher, Boolean> columnVirtual;

    @FXML
    private Label teachersLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnFullName.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        this.columnVirtual.setCellValueFactory(new PropertyValueFactory<>("virtual"));

        this.tableTeachers.setItems(FXCollections.observableList((new TeacherDao()).getAllTeachers()));

        this.previousViewName = "home";
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            if (this.previousViewName.equals("subjects")) {
                Object controller = this.showViewAndGetController(this.previousViewName, event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(this.teacherOfPreviousSubjects);
                }
            } else {
                this.showView(this.timetable == null ? "home" : "timetables", event);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonLessonsClick(ActionEvent event) {
        try {
            if (this.timetable != null) {
                Teacher selectedTeacher = this.tableTeachers.getSelectionModel().getSelectedItem();
                if (selectedTeacher != null) {
                    Object controller = this.showViewAndGetController("lessons", event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(selectedTeacher);
                        parametersController.addParameter(this.timetable);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonAvailabilityClick(ActionEvent event) {
        try {
            Teacher selectedTeacher = this.tableTeachers.getSelectionModel().getSelectedItem();
            if (selectedTeacher != null) {
                Object controller = this.showViewAndGetController("availabilities", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedTeacher);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSubjectsClick(ActionEvent event) {
        try {
            Teacher selectedTeacher = tableTeachers.getSelectionModel().getSelectedItem();
            if (selectedTeacher != null) {
                Object controller = this.showViewAndGetController("subjects", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    List<Object> parameters = new ArrayList<>();
                    parameters.add(selectedTeacher);
                    parameters.add(this.subjectOfTeachers);
                    parametersController.addParameters(parameters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Timetable) {
            this.timetable = (Timetable) parameter;
        } else if (parameter instanceof Subject) {
            this.subjectOfTeachers = (Subject) parameter;
            this.teachersLabel.setText(TextConstans.teachersLabelPrefix
                    + this.subjectOfTeachers.getName() + ", " + this.subjectOfTeachers.getType());

            this.tableTeachers.setItems(FXCollections.observableList(
                    new ArrayList<>((new TeacherDao()).getTeachersBySubject(this.subjectOfTeachers))
            ));

            this.previousViewName = "subjects";
        } else if (parameter instanceof Teacher) {
            this.teacherOfPreviousSubjects = (Teacher) parameter;
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
