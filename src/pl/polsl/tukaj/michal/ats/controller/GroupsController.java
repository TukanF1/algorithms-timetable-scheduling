package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.FieldOfStudyDao;
import pl.polsl.tukaj.michal.ats.dao.GroupDao;
import pl.polsl.tukaj.michal.ats.dao.SemesterDao;
import pl.polsl.tukaj.michal.ats.model.*;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class GroupsController extends ViewController implements Initializable, IParametersController {
    private String previousViewName;
    private Semester semester;
    private FieldOfStudy fieldOfStudy;
    private Subject subjectOfGroups;
    private Group groupOfPrevious;
    private Teacher teacherOfPrevious;
    private Timetable timetable;

    @FXML
    private TableView<Group> tableGroups;

    @FXML
    private TableColumn<Group, Integer> columnId;

    @FXML
    private TableColumn<Group, String> columnName;

    @FXML
    private TableColumn<Group, Integer> columnNumberOfPeople;

    @FXML
    private Label groupsLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.columnNumberOfPeople.setCellValueFactory(new PropertyValueFactory<>("numberOfPeople"));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            switch (this.previousViewName) {
                case "subjects": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        if (this.groupOfPrevious != null) {
                            parametersController.addParameter(this.groupOfPrevious);
                        }
                        if (this.teacherOfPrevious != null) {
                            parametersController.addParameter(this.teacherOfPrevious);
                        }
                    }
                    break;
                }
                case "semesters": {
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(this.semester);
                        if (this.timetable != null) {
                            parametersController.addParameter(this.timetable);
                        }
                    }
                    break;
                }
                default:
                    this.showView(this.previousViewName, event);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonLessonsClick(ActionEvent event) {
        try {
            if (this.timetable != null) {
                Group group = this.tableGroups.getSelectionModel().getSelectedItem();
                if (group != null) {
                    Object controller = this.showViewAndGetController("lessons", event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        List<Object> parameters = new ArrayList<>();
                        parameters.add(group);
                        parameters.add(this.timetable);
                        parametersController.addParameters(parameters);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSectionsClick(ActionEvent event) {
        try {
            Group group = this.tableGroups.getSelectionModel().getSelectedItem();
            if (group != null) {
                Object controller = this.showViewAndGetController("sections", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    List<Object> parameters = new ArrayList<>();
                    parameters.add(this.fieldOfStudy);
                    parameters.add(this.semester);
                    parameters.add(group);
                    if (this.timetable != null) {
                        parameters.add(this.timetable);
                    }
                    parametersController.addParameters(parameters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSubjectsClick(ActionEvent event) {
        try {
            Group selectedGroup = this.tableGroups.getSelectionModel().getSelectedItem();
            if (selectedGroup != null) {
                Object controller = this.showViewAndGetController("subjects", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    List<Object> parameters = new ArrayList<>();
                    parameters.add(selectedGroup);
                    parameters.add(this.subjectOfGroups);
                    parametersController.addParameters(parameters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Timetable) {
            this.timetable = (Timetable) parameter;
        } else if (parameter instanceof Semester) {
            this.semester = (Semester) parameter;

            this.tableGroups.setItems(FXCollections.observableList(
                    new ArrayList<>((new GroupDao()).getGroupsBySemester(semester))
            ));

            this.previousViewName = "semesters";
        } else if (parameter instanceof FieldOfStudy) {
            this.fieldOfStudy = (FieldOfStudy) parameter;
        } else if (parameter instanceof Group) {
            Group group = (Group) parameter;
            this.semester = (new SemesterDao()).getSemesterById(group.getSemesterId());
            this.fieldOfStudy = (new FieldOfStudyDao()).getFieldOfStudyById(this.semester.getFieldOfStudyId());

            this.tableGroups.setItems(FXCollections.observableList(
                    new ArrayList<>((new GroupDao()).getGroupsBySemester(this.semester))
            ));
            this.groupOfPrevious = group;

            if (this.previousViewName == null) {
                this.previousViewName = "semesters";
            }
        } else if (parameter instanceof Subject) {
            this.subjectOfGroups = (Subject) parameter;
            this.groupsLabel.setText(TextConstans.groupsLabelPrefix + this.subjectOfGroups.getName());

            this.tableGroups.setItems(FXCollections.observableList(
                    new ArrayList<>((new GroupDao()).getGroupsBySubject(this.subjectOfGroups))
            ));

            this.previousViewName = "subjects";
        } else if (parameter instanceof Teacher) {
            this.teacherOfPrevious = (Teacher) parameter;
        }
        if (this.subjectOfGroups == null && this.fieldOfStudy != null && this.semester != null) {
            this.groupsLabel.setText(
                    TextConstans.groupsLabelPrefix + this.fieldOfStudy.getName()
                            + TextConstans.groupsOfSemesterNameLabelPrefixSemester + this.semester.getSemester()
            );
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
