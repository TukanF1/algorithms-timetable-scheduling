package pl.polsl.tukaj.michal.ats.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import pl.polsl.tukaj.michal.ats.algorithms.Genetic;
import pl.polsl.tukaj.michal.ats.algorithms.Memetic;
import pl.polsl.tukaj.michal.ats.algorithms.TabuSearch;
import pl.polsl.tukaj.michal.ats.model.Semester;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class GenerateTimetableController extends ViewController implements Initializable, IParametersController {
    private List<Semester> semesters;

    @FXML
    private TextField textFieldNumberOfIterations;

    @FXML
    private TextField textFieldTimetableName;

    @FXML
    private TextField textFieldSizeOfPopulation;

    @FXML
    private TextField textFieldNumberOfInnerIterations;

    @FXML
    private TextField textFieldPartOfPopulation;

    @FXML
    private Label labelInformation;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        semesters = new ArrayList<>();
        textFieldNumberOfIterations.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textFieldNumberOfIterations.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        textFieldSizeOfPopulation.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textFieldSizeOfPopulation.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        textFieldNumberOfInnerIterations.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textFieldNumberOfInnerIterations.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        textFieldPartOfPopulation.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textFieldPartOfPopulation.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            this.showView("selectSemestersToGenerate", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonTabuSearchClick(ActionEvent event) {
        LocalDateTime startTime = LocalDateTime.now();
        String timetableName = textFieldTimetableName.getText().trim();
        if (timetableName.isEmpty()) {
            timetableName = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(startTime);
        }
        final String finalTimetableName = timetableName;
        labelInformation.setText(
                "Timetable \"" + timetableName + "\" is generating by Tabu Search Algorithm.\n" +
                        "Generation started at " +
                        DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm:ss").format(startTime) +
                        ".\n\n" +
                        "Please wait...\n"
        );
        Integer iterations = Integer.valueOf(textFieldNumberOfIterations.getText());

        Thread thread = new Thread(() -> {
            final String result = TabuSearch.generate(finalTimetableName, iterations, semesters);
            Platform.runLater(() -> {
                labelInformation.setText(result);
            });
        });
        thread.start();
    }

    @FXML
    void buttonGeneticClick(ActionEvent event) {
        LocalDateTime startTime = LocalDateTime.now();
        String timetableName = textFieldTimetableName.getText().trim();
        if (timetableName.isEmpty()) {
            timetableName = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(startTime);
        }
        final String finalTimetableName = timetableName;
        labelInformation.setText(
                "Timetable \"" + timetableName + "\" is generating by Genetic Algorithm.\n" +
                        "Generation started at " +
                        DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm:ss").format(startTime) +
                        ".\n\n" +
                        "Please wait...\n"
        );
        Integer iterations = Integer.valueOf(textFieldNumberOfIterations.getText());
        Integer population = Integer.valueOf(textFieldSizeOfPopulation.getText());

        Thread thread = new Thread(() -> {
            final String result = Genetic.generate(finalTimetableName, iterations, population, semesters);
            Platform.runLater(() -> {
                labelInformation.setText(result);
            });
        });
        thread.start();
    }

    @FXML
    void buttonMemeticClick(ActionEvent event) {
        LocalDateTime startTime = LocalDateTime.now();
        String timetableName = textFieldTimetableName.getText().trim();
        if (timetableName.isEmpty()) {
            timetableName = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM).format(startTime);
        }
        final String finalTimetableName = timetableName;
        labelInformation.setText(
                "Timetable \"" + timetableName + "\" is generating by Memetic Algorithm.\n" +
                        "Generation started at " +
                        DateTimeFormatter.ofPattern("dd.MM.yyyy, HH:mm:ss").format(startTime) +
                        ".\n\n" +
                        "Please wait...\n"
        );
        Integer iterations = Integer.valueOf(textFieldNumberOfIterations.getText());
        Integer population = Integer.valueOf(textFieldSizeOfPopulation.getText());
        Integer innerIterations = Integer.valueOf(textFieldNumberOfInnerIterations.getText());
        Integer partOfPopulation = Integer.valueOf(textFieldPartOfPopulation.getText());

        Thread thread = new Thread(() -> {
            final String result = Memetic.generate(finalTimetableName, iterations, population,
                    innerIterations, partOfPopulation, semesters);
            Platform.runLater(() -> {
                labelInformation.setText(result);
            });
        });
        thread.start();
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Semester) {
            semesters.add((Semester) parameter);
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
