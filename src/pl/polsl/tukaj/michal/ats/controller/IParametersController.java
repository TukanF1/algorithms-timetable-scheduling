package pl.polsl.tukaj.michal.ats.controller;

import java.util.List;

interface IParametersController {
    void addParameter(Object parameter);

    void addParameters(List<?> parameters);
}
