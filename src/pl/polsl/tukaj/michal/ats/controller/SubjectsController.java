package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import pl.polsl.tukaj.michal.ats.dao.FieldOfStudyDao;
import pl.polsl.tukaj.michal.ats.dao.SemesterDao;
import pl.polsl.tukaj.michal.ats.dao.SubjectDao;
import pl.polsl.tukaj.michal.ats.model.*;

import java.io.IOException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SubjectsController extends ViewController implements Initializable, IParametersController {
    private static class ColumnTimeFormatter<S, T> implements Callback<TableColumn<S, T>, TableCell<S, T>> {
        public TableCell<S, T> call(TableColumn<S, T> arg0) {
            return new TableCell<>() {
                @Override
                protected void updateItem(T item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setText(null);
                    } else {
                        Duration duration = (Duration) item;
                        setText(duration.toHoursPart() + ":" + String.format("%02d", duration.toMinutesPart()));
                    }
                }
            };
        }
    }

    private String previousViewName;
    private Teacher teacherOfSubjects;
    private Subject subjectOfPrevious;
    private Group groupOfSubjects;
    private Section sectionOfSubjects;

    private void setParametersForGroupsAndSections(Object controller, Subject selectedSubject) {
        IParametersController parametersController = (IParametersController) controller;
        List<Object> parameters = new ArrayList<>();
        parameters.add(selectedSubject);
        parameters.add(this.groupOfSubjects);
        if (this.teacherOfSubjects != null) {
            parameters.add(this.teacherOfSubjects);
        }
        parametersController.addParameters(parameters);
    }

    @FXML
    private TableView<Subject> tableSubjects;

    @FXML
    private TableColumn<Subject, Integer> columnId;

    @FXML
    private TableColumn<Subject, String> columnName;

    @FXML
    private TableColumn<Subject, Duration> columnDuration;

    @FXML
    private TableColumn<Subject, SubjectType> columnType;

    @FXML
    private TableColumn<Subject, Boolean> columnEveryTwoWeek;

    @FXML
    private TableColumn<Subject, Integer> columnLessonsInWeek;

    @FXML
    private TableColumn<Subject, Integer> columnTeachersInOneLesson;

    @FXML
    private Label subjectsLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.columnDuration.setCellValueFactory(new PropertyValueFactory<>("duration"));
        this.columnType.setCellValueFactory(new PropertyValueFactory<>("type"));
        this.columnEveryTwoWeek.setCellValueFactory(new PropertyValueFactory<>("everyTwoWeek"));
        this.columnLessonsInWeek.setCellValueFactory(new PropertyValueFactory<>("lessonsInWeek"));
        this.columnTeachersInOneLesson.setCellValueFactory(new PropertyValueFactory<>("teachersInOneLesson"));

        this.columnDuration.setCellFactory(new ColumnTimeFormatter<>());

        this.tableSubjects.setItems(FXCollections.observableList((new SubjectDao()).getAllSubjects()));

        this.previousViewName = "home";
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            switch (this.previousViewName) {
                case "teachers":
                case "groups":
                case "sections":
                    Object controller = this.showViewAndGetController(this.previousViewName, event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        List<Object> parameters = new ArrayList<>();
                        parameters.add(this.subjectOfPrevious);
                        parameters.add(this.groupOfSubjects);
                        parameters.add(this.sectionOfSubjects);
                        parametersController.addParameters(parameters);
                    }
                    break;
                default:
                    this.showView(this.previousViewName, event);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonFeaturesClick(ActionEvent event) {
        try {
            Subject selectedSubject = this.tableSubjects.getSelectionModel().getSelectedItem();
            if (selectedSubject != null) {
                Object controller = this.showViewAndGetController("features", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedSubject);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonGroupsClick(ActionEvent event) {
        try {
            Subject selectedSubject = this.tableSubjects.getSelectionModel().getSelectedItem();
            if (selectedSubject != null) {
                Object controller = this.showViewAndGetController("groups", event);
                if (controller instanceof IParametersController) {
                    this.setParametersForGroupsAndSections(controller, selectedSubject);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSectionsClick(ActionEvent event) {
        try {
            Subject selectedSubject = this.tableSubjects.getSelectionModel().getSelectedItem();
            if (selectedSubject != null) {
                Object controller = this.showViewAndGetController("sections", event);
                if (controller instanceof IParametersController) {
                    this.setParametersForGroupsAndSections(controller, selectedSubject);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonTeachersClick(ActionEvent event) {
        try {
            Subject selectedSubject = this.tableSubjects.getSelectionModel().getSelectedItem();
            if (selectedSubject != null) {
                Object controller = this.showViewAndGetController("teachers", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    List<Object> parameters = new ArrayList<>();
                    parameters.add(selectedSubject);
                    parameters.add(this.teacherOfSubjects);
                    parametersController.addParameters(parameters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Teacher) {
            this.teacherOfSubjects = (Teacher) parameter;
            this.subjectsLabel.setText(TextConstans.subjectsLabelPrefix + this.teacherOfSubjects.getFullName());

            this.tableSubjects.setItems(FXCollections.observableList(
                    new ArrayList<>((new SubjectDao()).getSubjectsByTeacher(this.teacherOfSubjects))
            ));

            this.previousViewName = "teachers";
        } else if (parameter instanceof Subject) {
            this.subjectOfPrevious = (Subject) parameter;
        } else if (parameter instanceof Group) {
            this.groupOfSubjects = (Group) parameter;
            Semester semester = new SemesterDao().getSemesterByGroup(this.groupOfSubjects);
            this.subjectsLabel.setText(TextConstans.subjectsLabelPrefix + this.groupOfSubjects.getName() + "\n"
                    + TextConstans.subjectsLabelSemesterPrefix
                    + semester.getSemester() + "\n"
                    + TextConstans.subjectsLabelFieldOfStudyPrefix
                    + new FieldOfStudyDao().getFieldOfStudyBySemester(semester).getName());

            this.tableSubjects.setItems(FXCollections.observableList(
                    new ArrayList<>((new SubjectDao()).getSubjectsByGroup(this.groupOfSubjects))
            ));

            this.previousViewName = "groups";
        } else if (parameter instanceof Section) {
            this.sectionOfSubjects = (Section) parameter;
            this.subjectsLabel.setText(TextConstans.subjectsLabelPrefix + this.sectionOfSubjects.getName());

            this.tableSubjects.setItems(FXCollections.observableList(
                    new ArrayList<>((new SubjectDao()).getSubjectsBySection(this.sectionOfSubjects))
            ));

            this.previousViewName = "sections";
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.parallelStream().forEach(this::addParameter);
    }
}
