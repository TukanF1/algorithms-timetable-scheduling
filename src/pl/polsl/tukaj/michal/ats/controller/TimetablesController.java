package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.TimetableDao;
import pl.polsl.tukaj.michal.ats.model.Timetable;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TimetablesController extends ViewController implements Initializable {

    @FXML
    private TableView<Timetable> tableTimetables;

    @FXML
    private TableColumn<Timetable, Integer> columnId;

    @FXML
    private TableColumn<Timetable, String> columnName;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnName.setCellValueFactory(new PropertyValueFactory<>("name"));

        this.tableTimetables.setItems(FXCollections.observableList(new TimetableDao().getAllTimetables()));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            this.showView("home", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonFieldsOfStudyClick(ActionEvent event) {
        try {
            Timetable selectedTimetable = this.tableTimetables.getSelectionModel().getSelectedItem();
            if (selectedTimetable != null) {
                Object controller = this.showViewAndGetController("fieldsOfStudy", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedTimetable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonRoomsClick(ActionEvent event) {
        try {
            Timetable selectedTimetable = this.tableTimetables.getSelectionModel().getSelectedItem();
            if (selectedTimetable != null) {
                Object controller = this.showViewAndGetController("rooms", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedTimetable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonTeachersClick(ActionEvent event) {
        try {
            Timetable selectedTimetable = this.tableTimetables.getSelectionModel().getSelectedItem();
            if (selectedTimetable != null) {
                Object controller = this.showViewAndGetController("teachers", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedTimetable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonLessonsClick(ActionEvent event) {
        try {
            Timetable selectedTimetable = this.tableTimetables.getSelectionModel().getSelectedItem();
            if (selectedTimetable != null) {
                Object controller = this.showViewAndGetController("lessons", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedTimetable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
