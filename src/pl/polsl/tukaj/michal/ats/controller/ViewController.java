package pl.polsl.tukaj.michal.ats.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.io.IOException;

class ViewController {
    private static final String viewPath = "/view/";

    private FXMLLoader fxmlLoader;

    ViewController() {
        this.fxmlLoader = new FXMLLoader();
    }

    private String getFullName(String name) {
        return ViewController.viewPath + name + ".fxml";
    }

    private Stage getStageFromActionEvent(ActionEvent actionEvent) {
        return (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
    }

    void showView(String name, Stage stage) throws IOException {
        Pane pane = fxmlLoader.load(getClass().getResourceAsStream(this.getFullName(name)));
        double width = stage.getWidth();
        double height = stage.getHeight();
        boolean fullScreen = stage.isFullScreen();
        stage.setScene(new Scene(pane));
        if (!fullScreen) {
            stage.setWidth(width);
            stage.setHeight(height);
        }
        stage.setFullScreen(fullScreen);
        stage.show();
    }

    void showView(String name, ActionEvent actionEvent) throws IOException {
        Stage stage = this.getStageFromActionEvent(actionEvent);
        this.showView(name, stage);
    }

    private <T> T showViewAndGetController(String name, Stage stage) throws IOException {
        this.showView(name, stage);
        return fxmlLoader.getController();
    }

    <T> T showViewAndGetController(String name, ActionEvent actionEvent) throws IOException {
        Stage stage = this.getStageFromActionEvent(actionEvent);
        return this.showViewAndGetController(name, stage);
    }
}
