package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.RoomDao;
import pl.polsl.tukaj.michal.ats.model.Room;
import pl.polsl.tukaj.michal.ats.model.Timetable;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class RoomsController extends ViewController implements Initializable, IParametersController {
    private Timetable timetable;

    @FXML
    private TableView<Room> tableRooms;

    @FXML
    private TableColumn<Room, Integer> columnId;

    @FXML
    private TableColumn<Room, String> columnName;

    @FXML
    private TableColumn<Room, Integer> columnCapacity;

    @FXML
    private TableColumn<Room, Integer> columnVirtual;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnName.setCellValueFactory(new PropertyValueFactory<>("name"));
        this.columnCapacity.setCellValueFactory(new PropertyValueFactory<>("capacity"));
        this.columnVirtual.setCellValueFactory(new PropertyValueFactory<>("virtual"));

        this.tableRooms.setItems(FXCollections.observableList((new RoomDao()).getAllRooms()));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            this.showView(this.timetable == null ? "home" : "timetables", event);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonLessonsClick(ActionEvent event) {
        try {
            if (this.timetable != null) {
                Room selectedRoom = this.tableRooms.getSelectionModel().getSelectedItem();
                if (selectedRoom != null) {
                    Object controller = this.showViewAndGetController("lessons", event);
                    if (controller instanceof IParametersController) {
                        IParametersController parametersController = (IParametersController) controller;
                        parametersController.addParameter(selectedRoom);
                        parametersController.addParameter(this.timetable);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonFeaturesClick(ActionEvent event) {
        try {
            Room selectedRoom = tableRooms.getSelectionModel().getSelectedItem();
            if (selectedRoom != null) {
                Object controller = this.showViewAndGetController("features", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedRoom);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonAvailabilityClick(ActionEvent event) {
        try {
            Room selectedRoom = this.tableRooms.getSelectionModel().getSelectedItem();
            if (selectedRoom != null) {
                Object controller = this.showViewAndGetController("availabilities", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedRoom);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof Timetable) {
            this.timetable = (Timetable) parameter;
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
