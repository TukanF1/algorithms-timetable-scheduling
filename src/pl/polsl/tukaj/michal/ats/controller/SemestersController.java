package pl.polsl.tukaj.michal.ats.controller;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import pl.polsl.tukaj.michal.ats.dao.FieldOfStudyDao;
import pl.polsl.tukaj.michal.ats.dao.SemesterDao;
import pl.polsl.tukaj.michal.ats.model.FieldOfStudy;
import pl.polsl.tukaj.michal.ats.model.Semester;
import pl.polsl.tukaj.michal.ats.model.Timetable;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SemestersController extends ViewController implements Initializable, IParametersController {
    private Timetable timetable;
    private FieldOfStudy fieldOfStudy;

    @FXML
    private TableView<Semester> tableSemesters;

    @FXML
    private TableColumn<Semester, Integer> columnId;

    @FXML
    private TableColumn<Semester, Integer> columnSemester;

    @FXML
    private Label fieldOfStudyNameLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.columnId.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.columnSemester.setCellValueFactory(new PropertyValueFactory<>("semester"));
    }

    @FXML
    void buttonBackClick(ActionEvent event) {
        try {
            Object controller = this.showViewAndGetController("fieldsOfStudy", event);
            if (controller instanceof IParametersController) {
                IParametersController parametersController = (IParametersController) controller;
                if (this.timetable != null) {
                    parametersController.addParameter(timetable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonLessonsClick(ActionEvent event) {
        try {
            Semester selectedSemester = this.tableSemesters.getSelectionModel().getSelectedItem();
            if (timetable != null && selectedSemester != null) {
                Object controller = this.showViewAndGetController("lessons", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedSemester);
                    parametersController.addParameter(timetable);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonAvailabilityClick(ActionEvent event) {
        try {
            Semester selectedSemester = this.tableSemesters.getSelectionModel().getSelectedItem();
            if (selectedSemester != null) {
                Object controller = this.showViewAndGetController("availabilities", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    parametersController.addParameter(selectedSemester);
                    if (this.timetable != null) {
                        parametersController.addParameter(timetable);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonSectionsClick(ActionEvent event) {
        try {
            Semester semester = this.tableSemesters.getSelectionModel().getSelectedItem();
            if (semester != null) {
                Object controller = this.showViewAndGetController("sections", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    List<Object> parameters = new ArrayList<>();
                    parameters.add(semester);
                    parameters.add(this.fieldOfStudy);
                    if (this.timetable != null) {
                        parameters.add(timetable);
                    }
                    parametersController.addParameters(parameters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void buttonGroupsClick(ActionEvent event) {
        try {
            Semester semester = this.tableSemesters.getSelectionModel().getSelectedItem();
            if (semester != null) {
                Object controller = this.showViewAndGetController("groups", event);
                if (controller instanceof IParametersController) {
                    IParametersController parametersController = (IParametersController) controller;
                    List<Object> parameters = new ArrayList<>();
                    parameters.add(semester);
                    parameters.add(this.fieldOfStudy);
                    if (this.timetable != null) {
                        parameters.add(timetable);
                    }
                    parametersController.addParameters(parameters);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addParameter(Object parameter) {
        if (parameter instanceof FieldOfStudy) {
            fieldOfStudy = (FieldOfStudy) parameter;
        } else if (parameter instanceof Semester) {
            Semester semester = (Semester) parameter;
            fieldOfStudy = (new FieldOfStudyDao()).getFieldOfStudyById(semester.getFieldOfStudyId());
        } else if (parameter instanceof Timetable) {
            this.timetable = (Timetable) parameter;
        }
        if (fieldOfStudy != null && this.tableSemesters.getItems().isEmpty()) {
            this.fieldOfStudyNameLabel.setText(TextConstans.semestersOfFieldOfStudyNameLabelPrefix
                    + fieldOfStudy.getName());

            this.tableSemesters.setItems(FXCollections.observableList(
                    new ArrayList<>((new SemesterDao()).getSemestersByFieldOfStudy(fieldOfStudy))
            ));
        }
    }

    @Override
    public void addParameters(List<?> parameters) {
        parameters.forEach(this::addParameter);
    }
}
