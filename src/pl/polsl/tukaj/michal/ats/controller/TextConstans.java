package pl.polsl.tukaj.michal.ats.controller;

class TextConstans {
    static final String availabilityNameLabelPrefix = "Availability of: ";
    static final String featuresParameterNameLabelPrefix = "Features of: ";
    static final String semestersOfFieldOfStudyNameLabelPrefix = "Semesters of: ";
    static final String groupsLabelPrefix = "Groups of: ";
    static final String groupsOfSemesterNameLabelPrefixSemester = "\nSemester: ";
    static final String sectionsLabelPrefix = "Sections of: ";
    static final String sectionsOfGroupNameLabelPrefixSemester = "\nSemester: ";
    static final String sectionsOfGroupNameLabelPrefixGroup = "\nGroup: ";
    static final String teachersLabelPrefix = "Teachers of: ";
    static final String subjectsLabelPrefix = "Subjects of: ";
    static final String subjectsLabelSemesterPrefix = "Semester: ";
    static final String subjectsLabelFieldOfStudyPrefix = "Field of study: ";
    static final String lessonsLabelTimetablePrefix = "Lessons of \"";
    static final String lessonsLabelTimetableSuffix = "\" timetable.";
}
