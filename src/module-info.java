module AlgorithmsTimetableScheduling {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;
    requires sqlite.jdbc;

    opens pl.polsl.tukaj.michal.ats.model;
    opens pl.polsl.tukaj.michal.ats.controller;
}